import { AppRouter } from '../../@settings/Setup';
import WriterQuery from '../../@query/order';
import { ORDER } from '../../@models';

const WriterRouter = AppRouter();

const { charge, deleteOrder } = new WriterQuery();

WriterRouter.post("/order/charge", charge);
WriterRouter.post("/order/deleteOrder", deleteOrder);
export default WriterRouter;