import { AppRouter } from '../../@settings/Setup';
import WriterQuery from '../../@query/writer';
import MiddlewareValidation from '../../@query/middleware';
import { PENULIS, BOOK } from '../../@models';

const WriterRouter = AppRouter();

const { signInValidation, authorizeToken, signUpValidation } = new MiddlewareValidation(PENULIS); // authorizeToken(["super"]), authorizeToken(["super", "admin"])
const { writerSignIn, verifyToken, sendVerification, verifyEmail, writerSignUp, signOut, listBook, addBook, updateBookById, deleteBookById } = new WriterQuery();

WriterRouter.post("/writer/auth/login", signInValidation, writerSignIn);
WriterRouter.post("/writer/auth/signup", signUpValidation, writerSignUp);
WriterRouter.post("/writer/auth/signout", signOut);
WriterRouter.post("/writer/auth/revoke", verifyToken);
WriterRouter.post("/writer/send/verify", sendVerification);
WriterRouter.get("/writer/verification/verify-email", verifyEmail);
WriterRouter.get("/writer/listbook", listbook);
WriterRouter.post("/writer/addbook", addbook);
WriterRouter.put("/writer/book/:id", updateBookById);
WriterRouter.delete("/writer/bookbyid/:id", deleteBookById);

export default WriterRouter;