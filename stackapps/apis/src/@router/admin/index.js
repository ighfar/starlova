import { ADMIN } from "../../@models";
import AdminQuery from "../../@query/admin";
import MiddlewareValidation from "../../@query/middleware";
import {
    AppRouter
} from "../../@settings/Setup";
const AdminRouter = AppRouter();
const { authorizeToken, signInValidation, signUpValidation } = new MiddlewareValidation(ADMIN)
const {
    searchById,
    getList,
    signIn,
    addRegularAdmin,
    validateAdminSuper,
    validateRegularAdmin,
    addPublisherAdmin,
    addWriterAdmin,
    addEditorAdmin,
    getListAllPenerbit,
    getListAllPenulis,
    getListAllEditor,
    deletePublisherById,
    deleteWriterById,
    deleteEditorById,
    updatePublisherById,
    updateWriterById,
    updateEditorById,
    getListPendingOrder,
    getListSuccessOrder,
    getListFailedOrder
} = new AdminQuery();

AdminRouter.post("/admin", authorizeToken(["super", "admin"]), getList);
AdminRouter.post("/admin/find", authorizeToken(["super", "admin"]), searchById);
AdminRouter.post("/admin/add/pub", authorizeToken(["super", "admin"]), addPublisherAdmin);

AdminRouter.post("/admin/publisher/list", authorizeToken(["super", "admin"]), getListAllPenerbit);
AdminRouter.post("/admin/writer/list", authorizeToken(["super", "admin"]), getListAllPenulis);
AdminRouter.post("/admin/editor/list", authorizeToken(["super", "admin"]), getListAllEditor);

AdminRouter.post("/admin/add/writer", authorizeToken(["super", "admin"]), addWriterAdmin);
AdminRouter.post("/admin/add/editor", authorizeToken(["super", "admin"]), addEditorAdmin);
AdminRouter.post("/admin/login", signInValidation, signIn);
AdminRouter.post("/admin/order/pending", authorizeToken("super"), getListPendingOrder);
AdminRouter.post("/admin/order/success", authorizeToken("super"), getListSuccessOrder);
AdminRouter.post("/admin/order/failed", authorizeToken("super"), getListFailedOrder);
/* RESTRICT ADMIN SUPER ONLY */
AdminRouter.post("/admin/register", signUpValidation, authorizeToken("super"), addRegularAdmin);
AdminRouter.delete("/admin/publisher/:id", authorizeToken("super"), deletePublisherById);
AdminRouter.delete("/admin/writer/:id", authorizeToken("super"), deleteWriterById);
AdminRouter.delete("/admin/editor/:id", authorizeToken("super"), deleteEditorById);
AdminRouter.put("/admin/publisher/:id", authorizeToken("super"), updatePublisherById);
AdminRouter.put("/admin/writer/:id", authorizeToken("super"), updateWriterById);
AdminRouter.put("/admin/editor/:id", authorizeToken("super"), updateEditorById);


export default AdminRouter;