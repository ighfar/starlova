import AdminRouter from "./admin";
import PublisherRouter from "./publisher";
import WriterRouter from "./writer";

export {
    AdminRouter,
    PublisherRouter,
    WriterRouter
};