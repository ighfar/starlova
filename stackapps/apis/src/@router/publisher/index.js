import { PENERBIT } from "../../@models";
import MiddlewareValidation from "../../@query/middleware";
import PublisherQuery from "../../@query/publisher";
import { AppRouter } from "../../@settings/Setup";

const PublisherRouter = AppRouter();
const { authorizeToken, signInValidation, signUpValidation } = new MiddlewareValidation(PENERBIT)
const {
    publisherSignIn,
    publisherSignUp,
    addNewPublisherAdminRoles,
    AddAdminRoleUserFromPublisher,
    addWriterPublisher,
    AllAccessAdminPublisher,
    adminSuperAccessOnly,
    adminAndSuperOnly,
    getAllWriters,
    deleteWriterById,
    deleteAdminByRolesEqualUser,
    banWriter,
    banEditor,
    suspenseWriter,
    suspenseEditor,
    addEditorWriter,
    getAllEditorsByWriter,
    getAllEditors,
    getAdminList,
    getAdminListRoleUsers,
    bannedAdmin,
    suspenseAdmin,
    bannedAdminUserRoles,
    suspenseAdminUserRoles,
} = new PublisherQuery();

PublisherRouter.post("/publisher/list/admin", authorizeToken("super"), getAdminList);
PublisherRouter.post("/publisher/list/roles/user", authorizeToken("super"), getAdminListRoleUsers);
PublisherRouter.put("/publisher/ban/admin", authorizeToken("super"), bannedAdmin);
PublisherRouter.put("/publisher/suspense/admin", authorizeToken("super"), suspenseAdmin);
PublisherRouter.patch("/publisher/ban/admin/user", authorizeToken("super"), bannedAdminUserRoles);
PublisherRouter.patch("/publisher/suspense/admin/user", authorizeToken("super"), suspenseAdminUserRoles);
PublisherRouter.post("/publisher/add/admin", authorizeToken("super"), addNewPublisherAdminRoles);
PublisherRouter.delete("/publisher/writer", authorizeToken("super"), deleteWriterById);

PublisherRouter.patch("/publisher/writer/list", authorizeToken(["super", "admin", "user"]), getAllWriters);
PublisherRouter.get("/publisher/editor/by", authorizeToken(["super", "admin", "user"]), getAllEditorsByWriter);
PublisherRouter.get("/publisher/editor/list", authorizeToken(["super", "admin", "user"]), getAllEditors);

PublisherRouter.put("/publisher/suspense/writer", authorizeToken(["super", "admin", "user"]), suspenseWriter);
PublisherRouter.put("/publisher/suspense/editor", authorizeToken(["super", "admin", "user"]), suspenseEditor);
PublisherRouter.put("/publisher/ban/writer", authorizeToken(["super", "admin", "user"]), banWriter);
PublisherRouter.put("/publisher/ban/editor", authorizeToken(["super", "admin", "user"]), banEditor);
PublisherRouter.put("/publisher/add/writer", authorizeToken(["super", "admin", "user"]), addWriterPublisher);
PublisherRouter.put("/publisher/add/editor", authorizeToken(["super", "admin", "user"]), addEditorWriter);

PublisherRouter.delete("/publisher/delete/admin/roles/user", authorizeToken(["super", "admin"]), deleteAdminByRolesEqualUser);
PublisherRouter.post("/publisher/add/admin/roles/user", authorizeToken(["super", "admin"]), AddAdminRoleUserFromPublisher);

PublisherRouter.post("/publisher/auth/login", signInValidation, publisherSignIn);
PublisherRouter.post("/publisher/auth/register",signUpValidation, publisherSignUp);

export default PublisherRouter;