import { App } from "../@settings/Setup";
import CookieParser from 'cookie-parser';
import { json, urlencoded } from 'express';
import * as ApiRouter from "../@router";
import { hostname } from 'os';
import HttpStatus from "../utils/HttpStatus";
import adminDb from "../firebase/firebase";
import { secret } from "../@config/config";
import session from 'express-session';

// SETUP PARSER TYPE OF REQ BODY
const ApiServer = App();

// PARSING COOKIE MIDDLEWARE
ApiServer.use(CookieParser());

// PARSING INCOMING DATA
ApiServer.use(json());
ApiServer.use(urlencoded({
    extended: true
}));

// BERLAKU 24 JAM
const oneDay = 1000 * 60 * 60 * 24;

// SESSION MIDDLEWARE
ApiServer.enable('trust proxy'); 
ApiServer.use(session({
    secret: secret,
    store: null,
    saveUninitialized:false,
    proxy : true,
    cookie: { 
        secure: process.env.NODE_ENV === 'production' ? true : false,
        path: '/',
        maxAge: oneDay 
    },
    resave: true
}));

// SETUP CORS
ApiServer.use(async function (req, res, next) {
    
    const originPort = process.env.NODE_ENV !== 'production' ? ["9000", "8000"] : ["3000", "4000"];
    const allowedOrigins = [];

    // PUSH PORT YANG DIZINKAN
    originPort.map((port, key) => {
        allowedOrigins.push(`${req.protocol}://${hostname()}:${port}`);
    })

    res.header('Access-Control-Allow-Credentials', true);
    res.header('Access-Control-Allow-Origin', req.headers.origin);
    res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT,PATCH,DELETE");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Authorization, X-HTTP-Method-Override, Content-Type, Accept");

    next();
   
});

Object.keys(ApiRouter).map((val, key) => {
    ApiServer.use('/v2', ApiRouter[val]);
});

/* HANDLING ROUTING NOT FOUND PADA PATH /v1 */
/* RESPONSE : GET METHOD /v1 tidak ditemukan! */

ApiServer.get('*', function (req, res) {
    res.status(404).send(`${req.method} METHOD ${req.path} tidak ditemukan!`);
});

ApiServer.post('*', function (req, res) {
    res.status(404).send(`${req.method} METHOD ${req.path} tidak ditemukan!`);
});

ApiServer.put('*', function (req, res) {
    res.status(404).send(`${req.method} METHOD ${req.path} tidak ditemukan!`);
});

ApiServer.patch('*', function (req, res) {
    res.status(404).send(`${req.method} METHOD ${req.path} tidak ditemukan!`);
});

ApiServer.delete('*', function (req, res) {
    res.status(404).send(`${req.method} METHOD ${req.path} tidak ditemukan!`);
});


export default ApiServer;