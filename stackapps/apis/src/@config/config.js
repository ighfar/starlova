import dotenv from 'dotenv';
if (process.env.NODE_ENV !== 'production') {
    dotenv.config();
}
/* Uncomment Line 1 Ketika menggunakan env pengembangan local */

export const data = {
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    user: process.env.DB_USER,
    password: process.env.DB_PASS,
    database: process.env.DB_NAME
};
export const port = {
    admin: process.env.NODE_PORT_ADMIN,
    app: process.env.NODE_PORT_APPS,
    api: process.env.NODE_PORT_API,
};
export const superAdmin = {
    email: process.env.SUPERADMIN_EMAIL,
    password: process.env.SUPERADMIN_PASS,
};
export const defaultPassword = process.env.DEFAULT_PASSWORD_USER;
export const secret = process.env.JWT_SECRET;
export const refreshSecret = process.env.JWT_REFRESH_SECRET;
export const emailFrom = process.env.SMTP_EMAIL;
export const smtpOptions = {
    pool: process.env.POOL,
    secure: process.env.SECURE,
    host: process.env.SMTP_HOST,
    port: process.env.SMTP_PORT,
    auth: {
        user: process.env.SMTP_USER,
        pass: process.env.SMTP_PASS
    },
    tls: {
        // Mencegah kegagalan jika certs domain invalid
        rejectUnauthorized: false,
    },
};