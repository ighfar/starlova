import { Sequelize } from 'sequelize';
import { data } from './config';
import mysql from 'mysql2/promise'
const { database, user, password, host } = data;

(async () => {
    try {
        const connect = await mysql.createConnection({user, password})
        connect.query(`CREATE DATABASE IF NOT EXISTS ${database}`)
            .then(() => {
                console.log(`======== DATABASE ${database} CONNECTED =========`)
            })
    } catch (error) {
        console.log(error.message);
    }
})()

const db = new Sequelize(database, user, password, {
    host,
    dialect: 'mysql',
    timezone: '+07:00'
})

export const HandlingSync = {

    // HAPUS SEMUA TABLE DIDALAM DATABASE
    // ( LOGIC => TABLE || COLUMNS ? UBAH TABLE || UBAH COLUMN : BUAT TABLE BARU || UBAH COLUMN FIELD )
    ClearFunc: async (model) => {
        try {
            await model.drop({
                match: /_test$/
            });
            console.log('HAPUS SEMUA TABLE')
        } catch (error) {
            console.log(error.message);
        }

    },

    // BUAT TABLE User dari Model JIKA TIDAK ADA DIPANGGIL SATU KALI
    // ( LOGIC => TABLE || COLUMNS ? UBAH TABLE || UBAH COLUMN : BUAT TABLE BARU || UBAH COLUMN FIELD ) 1 KALI
    SyncFunc: async (model) => {
        try {
            await model.sync();
        } catch (error) {
            console.log(error.message);
        }

    },


    // AUTO MODIFIKASI JIKA NAMA TABLE DAN COLUMNS BERUBAH
    // ( LOGIC => TABLE || COLUMNS ? UBAH TABLE || UBAH COLUMN : BUAT TABLE BARU || UBAH COLUMN FIELD )
    // JIKA OPERASI CRUD DATA BALIKAN AKAN BERTAMBAH => LENGTH = 1++
    AlterFunc: async (model) => {
        try {
            await model.sync({
                alter: true,
                match: /_test$/
            });
        } catch (error) {
            console.log(error.message);
        }

    },

    // MEMBUAT TABLE JIKA TIDAK ADA ( LOGIC => TABLE ? HAPUS TABLE : BUAT BARU )
    // JIKA OPERASI CRUD DATA BALIKAN TIDAK 1 => LENGTH = 1
    ForceFunc: async (model) => {
        try {
            await model.sync({
                force: true,
                match: /_test$/
            });
        } catch (error) {
            console.log(error.message);
        }

    },
}
/* AUTO MODIFIKASI JIKA NAMA TABLE DAN COLUMNS BERUBAH, AKTIFKAN JIKA DIPERLUKAN GUNAKAN DENGAN BIJAK */
HandlingSync.SyncFunc(db);
// HandlingSync.ForceFunc(db);


export default db;