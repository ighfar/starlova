import MasterQuery from ".";
import multer from 'multer';
import path from 'path';
import fs from 'fs';

export default class UploadQuery extends MasterQuery {
    constructor(data = {
        destination: "./public/upload",
        fileName: "myfile",
        fieldName: "file"
    }) {
        super();
        this.data = data;
        this.storage = multer.diskStorage({
            destination: this.data.destination,
            filename: function (req, file, cb) {
                const ext = path.extname(file.originalname);
                const FileName = file.fieldname + '-' + Date.now() + ext;
                cb(null, FileName);
            }
        });

        this.images = multer({
            storage: this.storage,
            limits: {
                fileSize: '2mb'
            },
            fileFilter: function (req, file, cb) {
                // Allowed ext
                const filetypes = /jpeg|jpg|png|gif|zip/;
                // Check ext
                const extname = filetypes.test(path.extname(file.originalname).toLowerCase());
                // Check mime
                const mimetype = filetypes.test(file.mimetype);

                if (mimetype && extname) {
                    return cb(null, true);
                } else {
                    cb('Error: Images Only!');
                }
            }
        }).single(this.data.fieldName);
    }

    uploadFile = (error, data) => {
        // Membuat Promise Upload
        
        return this.images(req, res, (err) => {
            return new Promise((resolve, reject) => {
                if (err) {
                    error(err)
                    return reject({
                        error: err
                    });
                }
                if (req.file == undefined) {
                    error(err)
                    return reject({
                        error: err
                    })
                };
                const finalFileName = `${this.data.fieldName}-${this.data.fileName + path.extname(req.file.filename)}`;
                fs.renameSync(req.file.path, req.file.path.replace(req.file.filename, finalFileName));
                const pathURI = `${this.data.destination}/${finalFileName}`;
                data(pathURI);
                return resolve({
                    pathFile: pathURI
                })
            })
        });
        
    }

}