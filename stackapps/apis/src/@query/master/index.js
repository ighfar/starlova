import { Op } from 'sequelize';
import HttpStatus from '../../utils/HttpStatus';

export default class MasterQuery {
    constructor(props){
        this.props = props;
    }

    getListAll = async (req = [], res) => {
        try {
            const getListExist = await this.props.findAll({
                attribute: req.body.attribute
            });
            if(!getListExist) {
                res.status(HttpStatus.NOT_FOUND).send({
                    message: "gagal!"
                })
            } else {
                res.status(HttpStatus.OK).send({
                    result: getListExist
                });
            }
        } catch (error) {
            res.status(HttpStatus.BAD_GATEWAY).send({
                message: error.message
            })
        }
    }
    
    getList = async (req, res) => {
        try {
            const getListExist = await this.props.findAll({
                where: {
                    userUserid: req.body.userid
                }
            });
            if(!getListExist) {
                res.status(HttpStatus.NOT_FOUND).send({
                    message: "gagal!"
                })
            } else {
                res.status(HttpStatus.OK).send({
                    result: getListExist
                });
            }
        } catch (error) {
            res.status(HttpStatus.BAD_GATEWAY).send({
                message: error.message
            })
        }
    }
    getById = async (req, res) => {
        try {
            const getDataExist = await this.props.findOne({
                where: {
                    [Op.and] : [
                        { id: req.query.id },
                        { userUserid: req.body.userid },
                    ]
                }
            });
            if(!getDataExist) {
                res.status(HttpStatus.NOT_FOUND).send({
                    message: "gagal!"
                })
            } else {
                res.status(HttpStatus.OK).send({
                    result: getDataExist
                });
            }
        } catch (error) {
            res.status(HttpStatus.BAD_GATEWAY).send({
                message: error.message
            })
        }
    }
    create = async (req, res) => {
        try {
            const created = await this.props.create(req.body);
            if(!created) {
                res.status(HttpStatus.NOT_FOUND).send({
                    message: "gagal!"
                })
            } else {
                res.status(HttpStatus.OK).send({
                    result: created
                });
            }
        } catch (error) {
            res.status(HttpStatus.BAD_GATEWAY).send({
                message: error.message
            })
        }
    }
    update = async (req, res) => {
        try {
            const updateSuccess = await this.props.update(req.body,{
                where: {
                    [Op.and] : [
                        { id: req.params.id },
                        { userUserid: req.body.userid },
                    ]
                }
            });
            if(!updateSuccess) {
                res.status(HttpStatus.NOT_FOUND).send({
                    message: "gagal!"
                })
            } else {
                res.status(HttpStatus.OK).send({
                    result: "updated"
                });
            }
        } catch (error) {
            res.status(HttpStatus.BAD_GATEWAY).send({
                message: error.message
            })
        }
    }

    remove = async (req, res) => {
        try {
            const removeSuccess = await this.props.destroy({
                where: {
                    [Op.and] : [
                        { id: req.params.id },
                        { userUserid: req.body.userid },
                    ]
                }
            });
            if(!removeSuccess) {
                res.status(HttpStatus.NOT_FOUND).send({
                    message: "gagal!"
                })
            } else {
                res.status(HttpStatus.OK).send({
                    result: "removed"
                });
            }
        } catch (error) {
            res.status(HttpStatus.BAD_GATEWAY).send({
                message: error.message
            })
        }
    }
}