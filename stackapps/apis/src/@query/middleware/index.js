import { secret } from "../../@config/config";
import { Op } from "sequelize";
import HttpStatus from "../../utils/HttpStatus"
import Joi from 'joi';
import jwt from 'jsonwebtoken';

export default class MiddlewareValidation {
    constructor(model, roles = []){
        this.model = model;
        this.rolesPermission = typeof roles === 'string' ? roles[roles] : roles;
        this.validFirstName = Joi.string().required();
        this.validlastName = Joi.string().required();
        this.validUsername = Joi.string().alphanum().min(6).max(30).required();
        this.validEmail = Joi.string().email({ minDomainSegments: 2, tlds: { allow: ['com', 'net'] } }).message("Masukan email yang valid!").required();
        this.validPassword = Joi.string().min(6).max(12).required();
        this.confirmPassword = Joi.any().valid(Joi.ref('password')).required();
        this.accessToken = [ Joi.string(), Joi.number() ];
        this.validBirthYear = Joi.number().integer().min(1900).max(2005);
        this.validIpAddress = Joi.string().ip({ version: [ 'ipv4', 'ipv6' ], cidr: 'required'});
        this.validUserid = Joi.string().guid({ version: [ 'uuidv4', 'uuidv5' ] });
        this.validCreditCard = Joi.string().creditCard();
        this.validBase64 = Joi.string().base64({ paddingRequired: true });
        this.validInputNoWhiteSpace = Joi.string().trim(true);
        this.validToken = Joi.string().required();
        this.validAcceptTerms = Joi.boolean();
    }
    
    authorizeToken = (roles = []) => {
        // Jika ada 1 tipe params sama dengan string maka konversikan ke bentuk arrays ["super"]
        const permission = typeof roles === 'string' ? [roles] : roles;
        return async (req, res, next) => {
            try {
                
                const authHeader = req.headers['authorization']; // With Bearer Token
                const authHeaderCustom = req.headers['auth-token'];
                const token = authHeader ? authHeader.split(' ')[1] : authHeaderCustom;

                const decoded = jwt.decode(token, secret);
                const validate = jwt.verify(token, secret);

                if (!validate) {
                    res.status(HttpStatus.FORBIDDEN).send({
                        messages: "It's not allowed!"
                    })
                } else {
                    const checkUser = await this.model.findOne({
                        where: {
                            [Op.and]: [{
                                    id: decoded.userid
                                },
                                {
                                    roles: {
                                        [Op.or]: permission
                                    }
        
                                }
                            ]
                        }
                    })
                    if (!checkUser) {
                        const statusMsg = permission !== "super" ? "Your are not admin super!" : "Your are not admin!"
                        res.status(HttpStatus.NOT_FOUND).send({
                            messages: statusMsg
                        })
                    } else {
                        req.user = validate;
                        next();
                    }
        
        
                }
            } catch (error) {
                res.status(HttpStatus.NOT_FOUND).send({
                    messages: 'You are not authorized!',
                    error: error.messages
                })
            }
        }
    }

    signInValidation = async (req, res, next) => {
        const schema = Joi.object({
            email: this.validEmail,
            password: this.validPassword,
        })
        try {
            const valid = await schema.validateAsync({
                email: req.body.email,
                password: req.body.password
            })
            const checkUserBlockingExist = await this.model.findOne({
                where: {
                    [Op.and] : [
                        { email: valid.email},
                        { [Op.or]: [
                            {isSuspended: true},
                            {isBanned: true},
                            {isVerified: false }
                        ] },
                        { roles : {
                            [Op.or] : ["admin", "user"]
                        }}
                    ]
                }
            }) 

            if(!checkUserBlockingExist){
                next();
            } else {
                if(checkUserBlockingExist.isVerified === false){
                    res.status(HttpStatus.FORBIDDEN).send({
                        message: "Verifikasi email kamu!"
                    })
                } else {
                    const statusBlock = checkUserBlockingExist.isSuspended === true ? "User has been suspended!" : "User has been banned!";
                    res.status(HttpStatus.FORBIDDEN).send({
                        message: statusBlock
                    })
                }
            }

            
        } catch (error) {
            res.status(HttpStatus.EXPECTATION_FAILED).send({
                message: error.message
            })
        }
        
    }

    signUpValidation = async (req, res, next) => {
        const schema = Joi.object({
            email: this.validEmail,
            password: this.validPassword,
            confirm: this.confirmPassword,
            acceptTerms: this.validAcceptTerms
        })
        try {
            const valid = await schema.validateAsync({
                email: req.body.email,
                password: req.body.password,
                confirm: req.body.confirm,
                acceptTerms: req.body.acceptTerms,
            })
            
            const checkUserExist = await this.model.findOne({
                where: {
                    [Op.and] : [
                        { email: valid.email},
                        { roles : {
                            [Op.or] : ["admin", "user"]
                        }}
                    ]
                }
            })
            if(!checkUserExist){
                next();
            } else {
                if(checkUserExist.isVerified === false){
                    res.status(HttpStatus.NOT_ACCEPTABLE).send({ message: "E-mail belum diverifikasi!" })
                } else {
                    res.status(HttpStatus.NOT_ACCEPTABLE).send({ message: "E-mail telah digunakan user lain!" })
                }
            }

        } catch (error) {
            res.status(HttpStatus.EXPECTATION_FAILED).send({
                message: error.message
            })
            
        }
    }

   
    
    sendAlreadyRegisteredEmail = async (email, origin) => {
        let message;
        if (origin) {
            message = `<p>If you don't know your password please visit the <a href="${origin}/account/forgot-password">forgot password</a> page.</p>`;
        } else {
            message = `<p>If you don't know your password you can reset it via the <code>/account/forgot-password</code> api route.</p>`;
        }
    
        await sendEmail({
            to: email,
            subject: 'Sign-up Verification API - Email Already Registered',
            html: `<h4>Email Already Registered</h4>
                   <p>Your email <strong>${email}</strong> is already registered.</p>
                   ${message}`
        });
    }
    
    sendPasswordResetEmail = async (account, origin) =>{
        let message;
        if (origin) {
            const resetUrl = `${origin}/account/reset-password?token=${account.resetToken}`;
            message = `<p>Please click the below link to reset your password, the link will be valid for 1 day:</p>
                       <p><a href="${resetUrl}">${resetUrl}</a></p>`;
        } else {
            message = `<p>Please use the below token to reset your password with the <code>/account/reset-password</code> api route:</p>
                       <p><code>${account.resetToken}</code></p>`;
        }
    
        await sendEmail({
            to: account.email,
            subject: 'Sign-up Verification API - Reset Password',
            html: `<h4>Reset Password Email</h4>
                   ${message}`
        });
    }

}