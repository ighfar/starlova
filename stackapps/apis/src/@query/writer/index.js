import { Op } from 'sequelize';
import jwt from 'jsonwebtoken';
import bcrypt from 'bcryptjs';
import HttpStatus from '../../utils/HttpStatus';
import { PENERBIT, PENULIS, TOKEN, BOOK, ORDER } from '../../@models';
import { secret, emailFrom, smtpOptions, superAdmin } from '../../@config/config';
import MiddlewareValidation from '../middleware';
import nodemailer from 'nodemailer';
import Joi from 'joi';
import store from 'store';
import { btoa as encode, atob as decode64 } from '../../utils/base64';

( async () => {
    try {
        const findAdminExist = await PENERBIT.findOne({
            where: {
                [Op.and] : [
                    { roles: "super" },
                ]
            }
        });
        if(!findAdminExist){
            console.log(`======== ERROR REFRENCE PUBLISHER ID NOT EXIST =========`);
            
        } else {
            const findAdminWriter = await PENULIS.findOne({
                where: {
                    [Op.and] : [
                        { roles: "admin" },
                    ]
                }
            });
            if(!findAdminWriter){
                bcrypt.hash(superAdmin.password, 10, async (err, hash) => {
                    if (err) throw err;
                    await PENULIS.create({ 
                        email: superAdmin.email, 
                        password: `${hash}`, 
                        roles: "admin", 
                        publisherId: findAdminExist.id,
                        isVerified: true
                    })
                    .then((success) => {
                        console.log(`======== ADMIN WRITER IS CREATED =========`);
                        console.log(JSON.stringify(success, null, 4));
                    })
                    .catch((error) => {
                        console.log(`======== FAILED TO REGISTER ADMIN WRITER  =========`);
                    })
                })
            } else {
                console.table([{
                    "STATUS ADMIN WRITER": `======== ADMIN WRITER IS ALREADY EXIST  =========`
                }]);
            }
           
        }
    } catch (error) {
        console.log(`======== SOMETHING WENT WRONG =========`);
    }
})()

export default class WriterQuery extends MiddlewareValidation {
    constructor(){
        super();
        this.token = this.validToken;
        
    }
    async writerSignIn(req, res){
        try {

            const penulisExist = await PENULIS.findOne({
                where: {
                    email: req.body.email,
                }
            })
            if (!penulisExist) {
                res.status(HttpStatus.BAD_REQUEST).send({
                    message: 'User does not exist!'
                })
    
            } else {
                if (bcrypt.compareSync(req.body.password, penulisExist.password)) {

                    const access = jwt.sign({ 
                        userid: penulisExist.id, 
                        roles: penulisExist.roles, 
                        pubid: penulisExist.publisherId 
                    }, secret, {
                        expiresIn: "60m", // 10 detik
                    })

                    const refreshToken = jwt.sign({ 
                        userid: penulisExist.id, 
                        roles: penulisExist.roles, 
                        pubid: penulisExist.publisherId 
                    }, secret, {
                        expiresIn: "2d", // 1 hari
                    })

                    const getToken = await penulisExist.getToken();
                    const timestamp = new Date().getTime(); // current time
                    const exp = timestamp + (60 * 60 * 1000 * 2)

                    if(!getToken){
                        await penulisExist.createToken({
                            token: refreshToken,
                            createdByIp: req.ip,
                            expires: exp
                        })
                        .catch((error) => {
                            res.status(HttpStatus.BAD_GATEWAY).send({
                                message: error.message
                            })
                        })
                    } else {
                        await TOKEN.update({
                            token: refreshToken,
                            createdByIp: req.ip,
                        }, {
                            where: {
                                id: getToken.id
                            }
                        })
                    }
                    const session = req.session;
                    session.access = req.body.isRemember ? access: refreshToken;
                    store.set('access', session.access);
                    console.log("CHECK TOKEN SESSION DI STORE", store.get('access'));
                    res.send({
                        token: store.get('access'),
                        email: penulisExist.email
                    })
                } else {
                    res.status(HttpStatus.BAD_REQUEST).send({
                        message: "Username or password invalid!"
                    })
                }
            }
    
        } catch (error) {
            res.status(HttpStatus.BAD_REQUEST).send({
                message: "BAD GATEWAY REQUEST",
                error: error.message
            })
        }
    }

     writerSignUp = async (req, res) => {
        try { 
            const findAdminSuperPublisher = await PENERBIT.findOne({
                where: {
                    [Op.and] : [
                        { roles: "super" },
                    ]
                }
            });
            if(!findAdminSuperPublisher){
                res.status(HttpStatus.BAD_REQUEST).send({
                    message: "Required reference publisherId!"
                })
            } else {
                bcrypt.hash(req.body.password, 10, async (err, hash) => { 
                    if(err) return sendStatus(404);
                    const created = await PENULIS.create({...req.body, password: hash, roles: "user", publisherId: findAdminSuperPublisher.id});
                    if(!created) { 
                        
                        res.status(HttpStatus.BAD_REQUEST).send({
                            message: "register failed!"
                        })
                    } else {
        
                        const tokenVerification = jwt.sign({ userid: created.id }, secret, {
                            expiresIn: "7d", // 2 jam
                        })
            
                        const timestamp = new Date().getTime(); // current time
                        const exp = timestamp + (60 * 60 * 1000 * 2)
                        const buatToken = await created.createToken({
                            token: tokenVerification,
                            createdByIp: req.ip,
                            expires: exp
                        })
            
                        if(!buatToken)return res.status(HttpStatus.BAD_REQUEST).send({
                            message: "create token failed!"
                        })
            
                        this.sendVerificationEmail(created, `${process.env.REACT_APP_WEB_APP}/api/v2`, tokenVerification)
                        .then((success) => {
                            res.send({
                                message: `Verifikasi email dikirim ke ${created.email}`
                            })
                        })
            
                        .catch((error) => {
                            res.status(HttpStatus.BAD_GATEWAY).send({
                                message: error
                            })
                        })
                            
                    }
                })
            }
    
        } catch (error) {
            res.status(HttpStatus.BAD_REQUEST).send({
                message: "BAD GATEWAY REQUEST",
                error: error.message
            })
        }
    }

    async verifyToken(req, res, next){
        const session = req.session;
        const access = session.access === 'undefined' ? store.get('access') : session.access;

        console.log("REVOKE AKSES TOKEN ON SESSION ", access);
        if(access === 'undefined') {
            return res.status(HttpStatus.FORBIDDEN).send({
                message: "Forbidden access!",
                code: 403
            })
        }
                
        jwt.verify(access, secret, async (error, decoded) => {
            // console.log(error)
            if(error) {
                return res.status(HttpStatus.FORBIDDEN).send({
                    message: "Forbidden access!",
                    code: 403
                })
            };
            
            try {
                const checkToken = await TOKEN.findOne({
                    where: {
                        writerId: decoded.userid
                    }
                })
                if(!checkToken){
                    res.status(HttpStatus.FORBIDDEN).send({
                        message: "invalid token!"
                    })
                } else {
                    
                    const access = jwt.sign({ userid: decoded.userid, roles: decoded.roles, pubid: decoded.pubid }, secret, {
                        expiresIn: "60m"
                    })

                    const refreshToken = jwt.sign({ userid: decoded.userid, roles: decoded.roles, pubid: decoded.pubid }, secret, {
                        expiresIn: "1d"
                    })

                    const timestamp = new Date().getTime(); // current time
                    const exp = timestamp + (60 * 60 * 1000 * 2)
                    await TOKEN.update({
                        token: refreshToken,
                        createdByIp: req.ip,
                        expires: exp
                        }, {
                            where: {
                                writerId: decoded.userid
                            }
                    })

                    .then(success =>  session.access = req.body.isRemember ? access : refreshToken)
                    .catch(error => res.sendStatus(403))
                    res.send({
                        token: session.access,
                    })
                }
            } catch (error) {
                res.status(HttpStatus.BAD_REQUEST).send({
                    message: error.message
                })
            }
        })
    }

    verifyEmail = async (req, res, next) => {
        const shema = Joi.object({
            token: this.validToken
        })

        const accessToken = req.query.token;
        
        try {
            const isTokenValid = await shema.validateAsync({
                token: accessToken
            })
            jwt.verify(isTokenValid.token, secret, async (error, decoded) => {
                if(error) return res.status(HttpStatus.FORBIDDEN).send({
                    message: "Verification token is invalid!"
                });
                try {
                    const checkToken = await TOKEN.findOne({
                        where: {
                            writerId: decoded.userid
                        }
                    })
                    if(!checkToken){
                        res.status(HttpStatus.FORBIDDEN).send({
                            message: "Verification token is invalid!"
                        })
                    } else {
                        const updateUserStatus = await PENULIS.update({
                            isVerified: true,
                        }, {
                            where: {
                                id: decoded.userid
                            }
                        })
    
                        if(!updateUserStatus) { 
                            return res.status(HttpStatus.BAD_REQUEST).send({
                                message: "Verification failed!"
                            })
                        } else {
                            store.set('access', accessToken);
                            const switchPath = decoded?.roles === 'admin' ? 'tenant' : 'writer';
                            res.redirect(`/${switchPath}?stpub=${decoded?.userid}&pubid=${decoded?.pubid}`);
                        }
                        
                    }
                } catch (error) {
                    res.status(HttpStatus.BAD_REQUEST).send({
                        message: error.message
                    })
                }
            })
        } catch (error) {
            res.status(HttpStatus.BAD_GATEWAY).send({
                message: error.message
            })
        }

    }

    sendVerification = async (req, res, next) => {
        try {
            const checkVerified = await PENULIS.findOne({
                where: {
                   [Op.and] : [
                       { isVerified: false},
                       { email: req.body.email }
                   ]
                }
            })
            if(!checkVerified){
                res.status(HttpStatus.FORBIDDEN).send({
                    message: "Email sudah diverifikasi, silahkan login!"
                })
            } else {
                const tokenVerification = jwt.sign({ 
                    userid: checkVerified.id,
                    pubid: checkVerified.publisherId,
                    roles: checkVerified.roles,
                }, secret, {
                    expiresIn: "7d", // 7 hari
                })
                this.sendVerificationEmail(checkVerified, `${process.env.REACT_APP_WEB_APP}/api/v2`, tokenVerification)
                .then((success) => {
                    res.send({
                        message: `Verifikasi email telah dikirim ke ${checkVerified.email}`
                    })
                })
                .catch((error) => {
                    res.status(HttpStatus.BAD_GATEWAY).send({
                        message: "Gagal mengirim verifikasi email!"
                    })
                })
            }
        } catch (error) {
            res.status(HttpStatus.BAD_REQUEST).send({
                message: error.message
            })
        }
    }

    signOut = async (req, res) => {
      
        try {
            jwt.verify(req.session.access, secret, async (error, decoded) => {
                if(error) return res.status(HttpStatus.FORBIDDEN).send({
                    message: "session expired",
                    code: HttpStatus.FORBIDDEN
                })
                const deleteToken = await TOKEN.update({
                    token: null,
                 }, {
                     where : {
                        writerId: decoded.userid
                     }
                 })
                 if(!deleteToken){
                   res.sendStatus(HttpStatus.BAD_REQUEST).send({
                       message: "Something went wrong!"
                   })
                 } else {
                   req.session.destroy();
                   store.remove("access");
                   res.status(HttpStatus.OK).send({
                       message: 'success'
                   })
    
                 }
            })
           
        } catch (error) {
            res.sendStatus(HttpStatus.BAD_GATEWAY).send({
                message: error.message
            })
        }
       
    }

    sendEmail = async ({ to, subject, html, from = `Starlova Publishing Platform <${emailFrom}>` }) => {
        const transporter = nodemailer.createTransport(smtpOptions);
        await transporter.sendMail({ from, to, subject, html });
    }

    sendVerificationEmail = async (account, origin, token) => {
        let message;
        if (origin) {
            const verifyUrl = `${origin}/writer/verification/verify-email?token=${token}`;
            message = `<p>Klik link dibawah ini untuk verifikasi email:</p>
                       <p><a href="${verifyUrl}">${verifyUrl}</a></p>`;
        } else {
            message = `<p>Please use the below token to verify your email address with the <code>/writer/verification/verify-email</code> api route:</p>
                       <p><code>${token}</code></p>`;
        }
    
       return await this.sendEmail({
            to: account.email,
            subject: 'Verifikasi Akun Pendaftaran - Verify Email',
            html: `<h4>Verifikasi Email</h4>
                   <p>Terimakasih telah melakukan pendaftaran di platform kami!</p>
                   ${message}`
        });
    }

     addBook = async (req, res) => {
      
        try {
                const bookExist = await BOOK.create(req.body)
                  .then((success) => {
                            res.status(HttpStatus.OK).send({
                                data: success
                            })
                        })
                } catch (error) {

                        res.status(HttpStatus.BAD_REQUEST).send({
                            message: error.message,
                        });
                    }

                }
     updateBookById = async (req, res) => {
      
        try {
                const bookExist = await BOOK.findOne({
                where: {
                    id: req.params.id
                }
            });
            if(!isExist){
                res.status(HttpStatus.BAD_REQUEST).send({
                    message: "failed"
                })
            } else {
               const updated = BOOK.update(req.body, {
                   where: {
                       id: req.params.id
                   }
               });
               if(!updated){
                   res.status(HttpStatus.BAD_REQUEST).send({
                       message: "failed"
                   })
               } else {
                   res.status(HttpStatus.ACCEPTED).send({
                       message: "updated"
                   })
               }
            }
        } catch (error) {
            res.status(HttpStatus.BAD_GATEWAY).send({
                message: error.message
            })
        }
    }

deleteBookById = async (req, res) => {
        try {
            const deleted = await BOOK.destroy({
                where: {
                    id: req.params.id
                }
            });
            if(!deleted){
                res.status(HttpStatus.BAD_REQUEST).send({
                    message: "failed"
                })
            } else {
                res.status(HttpStatus.OK).send({
                    message: "deleted"
                })
            }
        } catch (error) {
            res.status(HttpStatus.BAD_GATEWAY).send({
                message: error.message
            })
        }
    }

}





















