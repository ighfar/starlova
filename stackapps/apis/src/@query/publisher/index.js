import { Op } from 'sequelize';
import jwt from 'jsonwebtoken';
import bcrypt from 'bcryptjs';
import HttpStatus from '../../utils/HttpStatus';
import { PENERBIT, PENULIS, EDITOR, ADMIN } from '../../@models';
import { secret, superAdmin, defaultPassword } from '../../@config/config';

( async () => {
    try {
        const findAdminSuperPublisher = await PENERBIT.findOne({
            where: {
                [Op.and] : [
                    { roles: "super" },
                ]
            }
        });
        if(!findAdminSuperPublisher){
            const findSuperAdmin = await ADMIN.findOne({
                where: {
                    roles: "super",
                }
            })
            if(!findSuperAdmin){
                console.log(`======== Tidak ada super admin  =========`);
            } else {
                bcrypt.hash(superAdmin.password, 10, async (err, hash) => {
                    if (err) throw err;
                    await PENERBIT.create({ email: superAdmin.email, password: `${hash}`, roles: "super", adminId: findSuperAdmin.id})
                    .then((success) => {
                        console.log(`======== SUPER ADMIN PUBLISHER IS CREATED =========`);
                        console.log(JSON.stringify(success, null, 4));
                    })
                    .catch((error) => {
                        console.log(`======== FAILED TO REGISTER SUPER ADMIN PUBLISHER  =========`);
                    })
                })
            }
            
        } else {
            console.table([{
                "STATUS SUPER ADMIN PUBLISHER": `======== SUPER ADMIN PUBLISHER IS EXIST =========`
            }]);
        }
    } catch (error) {
        console.log(`======== SOMETHING WENT WRONG =========`);
    }
})()

export default class PublisherQuery {
    constructor(token){
        this.token = token;
    }
    async adminAndSuperOnly(req, res, next){
        try {
            const decoded = jwt.decode(req.headers['auth-token'], secret);
            const validate = jwt.verify(req.headers['auth-token'], secret);
            if (!validate) {
                res.status(HttpStatus.FORBIDDEN).send({
                    messages: "It's not allowed!"
                })
            } else {
                const checkUser = await PENERBIT.findOne({
                    where: {
                        [Op.and]: [{
                                id: decoded.userid
                            },
                            {
                                roles: {
                                    [Op.or]: ["super"]
                                }
    
                            }
                        ]
                    }
                })
                if (!checkUser) {
                    res.status(HttpStatus.NOT_FOUND).send({
                        messages: "Your are not admin publisher!"
                    })
                } else {
                    req.user = validate;
                    next();
                }
    
    
            }
        } catch (error) {
            res.status(HttpStatus.NOT_FOUND).send({
                messages: 'You are not authorized!',
                error: error.messages
            })
        }
    }
    async AllAccessAdminPublisher(req, res, next){
        try {
            const decoded = jwt.decode(req.headers['auth-token'], secret);
            const validate = jwt.verify(req.headers['auth-token'], secret);
            if (!validate) {
                res.status(HttpStatus.FORBIDDEN).send({
                    messages: "It's not allowed!"
                })
            } else {
                const checkUser = await PENERBIT.findOne({
                    where: {
                        [Op.and]: [{
                                id: decoded.userid
                            },
                            {
                                roles: {
                                    [Op.or]: ["admin", "super", "user"]
                                }
    
                            }
                        ]
                    }
                })
                if (!checkUser) {
                    res.status(HttpStatus.NOT_FOUND).send({
                        messages: "Your are not admin publisher!"
                    })
                } else {
                    req.user = validate;
                    next();
                }
    
    
            }
        } catch (error) {
            res.status(HttpStatus.NOT_FOUND).send({
                messages: 'You are not authorized!',
                error: error.messages
            })
        }
    }

    async adminSuperAccessOnly(req, res, next){
        try {
            const decoded = jwt.decode(req.headers['auth-token'], secret);
            const validate = jwt.verify(req.headers['auth-token'], secret);
            if (!validate) {
                res.status(HttpStatus.FORBIDDEN).send({
                    messages: "It's not allowed!"
                })
            } else {
                const checkUser = await PENERBIT.findOne({
                    where: {
                        [Op.and]: [{
                                id: decoded.userid
                            },
                            {
                                roles: {
                                    [Op.or]: ["super"]
                                }
    
                            }
                        ]
                    }
                })
                if (!checkUser) {
                    res.status(HttpStatus.NOT_FOUND).send({
                        messages: "Your are not admin publisher!"
                    })
                } else {
                    req.user = validate;
                    next();
                }
    
    
            }
        } catch (error) {
            res.status(HttpStatus.NOT_FOUND).send({
                messages: 'You are not authorized!',
                error: error.messages
            })
        }
    }

    async publisherSignUp(req, res){
        try {
            const publisherExist = await PENERBIT.findOne({
                where: {
                    [Op.and]: [
                        {
                            email: req.body.email
                        },
                        {
                            isBanned: false
                        },
                        {
                            isSuspended: false
                        }
                    ]
                }
            })
            if (!publisherExist) {
                bcrypt.hash(req.body.password, 10, async (err, hash) => {
                    if (err) throw err;
                    try {
                        try {
                            const getAdminExist = await ADMIN.findOne({
                                attributes: ["id"],
                                where: {
                                    roles: "admin"
                                }
                            })
                            if(!getAdminExist){
                                res.status(HttpStatus.BAD_REQUEST).send({
                                    message: "failed"
                                })
                            } else {
                               const created = await getAdminExist.createPublisher({
                                   email: req.body.email, 
                                   password: `${hash}`, 
                                   roles: "admin", 
                                   adminId: getAdminExist.id
                                });
                                if(!created){
                                    res.status(HttpStatus.BAD_REQUEST).send({
                                        message: "failed"
                                    })
                                } else {
                                    res.status(HttpStatus.OK).send({
                                        success: created
                                    })
                                }
                            }
                        } catch (error) {
                            res.status(HttpStatus.BAD_REQUEST).send({
                                message: error.message,
                            });
                        }
                       
                    } catch (error) {

                        res.status(HttpStatus.BAD_REQUEST).send({
                            message: error.message,
                        });
                    }
                })
    
            } else {
                res.status(HttpStatus.BAD_REQUEST).send({
                    message: "failed",
                });
            }
    
        } catch (error) {
            res.status(HttpStatus.BAD_REQUEST).send({
                message: "BAD GATEWAY REQUEST",
                error: error.message
            })
        }
    }

    async publisherSignIn(req, res){
        try {
            const publisherExist = await PENERBIT.findOne({
                where: {
                    email: req.body.email
                }
            })
            if (!publisherExist) {
                res.status(HttpStatus.BAD_REQUEST).send({
                    message: 'User does not exist!'
                })
    
            } else {
                if (bcrypt.compareSync(req.body.password, publisherExist.password)) {
                    const token = jwt.sign({ userid: publisherExist.id }, secret, {
                        expiresIn: "24h",
                    })
                    res.send({
                        token
                    })
                } else {
                    res.status(HttpStatus.BAD_REQUEST).send({
                        message: "Username or password invalid!"
                    })
                }
            }
    
        } catch (error) {
            res.status(HttpStatus.BAD_REQUEST).send({
                message: "BAD GATEWAY REQUEST",
                error: error.message
            })
        }
    }

    async addWriterPublisher(req, res){
        const decoded = jwt.decode(req.headers['auth-token'], secret);
        try {
            const publisherExist = await PENERBIT.findOne({
                where: {
                    [Op.and] : [
                        { id: decoded.userid }
                    ]
                }
            })
            if(!publisherExist){
                res.status(HttpStatus.BAD_REQUEST).send({
                    message: "failed"
                })
            } else {
                bcrypt.hash(defaultPassword, 10, async (err, hash) => {
                    if (err) throw err;
                    try {
                        await publisherExist.createWriter({ email: req.body.email, password: `${hash}`, roles: "admin"})
                        .then((success) => {
                            res.status(HttpStatus.OK).send({
                                data: success
                            })
                        })
                    } catch (error) {

                        res.status(HttpStatus.BAD_REQUEST).send({
                            message: error.message,
                        });
                    }
                })
            }
        } catch (error) {
            res.status(HttpStatus.BAD_GATEWAY).send({
                message: error.message
            })
        }
    }

    async addEditorWriter(req, res){
        const decoded = jwt.decode(req.headers['auth-token'], secret);
        try {
            const writerExist = await PENULIS.findOne({
                where: {
                    [Op.and] : [
                        { id: req.query.writerId }
                    ]
                }
            })
            if(!writerExist){
                res.status(HttpStatus.BAD_REQUEST).send({
                    message: "failed"
                })
            } else {
                bcrypt.hash(defaultPassword, 10, async (err, hash) => {
                    if (err) throw err;
                    try {
                        await writerExist.createEditor({ email: req.body.email, password: `${hash}`, roles: "admin"})
                        .then((success) => {
                            res.status(HttpStatus.OK).send({
                                data: success
                            })
                        })
                    } catch (error) {

                        res.status(HttpStatus.BAD_REQUEST).send({
                            message: error.message,
                        });
                    }
                })
            }
        } catch (error) {
            res.status(HttpStatus.BAD_GATEWAY).send({
                message: error.message
            })
        }
    }

    async getAllWriters(req, res){
        const decoded = jwt.decode(req.headers['auth-token'], secret);
        try {
            const publisherExist = await PENERBIT.findOne({
                where: {
                    [Op.and] : [
                        { id: decoded.userid }
                    ]
                }
            })
            if(!publisherExist){
                res.status(HttpStatus.BAD_REQUEST).send({
                    message: "failed"
                })
            } else {
                const getListExist = await publisherExist.getWriters();
                if(!getListExist){
                    res.status(HttpStatus.NOT_FOUND).send({
                        message: "Tidak ditemukan data!"
                    })
                } else {
                    res.status(HttpStatus.OK).send({
                        data: getListExist
                    })
                }
            }
        } catch (error) {
            res.status(HttpStatus.BAD_GATEWAY).send({
                message: error.message
            })
        }
    }

    async getAllEditorsByWriter(req, res){
        try {
            const writerExist = await PENULIS.findOne({
                where: {
                    [Op.and] : [
                        { id: req.query.writerId }
                    ]
                }
            })
            if(!writerExist){
                res.status(HttpStatus.BAD_REQUEST).send({
                    message: "failed"
                })
            } else {
                const getListExist = await writerExist.getEditors();
                if(!getListExist){
                    res.status(HttpStatus.NOT_FOUND).send({
                        message: "Tidak ditemukan data!"
                    })
                } else {
                    res.status(HttpStatus.OK).send({
                        data: getListExist
                    })
                }
            }
        } catch (error) {
            res.status(HttpStatus.BAD_GATEWAY).send({
                message: error.message
            })
        }
    }

    async getAllEditors(req, res){
        try {
            const getListExist = await EDITOR.findAll();
            if(!getListExist){
                res.status(HttpStatus.NOT_FOUND).send({
                    message: "Tidak ditemukan data!"
                })
            } else {
                res.status(HttpStatus.OK).send({
                    data: getListExist
                })
            }
        } catch (error) {
            res.status(HttpStatus.BAD_GATEWAY).send({
                message: error.message
            })
        }
    }

    async deleteWriterById(req, res){
        const decoded = jwt.decode(req.headers['auth-token'], secret);
        try {
            const publisherExist = await PENERBIT.findOne({
                where: {
                    [Op.and] : [
                        { id: decoded.userid }
                    ]
                }
            })
            if(!publisherExist){
                res.status(HttpStatus.BAD_REQUEST).send({
                    message: "failed"
                })
            } else {
                const getListExist = await PENULIS.destroy({
                   where: {
                    [Op.and] : [
                        { id: req.query.id },
                        { publisherId: decoded.userid },
                    ]
                   } 
                });
                if(!getListExist){
                    res.status(HttpStatus.NOT_FOUND).send({
                        message: "Tidak ditemukan data!"
                    })
                } else {
                    res.status(HttpStatus.OK).send({
                        data: getListExist
                    })
                }
            }
        } catch (error) {
            res.status(HttpStatus.BAD_GATEWAY).send({
                message: error.message
            })
        }
    }

    async suspenseWriter(req, res){
        const decoded = jwt.decode(req.headers['auth-token'], secret);
        try {
            const publisherExist = await PENERBIT.findOne({
                where: {
                    [Op.and] : [
                        { id: decoded.userid }
                    ]
                }
            })
            if(!publisherExist){
                res.status(HttpStatus.BAD_REQUEST).send({
                    message: "failed"
                })
            } else {
                const getListExist = await PENULIS.update({ isSuspended: req.query.status },{
                   where: {
                    [Op.and] : [
                        { id: req.query.id },
                        { publisherId: decoded.userid },
                    ]
                   } 
                });
                if(!getListExist){
                    res.status(HttpStatus.NOT_FOUND).send({
                        message: "Tidak ditemukan data!"
                    })
                } else {
                    if(new Set(getListExist).has(1)){
                        res.status(HttpStatus.OK).send({
                            message: "success"
                        })
                     } else {
                        res.status(HttpStatus.OK).send({
                            message: "failed"
                        })
                     }
                }
            }
        } catch (error) {
            res.status(HttpStatus.BAD_GATEWAY).send({
                message: error.message
            })
        }
    }

    async banWriter(req, res){
        const decoded = jwt.decode(req.headers['auth-token'], secret);
        try {
            const publisherExist = await PENERBIT.findOne({
                where: {
                    [Op.and] : [
                        { id: decoded.userid }
                    ]
                }
            })
            if(!publisherExist){
                res.status(HttpStatus.BAD_REQUEST).send({
                    message: "failed"
                })
            } else {
                const getListExist = await PENULIS.update({ isBanned: req.query.status },{
                   where: {
                    [Op.and] : [
                        { id: req.query.id },
                        { publisherId: decoded.userid },
                    ]
                   } 
                });
                if(!getListExist){
                    res.status(HttpStatus.NOT_FOUND).send({
                        message: "Tidak ditemukan data!"
                    })
                } else {
                    if(new Set(getListExist).has(1)){
                        res.status(HttpStatus.OK).send({
                            message: "success"
                        })
                     } else {
                        res.status(HttpStatus.OK).send({
                            message: "failed"
                        })
                     }
                }
            }
        } catch (error) {
            res.status(HttpStatus.BAD_GATEWAY).send({
                message: error.message
            })
        }
    }

    async suspenseEditor(req, res){
        try {
            const getListExist = await EDITOR.update({ isSuspended: req.query.status },{
                where: {
                 [Op.and] : [
                     { id: req.query.id },
                     { writerId: req.query.writerId },
                 ]
                } 
             });
             if(!getListExist){
                 res.status(HttpStatus.NOT_FOUND).send({
                     message: "Tidak ditemukan data!"
                 })
             } else {
                if(new Set(getListExist).has(1)){
                    res.status(HttpStatus.OK).send({
                        message: "success"
                    })
                 } else {
                    res.status(HttpStatus.OK).send({
                        message: "failed"
                    })
                 }
             }
        } catch (error) {
            res.status(HttpStatus.BAD_GATEWAY).send({
                message: error.message
            })
        }
    }

    async suspenseAdmin(req, res){
        try {
            const getListExist = await PENERBIT.update({ isSuspended: req.query.status },{
                where: {
                 [Op.and] : [
                     { id: req.query.id },
                     { roles: "admin" },
                 ]
                } 
             });
             if(!getListExist){
                 res.status(HttpStatus.NOT_FOUND).send({
                     message: "Tidak ditemukan data!"
                 })
             } else {
                if(new Set(getListExist).has(1)){
                    res.status(HttpStatus.OK).send({
                        message: "success"
                    })
                 } else {
                    res.status(HttpStatus.OK).send({
                        message: "failed"
                    })
                 }
             }
        } catch (error) {
            res.status(HttpStatus.BAD_GATEWAY).send({
                message: error.message
            })
        }
    }
    async bannedAdmin(req, res){
        try {
            const getListExist = await PENERBIT.update({ isBanned: req.query.status },{
                where: {
                 [Op.and] : [
                     { id: req.query.id },
                     { roles: "admin" },
                 ]
                } 
             });
             if(!getListExist){
                 res.status(HttpStatus.NOT_FOUND).send({
                     message: "Tidak ditemukan data!"
                 })
             } else {
                if(new Set(getListExist).has(1)){
                    res.status(HttpStatus.OK).send({
                        message: "success"
                    })
                 } else {
                    res.status(HttpStatus.OK).send({
                        message: "failed"
                    })
                 }
             }
        } catch (error) {
            res.status(HttpStatus.BAD_GATEWAY).send({
                message: error.message
            })
        }
    }

    async suspenseAdminUserRoles(req, res){
        try {
            const getListExist = await PENERBIT.update({ isSuspended: req.query.status },{
                where: {
                 [Op.and] : [
                     { id: req.query.id },
                     { roles: "user" },
                 ]
                } 
             });
             if(!getListExist){
                 res.status(HttpStatus.NOT_FOUND).send({
                     message: "Tidak ditemukan data!"
                 })
             } else {
                 if(new Set(getListExist).has(1)){
                    res.status(HttpStatus.OK).send({
                        message: "success"
                    })
                 } else {
                    res.status(HttpStatus.OK).send({
                        message: "failed"
                    })
                 }
                 
             }
        } catch (error) {
            res.status(HttpStatus.BAD_GATEWAY).send({
                message: error.message
            })
        }
    }
    async bannedAdminUserRoles(req, res){
        try {
            const getListExist = await PENERBIT.update({ isBanned: req.query.status },{
                where: {
                 [Op.and] : [
                     { id: req.query.id },
                     { roles: "user" },
                 ]
                } 
             });
             if(!getListExist){
                 res.status(HttpStatus.NOT_FOUND).send({
                     message: "Tidak ditemukan data!"
                 })
             } else {
                if(new Set(getListExist).has(1)){
                    res.status(HttpStatus.OK).send({
                        message: "success"
                    })
                 } else {
                    res.status(HttpStatus.OK).send({
                        message: "failed"
                    })
                 }
             }
        } catch (error) {
            res.status(HttpStatus.BAD_GATEWAY).send({
                message: error.message
            })
        }
    }

    async banEditor(req, res){
        try {
            const getListExist = await EDITOR.update({ isBanned: req.query.status },{
                where: {
                 [Op.and] : [
                     { id: req.query.id },
                     { writerId: req.query.writerId },
                 ]
                } 
             });
             if(!getListExist){
                 res.status(HttpStatus.NOT_FOUND).send({
                     message: "Tidak ditemukan data!"
                 })
             } else {
                if(new Set(getListExist).has(1)){
                    res.status(HttpStatus.OK).send({
                        message: "success"
                    })
                 } else {
                    res.status(HttpStatus.OK).send({
                        message: "failed"
                    })
                 }
             }
        } catch (error) {
            res.status(HttpStatus.BAD_GATEWAY).send({
                message: error.message
            })
        }
    }

    async getAdminList(req, res){
        try {
            const getListExist = await PENERBIT.findAll({
                where: {
                 [Op.and] : [
                     { roles: "admin" }
                 ]
                } 
             });
             if(!getListExist){
                 res.status(HttpStatus.NOT_FOUND).send({
                     message: "Tidak ditemukan data!"
                 })
             } else {
                 res.status(HttpStatus.OK).send({
                     data: getListExist
                 })
             }
        } catch (error) {
            res.status(HttpStatus.BAD_GATEWAY).send({
                message: error.message
            })
        }
    }

    async getAdminListRoleUsers(req, res){
        try {
            const getListExist = await PENERBIT.findAll({
                where: {
                 [Op.and] : [
                     { roles: "user" }
                 ]
                } 
             });
             if(!getListExist){
                 res.status(HttpStatus.NOT_FOUND).send({
                     message: "Tidak ditemukan data!"
                 })
             } else {
                 res.status(HttpStatus.OK).send({
                     data: getListExist
                 })
             }
        } catch (error) {
            res.status(HttpStatus.BAD_GATEWAY).send({
                message: error.message
            })
        }
    }


    async deleteAdminByRolesEqualUser(req, res){
        try {
            const getOne = await PENERBIT.destroy({
                where: {
                 [Op.and] : [
                     { id: req.query.id },
                 ]
                } 
             });
             if(!getOne){
                 res.status(HttpStatus.NOT_FOUND).send({
                     message: "Tidak ditemukan data!"
                 })
             } else {
                 res.status(HttpStatus.OK).send({
                     data: getOne
                 })
             }
        } catch (error) {
            res.status(HttpStatus.BAD_GATEWAY).send({
                message: error.message
            })
        }
    }

    async addNewPublisherAdminRoles(req, res){
        try {
            bcrypt.hash(req.body.password, 10, async (err, hash) => {
                if (err) throw err;
                try {
                    try {
                        const getAdminExist = await ADMIN.findOne({
                            attributes: ["id"],
                            where: {
                                roles: "admin"
                            }
                        })
                        if(!getAdminExist){
                            res.status(HttpStatus.BAD_REQUEST).send({
                                message: "failed"
                            })
                           
                        } else {
                           
                            const created = await getAdminExist.createPublisher({
                                email: req.body.email, 
                                password: `${hash}`, 
                                roles: "admin", 
                                adminId: getAdminExist.id
                             });
                             if(!created){
                                 res.status(HttpStatus.BAD_REQUEST).send({
                                     message: "failed"
                                 })
                             } else {
                                 res.status(HttpStatus.OK).send({
                                     success: created
                                 })
                             }
                        }
                    } catch (error) {
                        res.status(HttpStatus.BAD_REQUEST).send({
                            message: error.message,
                        });
                    }
                   
                } catch (error) {

                    res.status(HttpStatus.BAD_REQUEST).send({
                        message: error.message,
                    });
                }
            })
        } catch (error) {
            res.status(HttpStatus.BAD_GATEWAY).send({
                message: error.message
            })
        }
    }

    async AddAdminRoleUserFromPublisher(req, res){
        try {
           const adminSuperExist = await ADMIN.findOne({
               where: {
                   roles: "super"
               }
           })
           if(!adminSuperExist){
                res.status(HttpStatus.CONFLICT).send({
                    message: "There is no record admin super"
                })
           } else {
            bcrypt.hash(req.body.password, 10, async (err, hash) => {
                if (err) throw err;
                try {
                    try {
                        const created = await adminSuperExist.createPublisher({
                            email: req.body.email, 
                            password: `${hash}`, 
                            roles: "user",
                         });
                         if(!created){
                             res.status(HttpStatus.BAD_REQUEST).send({
                                 message: "failed"
                             })
                         } else {
                             res.status(HttpStatus.OK).send({
                                 success: created
                             })
                         }
                    } catch (error) {
                        res.status(HttpStatus.BAD_REQUEST).send({
                            message: error.message,
                        });
                    }
                   
                } catch (error) {

                    res.status(HttpStatus.BAD_REQUEST).send({
                        message: error.message,
                    });
                }
            })
           }
        } catch (error) {
            res.status(HttpStatus.BAD_GATEWAY).send({
                message: error.message
            })
        }
    }
}
