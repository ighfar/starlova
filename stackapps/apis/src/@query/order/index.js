import { Op } from 'sequelize';
import jwt from 'jsonwebtoken';
import bcrypt from 'bcryptjs';
import HttpStatus from '../../utils/HttpStatus';
import { BOOK, ORDER } from '../../@models';
import { secret, emailFrom, smtpOptions, superAdmin } from '../../@config/config';
import MiddlewareValidation from '../middleware';
import nodemailer from 'nodemailer';
import Joi from 'joi';
import store from 'store';
import { btoa as encode, atob as decode64 } from '../../utils/base64';

const midtransClient = require('midtrans-client');
// Create Core API instance
let coreApi = new midtransClient.CoreApi({
        isProduction : false,
        serverKey : 'SB-Mid-server-M_euV-nhxdnWc_omqQVi9jWj',
        clientKey : 'SB-Mid-client-AOoppNRbjGUvBIrE'
    });

module.exports = {

      async charge(req, res) {

            const orderExist = await ORDER.findAll();

          coreApi.charge(reg.body).then((chargeResponse)=>{
            var dataOrder = {
                id : chargeResponse.order_id,
                title : reg.body.title,
                nama : reg.body.nama,
                email : reg.body.email,
                response_midtrans : reg.body.JSON.stringify(chargeResponse)
            }

            ORDER.create(dataOrder).then( data=>{

                res.json({
                    status : true,
                    message: 'data berhasil ditambah',
                    data: orderExist

                });
            }).catch( err=>{
                 res.json({
                    status : false,
                    message: 'data gagal ditambah',
                    data: []

                });

            });

      
        }).catch((e)=>{
        console.log('Error occured:',e.message);
    });

    },
    
    async deleteOrder(req, res) {
        try {
            const order = await ORDER.findByPk(req.params.orderId)
            if (!order) {
                return res.status(403).send({
                    error: 'No order to delete.'
                })
            }
            await order.destroy()
            res.send(order)

        } catch (err) {
            res.status(500).send({
                error: 'An error occured when trying to delete an order item.'
            })
        }
    }
}
