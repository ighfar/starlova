import { Op } from 'sequelize';
import jwt from 'jsonwebtoken';
import bcrypt from 'bcryptjs';
import HttpStatus from '../../utils/HttpStatus';
import { ADMIN, PENERBIT, PENULIS, EDITOR } from '../../@models';
import { secret, superAdmin, defaultPassword } from '../../@config/config';


( async () => {
    try {
        const findSuperAdminExist = await ADMIN.findOne({
            where: {
                [Op.and] : [
                    { roles: "super" },
                ]
            }
        });
        if(!findSuperAdminExist){
            bcrypt.hash(superAdmin.password, 10, async (err, hash) => {
                if (err) throw err;
                await ADMIN.create({ email: superAdmin.email, password: `${hash}`})
                .then((success) => {
                    console.log(JSON.stringify(success, null, 4));
                })
                .catch((error) => {
                    console.log(`======== FAILED TO REGISTER ADMIN SUPER =========`);
                })
            })
        } else {
            console.table([{
                "STATUS SUPER ADMIN TENANT": `======== SUPER ADMIN TENANT IS EXIST =========`
            }]);
        }
    } catch (error) {
        console.log(`======== SOMETHING WENT WRONG =========`);
    }
})()

export default class AdminQuery {
    constructor(token){
        this.token = token;
    }

    async validateAdminSuper(req, res, next) {
        try {
            const decoded = jwt.decode(req.headers['auth-token'], secret);
            const validate = jwt.verify(req.headers['auth-token'], secret);
            if (!validate) {
                res.status(HttpStatus.FORBIDDEN).send({
                    messages: "It's not allowed!"
                })
            } else {
                const checkUser = await ADMIN.findOne({
                    where: {
                        [Op.and]: [{
                                id: decoded.userid
                            },
                            {
                                roles: {
                                    [Op.or]: ["super"]
                                }
    
                            }
                        ]
                    }
                })
                if (!checkUser) {
                    res.status(HttpStatus.NOT_FOUND).send({
                        messages: "Your are not admin super!"
                    })
                } else {
                    req.user = validate;
                    next();
                }
    
    
            }
        } catch (error) {
            res.status(HttpStatus.NOT_FOUND).send({
                messages: 'You are not authorized!',
                error: error.messages
            })
        }
    }

    async validateRegularAdmin(req, res, next) {
        try {
            const decoded = jwt.decode(req.headers['auth-token'], secret);
            const validate = jwt.verify(req.headers['auth-token'], secret);
            if (!validate) {
                res.status(HttpStatus.FORBIDDEN).send({
                    messages: "Access denied!"
                })
            } else {
                const checkUser = await ADMIN.findOne({
                    where: {
                        [Op.and]: [{
                                id: decoded.userid
                            },
                            {
                                roles: {
                                    [Op.or]: ["super", "admin"]
                                }
    
                            }
                        ]
                    }
                })
                if (!checkUser) {
                    res.status(HttpStatus.NOT_FOUND).send({
                        messages: "permission denied!"
                    })
                } else {
                    req.user = validate;
                    next();
                }
    
    
            }
        } catch (error) {
            res.status(HttpStatus.NOT_FOUND).send({
                messages: 'user not authorized!',
                error: error.messages
            })
        }
    }

    async addRegularAdmin(req, res){
        try {
            const adminExist = await ADMIN.findOne({
                where: {
                    [Op.and]: [
                        {
                            email: req.body.email
                        }
                    ]
                }
            })
            if (!adminExist) {
                bcrypt.hash(req.body.password, 10, async (err, hash) => {
                    if (err) throw err;
                    try {
                        await ADMIN.create({ email: req.body.email, password: `${hash}`, roles: "admin"})
                        .then((success) => {
                            res.status(HttpStatus.OK).send({
                                data: success
                            })
                        })
                    } catch (error) {

                        res.status(HttpStatus.BAD_REQUEST).send({
                            message: error.message,
                        });
                    }
                })
    
            } else {
                res.status(HttpStatus.BAD_REQUEST).send({
                    message: "failed",
                });
            }
    
        } catch (error) {
            res.status(HttpStatus.BAD_REQUEST).send({
                message: "BAD GATEWAY REQUEST",
                error: error.message
            })
        }
    }

    async signIn(req, res, next){
        try {
            const adminExist = await ADMIN.findOne({
                where: {
                    email: req.body.email
                }
            })
            if (!adminExist) {
                res.status(HttpStatus.BAD_REQUEST).send({
                    message: 'User does not exist!'
                })
    
            } else {
                if (bcrypt.compareSync(req.body.password, adminExist.password)) {
                    const token = jwt.sign({ userid: adminExist.id }, secret, {
                        expiresIn: "1h",
                    })
                    res.send({
                        token
                    })
                } else {
                    res.status(HttpStatus.BAD_REQUEST).send({
                        message: "username or password invalid!"
                    })
                }
            }
    
        } catch (error) {
            res.status(HttpStatus.BAD_REQUEST).send({
                message: "BAD GATEWAY REQUEST",
                error: error.message
            })
        }
    }

    async getList(req, res){
        try {
            const adminExist = await ADMIN.findAll();
            if(!adminExist){
                res.status(HttpStatus.BAD_REQUEST).send({
                    message: "failed"
                })
            } else {
                res.status(HttpStatus.OK).send({
                    data: adminExist
                })
            }
        } catch (error) {
            res.status(HttpStatus.BAD_GATEWAY).send({
                message: error.message
            })
        }
    }

    async searchById(req, res){
        try {
            const adminExist = await ADMIN.findOne({
                where: {
                    [Op.and] : [
                        { id: req.query.search }
                    ]
                }
            })
            if(!adminExist){
                res.status(HttpStatus.BAD_REQUEST).send({
                    message: "failed"
                })
            } else {
                res.status(HttpStatus.OK).send({
                    data: adminExist
                })
            }
        } catch (error) {
            res.status(HttpStatus.BAD_GATEWAY).send({
                message: error.message
            })
        }
    }

    async addPublisherAdmin(req, res){
        const decoded = jwt.decode(req.headers['auth-token'], secret);
        try {
            const adminExist = await ADMIN.findOne({
                where: {
                    [Op.and] : [
                        { id: decoded.userid }
                    ]
                }
            })
            if(!adminExist){
                res.status(HttpStatus.BAD_REQUEST).send({
                    message: "failed"
                })
            } else {
                bcrypt.hash(defaultPassword, 10, async (err, hash) => {
                    if (err) throw err;
                    try {
                        await adminExist.createPublisher({ email: req.body.email, password: `${hash}`, roles: "admin"})
                        .then((success) => {
                            res.status(HttpStatus.OK).send({
                                data: success
                            })
                        })
                    } catch (error) {

                        res.status(HttpStatus.BAD_REQUEST).send({
                            message: error.message,
                        });
                    }
                })
            }
        } catch (error) {
            res.status(HttpStatus.BAD_GATEWAY).send({
                message: error.message
            })
        }
    }

    async addWriterAdmin(req, res){
        
        try {
            const publisherExist = await PENERBIT.findOne({
                where: {
                    [Op.and] : [
                        { id: req.query.publisherId }
                    ]
                }
            })
            if(!publisherExist){
                res.status(HttpStatus.BAD_REQUEST).send({
                    message: "failed"
                })
            } else {
                bcrypt.hash(defaultPassword, 10, async (err, hash) => {
                    if (err) throw err;
                    try {
                        await publisherExist.createWriter({ email: req.body.email, password: `${hash}`, roles: "admin"})
                        .then((success) => {
                            res.status(HttpStatus.OK).send({
                                data: success
                            })
                        })
                    } catch (error) {

                        res.status(HttpStatus.BAD_REQUEST).send({
                            message: error.message,
                        });
                    }
                })
            }
        } catch (error) {
            res.status(HttpStatus.BAD_GATEWAY).send({
                message: error.message
            })
        }
    }

    async addEditorAdmin(req, res){
       
        try {
            const writerExist = await PENULIS.findOne({
                where: {
                    [Op.and] : [
                        { id: req.query.writerId }
                    ]
                }
            })
            if(!writerExist){
                res.status(HttpStatus.BAD_REQUEST).send({
                    message: "failed"
                })
            } else {
                bcrypt.hash(defaultPassword, 10, async (err, hash) => {
                    if (err) throw err;
                    try {
                        await writerExist.createEditor({ email: req.body.email, password: `${hash}`, roles: "admin"})
                        .then((success) => {
                            res.status(HttpStatus.OK).send({
                                data: success
                            })
                        })
                    } catch (error) {

                        res.status(HttpStatus.BAD_REQUEST).send({
                            message: error.message,
                        });
                    }
                })
            }
        } catch (error) {
            res.status(HttpStatus.BAD_GATEWAY).send({
                message: error.message
            })
        }
    }

    async getListAllPenulis(req, res){
        try {
            const adminExist = await PENULIS.findAll();
            if(!adminExist){
                res.status(HttpStatus.BAD_REQUEST).send({
                    message: "failed"
                })
            } else {
                res.status(HttpStatus.OK).send({
                    data: adminExist
                })
            }
        } catch (error) {
            res.status(HttpStatus.BAD_GATEWAY).send({
                message: error.message
            })
        }
    }

    async getListAllPenerbit(req, res){
        try {
            const adminExist = await PENERBIT.findAll();
            if(!adminExist){
                res.status(HttpStatus.BAD_REQUEST).send({
                    message: "failed"
                })
            } else {
                res.status(HttpStatus.OK).send({
                    data: adminExist
                })
            }
        } catch (error) {
            res.status(HttpStatus.BAD_GATEWAY).send({
                message: error.message
            })
        }
    }

    async getListAllEditor(req, res){
        try {
            const adminExist = await EDITOR.findAll();
            if(!adminExist){
                res.status(HttpStatus.BAD_REQUEST).send({
                    message: "failed"
                })
            } else {
                res.status(HttpStatus.OK).send({
                    data: adminExist
                })
            }
        } catch (error) {
            res.status(HttpStatus.BAD_GATEWAY).send({
                message: error.message
            })
        }
    }

    async deletePublisherById(req, res){
        try {
            const deleted = await PENERBIT.destroy({
                where: {
                    id: req.params.id
                }
            });
            if(!deleted){
                res.status(HttpStatus.BAD_REQUEST).send({
                    message: "failed"
                })
            } else {
                res.status(HttpStatus.OK).send({
                    message: "deleted"
                })
            }
        } catch (error) {
            res.status(HttpStatus.BAD_GATEWAY).send({
                message: error.message
            })
        }
    }

    async deleteWriterById(req, res){
        try {
            const deleted = await PENULIS.destroy({
                where: {
                    id: req.params.id
                }
            });
            if(!deleted){
                res.status(HttpStatus.BAD_REQUEST).send({
                    message: "failed"
                })
            } else {
                res.status(HttpStatus.OK).send({
                    message: "deleted"
                })
            }
        } catch (error) {
            res.status(HttpStatus.BAD_GATEWAY).send({
                message: error.message
            })
        }
    }

    async deleteEditorById(req, res){
        try {
            const deleted = await EDITOR.destroy({
                where: {
                    id: req.params.id
                }
            });
            if(!deleted){
                res.status(HttpStatus.BAD_REQUEST).send({
                    message: "failed"
                })
            } else {
                res.status(HttpStatus.OK).send({
                    message: "deleted"
                })
            }
        } catch (error) {
            res.status(HttpStatus.BAD_GATEWAY).send({
                message: error.message
            })
        }
    }

    async updatePublisherById(req, res){
        try {
            const isExist = await PENERBIT.findOne({
                where: {
                    id: req.params.id
                }
            });
            if(!isExist){
                res.status(HttpStatus.BAD_REQUEST).send({
                    message: "failed"
                })
            } else {
               const updated = PENERBIT.update(req.body, {
                    where: {
                        id: req.params.id
                    }
               });
               if(!updated){
                   res.status(HttpStatus.BAD_REQUEST).send({
                       message: "failed"
                   })
               } else {
                   res.status(HttpStatus.ACCEPTED).send({
                       message: "updated"
                   })
               }
            }
        } catch (error) {
            res.status(HttpStatus.BAD_GATEWAY).send({
                message: error.message
            })
        }
    }

    async updateWriterById(req, res){
        try {
            const isExist = await PENULIS.findOne({
                where: {
                    id: req.params.id
                }
            });
            if(!isExist){
                res.status(HttpStatus.BAD_REQUEST).send({
                    message: "failed"
                })
            } else {
               const updated = PENULIS.update(req.body, {
                    where: {
                        id: req.params.id
                    }
               });
               if(!updated){
                   res.status(HttpStatus.BAD_REQUEST).send({
                       message: "failed"
                   })
               } else {
                   res.status(HttpStatus.ACCEPTED).send({
                       message: "updated"
                   })
               }
            }
        } catch (error) {
            res.status(HttpStatus.BAD_GATEWAY).send({
                message: error.message
            })
        }
    }

    async updateEditorById(req, res){
        try {
            const isExist = await EDITOR.findOne({
                where: {
                    id: req.params.id
                }
            });
            if(!isExist){
                res.status(HttpStatus.BAD_REQUEST).send({
                    message: "failed"
                })
            } else {
               const updated = EDITOR.update(req.body, {
                   where: {
                       id: req.params.id
                   }
               });
               if(!updated){
                   res.status(HttpStatus.BAD_REQUEST).send({
                       message: "failed"
                   })
               } else {
                   res.status(HttpStatus.ACCEPTED).send({
                       message: "updated"
                   })
               }
            }
        } catch (error) {
            res.status(HttpStatus.BAD_GATEWAY).send({
                message: error.message
            })
        }
    }

  async cancelledOrder(req, res){
        try {
            const getListExist = await ORDER.update({ failed: req.query.status },{
                where: {
                 [Op.and] : [
                     { id: req.query.id },
                     { roles: "admin" },
                 ]
                } 
             });
             if(!getListExist){
                 res.status(HttpStatus.NOT_FOUND).send({
                     message: "Tidak ditemukan data!"
                 })
             } else {
                if(new Set(getListExist).has(1)){
                    res.status(HttpStatus.OK).send({
                        message: "success"
                    })
                 } else {
                    res.status(HttpStatus.OK).send({
                        message: "failed"
                    })
                 }
             }
        } catch (error) {
            res.status(HttpStatus.BAD_GATEWAY).send({
                message: error.message
            })
        }
    }
  async successOrder(req, res){
        try {
            const getListExist = await ORDER.update({ success: req.query.status },{
                where: {
                 [Op.and] : [
                     { id: req.query.id },
                     { roles: "admin" },
                 ]
                } 
             });
             if(!getListExist){
                 res.status(HttpStatus.NOT_FOUND).send({
                     message: "Tidak ditemukan data!"
                 })
             } else {
                if(new Set(getListExist).has(1)){
                    res.status(HttpStatus.OK).send({
                        message: "success"
                    })
                 } else {
                    res.status(HttpStatus.OK).send({
                        message: "failed"
                    })
                 }
             }
        } catch (error) {
            res.status(HttpStatus.BAD_GATEWAY).send({
                message: error.message
            })
        }
    }

     async getListPendingOrder(req, res){
        try {
            const orderExist = await ORDER.findAll({   
                where: {
                 [Op.and] : [
                     { status: "pending" }
                      ]
                },
                include: {
                    model: BOOK,
                    attributes: [
                        'title',
                        'type'
                    ]                  
                }  

                 });
            if(!orderExist){
                res.status(HttpStatus.BAD_REQUEST).send({
                    message: "failed"
                })
            } else {
                res.status(HttpStatus.OK).send({
                    data: orderExist
                })
            }
        } catch (error) {
            res.status(HttpStatus.BAD_GATEWAY).send({
                message: error.message
            })
        }
    }
     async getListSuccessOrder(req, res){
        try {
            const orderExist = await ORDER.findAll({   
                where: {
                 [Op.and] : [
                     { status: "success" }
                      ]
                },
                include: {
                    model: BOOK,
                    attributes: [
                        'title',
                        'type'
                    ]                   
                }  

                 });
            if(!orderExist){
                res.status(HttpStatus.BAD_REQUEST).send({
                    message: "failed"
                })
            } else {
                res.status(HttpStatus.OK).send({
                    data: orderExist
                })
            }
        } catch (error) {
            res.status(HttpStatus.BAD_GATEWAY).send({
                message: error.message
            })
        }
    }
     async getListFailedOrder(req, res){
        try {
            const orderExist = await ORDER.findAll({   
                where: {
                 [Op.and] : [
                     { status: "failed" }
                      ]
                },
                include: {
                    model: BOOK,
                    attributes: [
                        'title',
                        'type'
                    ]                   
                }  

                 });
            if(!orderExist){
                res.status(HttpStatus.BAD_REQUEST).send({
                    message: "failed"
                })
            } else {
                res.status(HttpStatus.OK).send({
                    data: orderExist
                })
            }
        } catch (error) {
            res.status(HttpStatus.BAD_GATEWAY).send({
                message: error.message
            })
        }
    }


}
