import { Op } from 'sequelize';
import jwt from 'jsonwebtoken';
import bcrypt from 'bcryptjs';
import HttpStatus from '../../utils/HttpStatus';
import { BOOK } from '../../@models';
import { secret, emailFrom, smtpOptions, superAdmin } from '../../@config/config';
import MiddlewareValidation from '../middleware';
import Joi from 'joi';
import store from 'store';
import { btoa as encode, atob as decode64 } from '../../utils/base64';

module.exports = {
  
    async getList(req, res){
        try {
            const bookExist = await BOOK.findAll();
            if(!bookExist){
                res.status(HttpStatus.BAD_REQUEST).send({
                    message: "failed"
                })
            } else {
                res.status(HttpStatus.OK).send({
                    data: bookExist
                })
            }
        } catch (error) {
            res.status(HttpStatus.BAD_GATEWAY).send({
                message: error.message
            })
        }
    },

    async bookById(req, res){
        try {
            const bookExist = await BOOK.findOne({
                where: {
                    [Op.and] : [
                        { id: req.query.bookId }
                    ]
                }
            })
            if(!bookExist){
                res.status(HttpStatus.BAD_REQUEST).send({
                    message: "failed"
                })
            } else {
                res.status(HttpStatus.OK).send({
                    data: bookExist
                })
            }
        } catch (error) {
            res.status(HttpStatus.BAD_GATEWAY).send({
                message: error.message
            })
        }
    }
}
