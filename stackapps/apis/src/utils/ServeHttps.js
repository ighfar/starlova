import {
  readFileSync
} from 'fs';
import {
  createServer
} from 'https';
import {
  join
} from 'path';

export default function ServeHttps(appInstance, path) {
  var key = readFileSync(join(__dirname, `../${path}`, 'server.key'));
  var cert = readFileSync(join(__dirname, `../${path}`, 'server.cert'));
  var options = {
    key: key,
    cert: cert
  };
  return createServer(options, appInstance);
}