export default function ArrayToObject(data) {
    if(Array.isArray(data)){
       const newObject =  data.reduce((a, v) => ({
            ...a,
            [v]: v
        }), {})
        return newObject;
    } else {
        console.log("Parameter data tidak valid !")
    }
}