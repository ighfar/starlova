import { Sequelize } from "sequelize";
import db from '../../@config/db';
const { DataTypes } = Sequelize;

export const SALES = db.define('sales', {

    quantity: {
        type: DataTypes.INTEGER,
        defaultValue: 0
    },

    amount: {
        type: DataTypes.BIGINT,
        defaultValue: 0
    },

    status: {
        type: DataTypes.ENUM('process', 'pending', 'success', 'failed'),
        defaultValue: "process",
        allowNull: false
    }
})