import { Sequelize } from "sequelize";
import db from '../../@config/db';
const { DataTypes } = Sequelize;

export const EARNING = db.define('earning', {
    day: {
        type: DataTypes.BIGINT,
        defaultValue: 0
    },
    month: {
        type: DataTypes.BIGINT,
        defaultValue: 0
    },
    year: {
        type: DataTypes.BIGINT,
        defaultValue: 0
    }
})