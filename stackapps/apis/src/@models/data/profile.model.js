import { Sequelize } from "sequelize";
import db from '../../@config/db';
const { DataTypes } = Sequelize;

export const PROFILE = db.define('profile', {
    avatar: {
        type: DataTypes.STRING,
    },
    firstName: {
        type: DataTypes.STRING,
        allowNull: false
    },

    lastName: {
        type: DataTypes.STRING,
        allowNull: false
    },
    address: {
        type: DataTypes.STRING,
        allowNull: true
    },

    saldo: {
        type: DataTypes.BIGINT,
        defaultValue: 0,
    },

    stpub: {
        type: DataTypes.UUID,
        allowNull: false
    },
})