import { Sequelize } from "sequelize";
import db from '../../@config/db';
const { DataTypes } = Sequelize;

export const ORDER = db.define('order', {
    quantity: {
        type: DataTypes.INTEGER,
        defaultValue: 0
    },
    amount: {
        type: DataTypes.BIGINT,
        defaultValue: 0
    },

             title: {
        type: DataTypes.STRING,
        allowNull: false
    },

      name: {
        type: DataTypes.STRING,
        allowNull: false
    },

         email: {
        type: DataTypes.STRING,
        allowNull: false
    },

       response_midtrans: {
        type: DataTypes.TEXT,
        allowNull: false
    },

    status: {
        type: DataTypes.ENUM('process', 'pending', 'success', 'failed'),
        defaultValue: "process",
        allowNull: false
    },

    id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true,
        allowNull: false
    }
})