import { Sequelize } from "sequelize";
import db from '../../@config/db';
const { DataTypes } = Sequelize;

export const DOCUMENT = db.define('document', {
    bankName: {
        type: DataTypes.STRING,
        allowNull: true
    },

    bankAccount: {
        type: DataTypes.STRING(21),
        allowNull: true
    },

    holderName: {
        type: DataTypes.STRING(35),
        allowNull: true
    },

    govermentId: {
        type: DataTypes.STRING,
        allowNull: true
    },

    npwp: {
        type: DataTypes.STRING,
        allowNull: true
    },
})