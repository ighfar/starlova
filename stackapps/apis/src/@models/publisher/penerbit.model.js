import { Sequelize } from "sequelize";
import db from '../../@config/db';
const { DataTypes } = Sequelize;

export const PENERBIT = db.define('publisher', {
    email: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true,
    },

    password: {
        type: DataTypes.STRING,
        allowNull: false
    },

    roles: {
        type: DataTypes.ENUM('admin', 'super', 'user'),
        defaultValue: "super",
        allowNull: false
    },

    isVerified: {
        type: DataTypes.BOOLEAN,
        defaultValue: false
    },

    isSuspended: {
        type: DataTypes.BOOLEAN,
        defaultValue: false
    },

    isBanned: {
        type: DataTypes.BOOLEAN,
        defaultValue: false
    },

    id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true,
        allowNull: false
    }
})