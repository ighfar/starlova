import { ADMIN } from './admin/admin.model';
import { PENERBIT } from './publisher/penerbit.model';
import { PENULIS } from './writer/penulis.model';
import { DOCUMENT } from './data/document.model';
import { PROFILE } from './data/profile.model';
import { BOOK } from './books/book.model';
import { CHAPTER } from './books/chapter.model';
import { PDF } from './books/pdf.model';
import { EDITOR } from './editor/editor.model';
import { ORDER } from './data/order.model';
import { SALES } from './data/sales.model';
import { TOKEN } from './token/token.model';

/* ==== ONE TO ONE MANY ===== */
ADMIN.hasMany(PENERBIT);

PENERBIT.hasMany(PENULIS);
PENULIS.hasMany(EDITOR);

PENULIS.hasMany(BOOK);
PENULIS.hasMany(ORDER, { as: "seller", foreignKey: "sellerId" });
PENULIS.hasMany(SALES, { as: "buyer", foreignKey: "buyerId" });

BOOK.belongsToMany(EDITOR, { through: 'BookEditor'});


BOOK.hasMany(CHAPTER);
BOOK.hasMany(PDF);

BOOK.hasMany(ORDER);
/* ==== ONE TO MANY END ===== */

/* ==== ONE TO ONE RELATION ===== */

ADMIN.hasOne(TOKEN);
TOKEN.belongsTo(ADMIN);

ADMIN.hasOne(DOCUMENT);
DOCUMENT.belongsTo(ADMIN);

PENERBIT.hasOne(TOKEN);
TOKEN.belongsTo(PENERBIT);

PENERBIT.hasOne(DOCUMENT);
DOCUMENT.belongsTo(PENERBIT);

PENULIS.hasOne(TOKEN);
TOKEN.belongsTo(PENULIS);

PENULIS.hasOne(DOCUMENT);
DOCUMENT.belongsTo(PENULIS);

ADMIN.hasOne(PROFILE);
PROFILE.belongsTo(ADMIN);

PENERBIT.hasOne(PROFILE);
PROFILE.belongsTo(PENERBIT);

PENULIS.hasOne(PROFILE);
PROFILE.belongsTo(PENULIS);

ORDER.hasOne(SALES);
SALES.belongsTo(ORDER);

/* ==== ONE TO ONE RELATION END ===== */

export { ADMIN, PENERBIT, PENULIS, ORDER, SALES, DOCUMENT, BOOK, PDF, CHAPTER, EDITOR, TOKEN} 