import { Sequelize } from "sequelize";
import db from '../../@config/db';
const { DataTypes } = Sequelize;

export const TOKEN = db.define('token', {
    token: { 
        type: DataTypes.TEXT('long'),
    },
    expires: { 
        type: DataTypes.DATE 
    },
    created: { 
        type: DataTypes.DATE, 
        allowNull: false, 
        defaultValue: DataTypes.NOW 
    },
    createdByIp: { 
        type: DataTypes.STRING 
    },
    isExpired: {
        type: DataTypes.VIRTUAL,
        get() { 
            return Date.now() >= this.expires; 
        }
    },
    isActive: {
        type: DataTypes.VIRTUAL,
        get() { 
            return this.token; 
        }
    }
}, {
    timestamps: false
});
