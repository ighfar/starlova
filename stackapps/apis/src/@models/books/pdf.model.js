import { Sequelize } from "sequelize";
import db from '../../@config/db';
const { DataTypes } = Sequelize;

export const PDF = db.define('pdf', {
    size: {
        type: DataTypes.INTEGER,
    },

    image: {
        type: DataTypes.STRING,
    }, 
    pages: {
        type: DataTypes.INTEGER,
    }
})