import { Sequelize } from "sequelize";
import db from '../../@config/db';
const { DataTypes } = Sequelize;

export const CHAPTER = db.define('chapter', {

    name: {
        type: DataTypes.STRING,
        allowNull: true
    },

    content: {
        type: DataTypes.STRING(1000),
        validate: {
            len: [500, 1000]
        },
        allowNull: true
    },

})