import { Sequelize } from "sequelize";
import db from '../../@config/db';
const { DataTypes } = Sequelize;

export const BOOK = db.define('book', {
    title: {
        type: DataTypes.STRING,
    },

    cover: {
        type: DataTypes.STRING,
    },

    price: {
        type: DataTypes.BIGINT,
        defaultValue: 0
    },

    type: {
        type: DataTypes.ENUM('chapter', 'pdf'),
        defaultValue: "chapter",
        allowNull: false
    },

    category: {
        type: DataTypes.STRING,
        defaultValue: "novel",
        allowNull: false
    },

    summary: {
        type: DataTypes.STRING(500),
    },
})