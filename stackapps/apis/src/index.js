import AppData from './@settings/Setup';
import Server from './@settings/Server';
import Traffic from "./@settings/Traffic";
import Hosted from "./@settings/Hosted";
import adminDb from './firebase/firebase';
import express, { Router } from "express";

// AMBIL DATA KONFIGURASI SERVER TAMBAHAN DARI DATABASE
( process.env.NODE_ENV === 'production' ? async () => {
    try {
        const querySnapshot = await adminDb.collection("servers").get();
        const servers = querySnapshot?.docs?.map((doc) => ({
            id: doc.id, ...doc.data()
        }))
        console.log("Check data server : ", servers)
        if (servers?.length > 0 && Array.isArray(servers)) {
            servers?.map((item, key) => {
                AppData.push({
                    appServer: express(),
                    appRouter: Router(),
                    appName: [(item.appName)],
                    servePath: item.servePath,
                    serveReact: item.serveReact,
                    port: item.port
                })
            })
        }

        // SERVE STATIC WEB REACT BUILD PRODUKSI
    
        Hosted(AppData);
        
        // TRAFFIC DIREKTORI
        Traffic(AppData);

        // MEMBUAT INITIAL MULTI APLIKASI INSTANCE
        Server(AppData, false);

    } catch (error) {
        console.log(error.message)
    }
} : () => {
       // SERVE STATIC WEB REACT BUILD PRODUKSI
       Hosted(AppData);
        
       // TRAFFIC DIREKTORI
       Traffic(AppData);

       // MEMBUAT INITIAL MULTI APLIKASI INSTANCE
       Server(AppData, false);
})()


