import { static as serve } from 'express';
import { join } from 'path';
import ApiServer from '../@server';

export default function Hosted(appInstanceOfExpress = []) {
    if (appInstanceOfExpress.length > 0) {
        appInstanceOfExpress.map((item, key) => {
            //MENDAPATKAN NAMA APLIKASI DAN FUNCTION INSTANCE DARI EXPRESS
            const nama_instance_appExpress = Object.keys(appInstanceOfExpress[key]);
            const nama_instance_appExpressRouter = Object.keys(appInstanceOfExpress[key]);
            
            if (typeof item.serveReact !== 'object') {
                // APP EXPRESS INSTANCE UNTUK SERVING MASTER CENTER API /api/v2 BERJALAN PADA http://localhost:4000 dan http://localhost:3000
                item.appServer.use(serve(join(__dirname, `../../../${item.serveReact}`)));
                item.appServer.use("/api", ApiServer);
                item.appServer.get('*', (req, res) => {
                    res.sendFile(join(__dirname, `../../../${item.serveReact}`, 'index.html'));
                })
            } else {
                // APP EXPRESS MASTER UNTUK SERVING MASTER CENTER API http://localhost:8181/api/v2 pada port 8181
                item.appServer.get('/', (req, res) => {
                    //INFORMASI REPLIKA APLIKASI
                    const instanceCommon = {
                        message: `Welcome to ${nama_instance_appExpress[0]} portal application!`,
                    }

                    res.send(instanceCommon)
                })

                item.appServer.use('/api', ApiServer);

                // HANDLING ROUTING NOT FOUND ENDPOINT
                item.appServer.get('*', function (req, res) {
                    res.status(404).send(`${req.method} METHOD ${req.path} tidak ditemukan!`);
                });

                item.appServer.post('*', function (req, res) {
                    res.status(404).send(`${req.method} METHOD ${req.path} tidak ditemukan!`);
                });

                item.appServer.put('*', function (req, res) {
                    res.status(404).send(`${req.method} METHOD ${req.path} tidak ditemukan!`);
                });

                item.appServer.patch('*', function (req, res) {
                    res.status(404).send(`${req.method} METHOD ${req.path} tidak ditemukan!`);
                });

                item.appServer.delete('*', function (req, res) {
                    res.status(404).send(`${req.method} METHOD ${req.path} tidak ditemukan!`);
                });
            }
            
        })
    } else {
        console.log("Tidak ada aplikasi!")
    }
}