import { hostname } from 'os';
import ServeHttps from '../utils/ServeHttps';

export default function Server(appInstanceOfExpress = [], enableHttps = false) {
    
    const AppCheck = process.env.NODE_ENV !== 'production' ? (appInstanceOfExpress[0]) : (appInstanceOfExpress);
    // console.log("App Check : ", AppCheck);
    
    // START MULTIPLE SERVER
    process.env.NODE_ENV !== 'production' ? (
        console.log("Server Development Running!"),
        ( () => {
            if(enableHttps) {
                // SETUP HTTP SERVER KETIKA DIBUTUHKAN
                ServeHttps(AppCheck.appServer, 'certs').listen(AppCheck.port, () => {
                    return console.log(`Server is running on https://${hostname()}:${AppCheck.port}`)
                });
            } else {
                AppCheck.appServer.listen(AppCheck.port, () => {
                    return console.log(`Server is running on http://${hostname()}:${AppCheck.port}`)
                });
            }
        })()

    ) : (
        console.log("Server Production Running!"),
        AppCheck.map((item, key) => {
            if(enableHttps) {
                // SETUP HTTP SERVER KETIKA DIBUTUHKAN
                ServeHttps(item.appServer, 'certs').listen(item.port, () => {
                    return console.log(`Server is running on https://${hostname()}:${item.port}`)
                });
            } else {
                item.appServer.listen(item.port, () => {
                    return console.log(`Server is running on http://${hostname()}:${item.port}`)
                });
            }
        })
    )
}