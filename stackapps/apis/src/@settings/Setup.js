import express, { Router } from "express";
import { port } from "../@config/config";

const { api, admin, app } = port;

// DAFTAR DATA INSTANCE(REPLIKA)
const dataArrInstance = [
    {
        appName: "api",
        servePath: ["images", "certs", "upload"],
        serveReact: null, // SET NULL UNTUK SERVING ADMIN API MASTER
        port: api,

    },
    {
        appName: "admin",
        servePath: ["images", "certs", "upload"],
        serveReact: 'packages/admin/build',
        port: admin,
    },

    {
        appName: "client",
        servePath: ["images", "certs", "upload"],
        serveReact: 'packages/apps/build',
        port: app,
    },
];

// AMBIL KONFIGURASI SERVER DARI DATABASE
const AppData = [];

 // MEMBUAT BANYAK REPLIKASI INSTANCE SERVER APP:express();
if (dataArrInstance.length > 0 && Array.isArray(dataArrInstance)) {
    dataArrInstance.map((item, key) => {
        AppData.push({
            appServer: express(),
            appRouter: Router(),
            [(item.appName)+ "Server"]: express(),
            [(item.appName)+ "Router"]: Router(),
            servePath: item.servePath,
            serveReact: item.serveReact,
            port: item.port
        })
    })
}

// console.log(AppData)
export const App = (instance = 0) => {
    return AppData.length !== 0 ? (AppData[instance].apiServer) : (console.log("Tidak ditemukan instance of express!"));
}
 
export const AppRouter = (instance = 0) => {
    return AppData.length !== 0 ? (AppData[instance].apiRouter) : (console.log("Tidak ditemukan instance of router !"));
}

export default AppData;
