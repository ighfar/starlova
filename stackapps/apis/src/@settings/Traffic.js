import { static as staticDir } from 'express';
import { join } from 'path';

export default function Traffic(appInstanceOfExpress = []) {
  /* ========== MEMPERBOLEHKAN TRAFFIC DAFTAR LIST DIREKTORI ROOT =========== */
  appInstanceOfExpress.lengh === 0 ? console.log('Tidak ditemukan object data path') : (
    appInstanceOfExpress.map((item, key) => {
      const AppMapperInstance = item.appServer;

      item.servePath.map((path, key) => {
        const pathDir = staticDir(join(__dirname, `../${path}`));
        AppMapperInstance.use(`/${path}`, pathDir);
      })

    })
  )
}