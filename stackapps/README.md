### NODE JS VERSION
```javascript 
   node --version = v12.22.9
   npm --version = 8.3.2
```

Beberapa component **apis** penting yang harus dipahami sebagai berikut :+1: 
1. Setup **App Server** di **@settings** component yang mengatur konfigurasi server
2. Setup  **App Query** Sequelize di @query component yang berisi kelas fungsi CRUD controllers & middleware (**_body validation logic_**)
3. Setup **App Router** di @router component yang berfungsi sebagai router dari express untuk mengatur **Routing URL Aplikasi**

### KONFIGURASI SERVER MULTI INSTANCE

Mengatur **Server** HTTPS Transports di **index.js** :
```javascript 
Server(AppData, true)
```

Mengatur **Server** HTTP Transports :
```javascript 
Server(AppData, false)
```

Mengatur Multi Instance Web Server ada di @settings component file **Setup.js** :
```javascript
const dataArrInstance = [
    {
        appName: "myapp1",
        servePath: ["images", "certs", "upload"],
        serveReact: null, // SET NULL UNTUK SERVING API
        port: api,

    },
    {
        appName: "myapp2",
        servePath: ["images", "certs", "upload"],
        serveReact: 'packages/admin/build',
        port: admin,
    },

    {
        appName: "myapp3",
        servePath: ["images", "certs", "upload"],
        serveReact: 'packages/apps/build',
        port: app,
    },
]
```
Untuk menambah konfigurasi instance **App Server Baru** sekarang sudah bisa ditambahkan lewat firebase database:

![Contoh Model Instances](https://raw.githubusercontent.com/admhabits/starlova-dashboard/alamdev/assets/contoh-konfigurasi.png?token=GHSAT0AAAAAABQ3JUVVF4AK5MRQDCECUEMYYP7ZQIQ)


### AKSES API ENDPOINT 
Semua endpoint **/api/v2** berlaku di semua server port yang berjalan secara default **8181, 4000, 3000**:
```javascript
/api/v2
```

Jika butuh bantuan install server dari **development** sampai **production**, silahkan hubungi email alamhafidz61@gmail.com
