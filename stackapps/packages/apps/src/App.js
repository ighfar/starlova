import React, { useEffect } from 'react';
import { Route, Routes } from 'react-router-dom';
import { Footer, Header, IconsContainer, Loader, Newsletter } from './components/layouts';
import { useSelector } from 'react-redux';
import useSession from './api/useSession';
import { WriterProtected } from './routes/WriterProtected';
import "assets/css/style.css";
// import LoadingInit from './components/utils/LoadingInit';

const HomeComponent = React.lazy(() => import('pages/Hompage'));
const WriterComponent = React.lazy(() => import('pages/WriterPage'));

const HomeLazy = () => {

     return (
          <React.Suspense fallback={<Loader />}>
               <Header />
               <HomeComponent />
               <Newsletter />
               <Footer />
          </React.Suspense>
     )
}


const WriterLazy = ({isLoggedIn}) => {
     return (
          <React.Suspense fallback={<Loader />}>
               <Header />
               <WriterComponent />
               <Footer />
          </React.Suspense>
     )
}

export default function App() {
     useSession();
     const { isLoggedIn, roles } = useSelector((state) => state.auth);
     return (
          <Routes>
               <Route path='/*' element={<HomeLazy />} />
               <Route element={<WriterProtected isLoggedIn={isLoggedIn} roles={roles} />}>
                    <Route
                         path={`writer/*`}
                         element={<WriterLazy isLoggedIn={isLoggedIn} />}
                    />
               </Route>
               <Route path="*" element={<div>Tidak ditemukan halaman disini!</div>} />
          </Routes>
     );
}