import React, { useEffect, useLayoutEffect } from 'react';
import {
  Blogs,
  Feature,
  Arrival,
  Deal,
  Review,
  IconsContainer,
  HomeSection,
  LoginContainer
} from 'components/layouts';
import BookSlider from 'components/utils/BookSlider';
import FeatureSlider from 'components/utils/FeatureSlider';
import ArrivalSlider from 'components/utils/ArrivalSlider';
import ReviewSlider from 'components/utils/ReviewSlider';
import BlogSlider from 'components/utils/BlogSlider';
import { useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';

export default function Hompage() {
  const navigate = useNavigate();
  const { isLoggedIn } = useSelector((state) => state.auth);
  useLayoutEffect(() => {
    BookSlider();
    FeatureSlider();
    ArrivalSlider();
    ReviewSlider();
    BlogSlider();
    !isLoggedIn && navigate('/');
  }, []);

  return (
    <React.Fragment>
      <LoginContainer />
      <HomeSection />
      <IconsContainer />
      <Feature />
      <Deal />
      <Arrival />
      <Review />
      <Blogs />
    </React.Fragment>
  );
}