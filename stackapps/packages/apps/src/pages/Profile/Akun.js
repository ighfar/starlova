import jwtDecode from 'jwt-decode';
import React from 'react';
import { useSelector } from 'react-redux';
import { Decrypt } from 'utils/Encrypt';

export default function Akun() {
    const [user, setUser] = React.useState(null);
    const { auth } = useSelector((state) => state);

    React.useState(() => {
        const tokenDecode =  auth?.secret && jwtDecode(Decrypt(auth.secret));
        setUser(tokenDecode);
    }, [auth]);

    return (
        <section className='home'>
            <div className='profile-info'>
                <div className="profile">
                    <div className="image">
                        <div className="circle-1"></div>
                        <div className="circle-2"></div>
                        <img src="https://100dayscss.com/codepen/jessica-potter.jpg" width="70" height="70" alt="Jessica Potter" />
                    </div>

                    <div className="name">Gea Lova</div>
                    <div className="job">Writer Starlova</div>

                    <div className="actions">
                        <button className="btn">Follow</button>
                        <button className="btn active">Message</button>
                    </div>
                </div>

                <div className="stats">
                    <div className="box">
                        <span className="value">21</span>
                        <span className="parameter">Books</span>
                    </div>
                    <div className="box">
                        <span className="value">1387</span>
                        <span className="parameter">Sales</span>
                    </div>
                    <div className="box">
                        <span className="value">146</span>
                        <span className="parameter">Follower</span>
                    </div>
                </div>
            </div >
        </section>
    )
}
