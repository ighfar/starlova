import React from 'react'
import 'assets/css/profile.css';
import Akun from './Akun';
import Slider from './Slider';

export default function Profile() {
    return (
        <>
            <div className='my-profile'>
                <Akun />
                <Slider/>
            </div>
        </>
    )
}
