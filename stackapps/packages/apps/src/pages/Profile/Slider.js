import React from 'react';
import ConvertToRp from 'components/utils/ConverToRp';
import Book1 from 'assets/image/book-1.png';
import Book2 from 'assets/image/book-2.png';
import Book3 from 'assets/image/book-3.png';
import Book6 from 'assets/image/book-6.png';
import Book7 from 'assets/image/book-7.png';
import Book8 from 'assets/image/book-8.png';
import Swiper, { Navigation } from 'swiper';

export default function Slider() {
    const data = [
        { price: 44000, discount: 10, star: 5, type: "Buku Terlaris", bookImage: Book1 },
        { price: 54000, discount: 10, star: 5, type: "Buku Terlaris", bookImage: Book2 },
        { price: 38000, discount: 10, star: 5, type: "Buku Terlaris", bookImage: Book3 },
        { price: 44000, discount: 10, star: 5, type: "Buku Terlaris", bookImage: Book6 },
        { price: 54000, discount: 10, star: 5, type: "Buku Terlaris", bookImage: Book7 },
        { price: 38000, discount: 10, star: 5, type: "Buku Terlaris", bookImage: Book8 },
    ];

    React.useEffect(() => {
        Swiper.use([Navigation]);
        new Swiper(".account-slider", {
            spaceBetween: 10,
            loop: true,
            centeredSlides: true,
            autoplay: {
                delay: 9500,
                disableOnInteraction: false,
            },
            navigation: {
                nextEl: ".swiper-button-next",
                prevEl: ".swiper-button-prev",
            },
            breakpoints: {
                0: {
                    slidesPerView: 1,
                },
                450: {
                    slidesPerView: 2,
                },
                768: {
                    slidesPerView: 3,
                },
                1024: {
                    slidesPerView: 4,
                },
            },
        });

    }, [])
    return (
        <section className='home'>
            <div className='account'>
                <h1 className="heading"> <span>Buku Terlaris</span> </h1>
                <div className="swiper account-slider">
                    <div className="swiper-wrapper">

                        {
                            data.map((item, key) => (
                                <div className="swiper-slide box" key={key}>
                                    <div className="icons">
                                        <a href="#" className="fas fa-search"></a>
                                        <a href="#" className="fas fa-heart"></a>
                                        <a href="#" className="fas fa-eye"></a>
                                    </div>
                                    <div className="image">
                                        <img src={item.bookImage} alt="" />
                                    </div>
                                    <div className="content">
                                        <h3>{item.type}</h3>
                                        <div className="price">
                                            {ConvertToRp(item.price - (item.price * item.discount / 100))}
                                            <span> {ConvertToRp(item.price)}</span>
                                        </div>
                                        <a href="#" className="btn">Beli Sekarang</a>
                                    </div>
                                </div>
                            ))
                        }
                    </div>
                    <div className="swiper-button-next"></div>
                    <div className="swiper-button-prev"></div>

                </div>
            </div>
        </section>
    )
}
