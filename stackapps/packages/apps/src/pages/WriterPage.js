import React from 'react';
import {
  Feature,
  Arrival,
  IconsContainer,
  Newsletter,
} from 'components/layouts';
import FeatureSlider from 'components/utils/FeatureSlider';
import ArrivalSlider from 'components/utils/ArrivalSlider';
import { Route, Routes, useParams } from 'react-router-dom';
import Profile from './Profile/Profile';
import Promotion from 'components/layouts/others/Promotion';


export default function WriterPage() {
  React.useEffect(() => {
    FeatureSlider();
    ArrivalSlider();
  }, [])

  return (
    <React.Fragment>
      <Routes>
        <Route index element={
          <>
            <IconsContainer/>
            <Feature />
            <Arrival />
            <Promotion/>
          </>
        }
        />
        <Route path='wishlist' element={<>wishlist</>} />
        <Route path='cart' element={<>cart</>} />
        <Route path='profile' element={<><Profile /></>} />
      </Routes>
    </React.Fragment>
  );
}