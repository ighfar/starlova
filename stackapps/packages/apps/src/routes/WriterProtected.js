import React from 'react'
import { Navigate, Outlet } from 'react-router-dom'

export function WriterProtected({ isLoggedIn, roles }) {
    return isLoggedIn && roles === 'user' ? <Outlet /> : <Navigate to="/" />
}
