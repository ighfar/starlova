import AuthSlices from 'redux/AuthSlices';
import { configureStore } from '@reduxjs/toolkit';

export const store = configureStore({
  reducer: {
    auth: AuthSlices,
  },
})

