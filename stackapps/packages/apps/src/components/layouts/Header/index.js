import React, { useEffect, useLayoutEffect } from 'react';
import { Link, NavLink, useNavigate } from 'react-router-dom';
import { useSelector } from 'react-redux';
import ApiConfig from 'config';
import { setAuth } from 'redux/AuthSlices';
import { useDispatch } from 'react-redux';
import { baseURL } from 'config';

export default function Header() {
    const { isLoggedIn, params } = useSelector((state) => state.auth);
    // const navigate = useNavigate();
    const dispatch = useDispatch();
	const SignOut = () => {
		ApiConfig.post(`/writer/auth/signout`).then((success) => {
            dispatch(
                setAuth({
                    isLoggedIn: false,
                })
            )
			localStorage.removeItem("profile");
			alert("Are you sure want to sign out ?");
			window.location.reload();
		})
			.catch((error) => {
				console.log(error.response.data)
			})
	}
    const scrollToHome = () => {
        const home = document.querySelector('#home');
        home.scrollIntoView({ behavior: "smooth" })
    }
    const scrollToFeatured = (e) => {
        const featured = document.querySelector('#featured');
        featured.scrollIntoView({ behavior: "smooth" })
    }

    const scrollToArrivals = (e) => {
        const arrival = document.querySelector('#arrivals');
        arrival.scrollIntoView({ behavior: "smooth" })
    }

    const scrollToReviews = (e) => {
        const reviews = document.querySelector('#reviews');
        reviews.scrollIntoView({ behavior: "smooth" })
    }

    const scrollToBlogs = () => {
        const blogs = document.querySelector('#blogs');
        blogs.scrollIntoView({ behavior: "smooth" })
    }
    const showLoginComponent = () => {
        let loginForm = document.querySelector('.login-form-container');
        loginForm.classList.toggle('active');
    }


    return (<>
        <header className="header">
            <div className="header-1">
                <div className="logo"> <i className="fas fa-book"></i> StarLova </div>

                <form action="" className="search-form">
                    <input type="search" name="" placeholder="Search here..." id="search-box" />
                    <label htmlFor="search-box" className="fas fa-search"></label>
                </form>

                <div className="icons">
                    <div id="search-btn" className="fas fa-search"></div>
                    <Link to="/writer/wishlist" className="fas fa-heart"></Link>
                    <Link to="/writer/cart" className="fas fa-shopping-cart"></Link>
                    <div onClick={isLoggedIn ? SignOut : showLoginComponent} className={ isLoggedIn ? "fa-solid fa-right-from-bracket" : "fas fa-user"}></div>
                </div>

            </div>

            <div className="header-2">
                <nav className="navbar">
                    {
                        isLoggedIn ? (
                            <>
                                <NavLink to={`/writer${params}`} onClick={() => window.location.reload()}>Home</NavLink>
                                <NavLink to={`/writer/books${params}`}>Semua Produk</NavLink>
                                <NavLink to={`/writer/orders${params}`}>Pendapatan</NavLink>
                                <NavLink to={`/writer/sales${params}`} onClick={() => (window.location.href = baseURL.admin)}>Manage Karya</NavLink>
                                <NavLink to={`/writer/profile${params}`}>Akun Saya</NavLink>

                            </>
                        ) : (
                            <>
                                <Link to="/" onClick={scrollToHome}>home</Link>
                                <Link to="/" onClick={scrollToFeatured}>featured</Link>
                                <Link to="/" onClick={scrollToArrivals}>arrivals</Link>
                                <Link to="/" onClick={scrollToReviews}>reviews</Link>
                                <Link to="/" onClick={scrollToBlogs}>blogs</Link>
                            </>
                        )
                    }
                </nav>
            </div>

        </header>
        <nav className="bottom-navbar">
            {
                isLoggedIn ? (
                    <>
                        <Link to="/writer" onClick={scrollToFeatured} className="fas fa-home"></Link>
                        <Link to={`/writer/books${params}`} onClick={scrollToFeatured} className="fas fa-list"></Link>
                        <Link to={`/writer/orders${params}`} onClick={scrollToFeatured} className="fas fa-tags"></Link>
                        <Link to={`/writer/sales${params}`} onClick={() => (window.location.href = baseURL.admin)} className="fa-solid fa-toolbox"></Link>
                        <Link to={`/writer/profile${params}`} onClick={scrollToArrivals} className="fa-solid fa-user"></Link>

                    </>
                ) : (
                    <>
                        <Link to="/" onClick={scrollToHome} className="fas fa-home"></Link>
                        <Link to="/" onClick={scrollToFeatured} className="fas fa-list"></Link>
                        <Link to="/" onClick={scrollToArrivals} className="fas fa-tags"></Link>
                        <Link to="/" onClick={scrollToReviews} className="fas fa-comments"></Link>
                        <Link to="/" onClick={scrollToBlogs} className="fas fa-blog"></Link>
                    </>
                )
            }
        </nav>
    </>
    );
}
