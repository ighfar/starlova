import Header from "./Header";
import Loader from "./others/Loader";
import Footer from "./Footer";
import Blogs from "./others/Blogs";
import Feature from "./others/Feature";
import Arrival from "./others/Arrival";
import Deal from "./others/Deal";
import Review from "./others/Review";
import IconsContainer from "./others/IconsContainer";
import HomeSection from "./others/HomeSection";
import Newsletter from "./others/Newsletter";
import LoginContainer from "./others/LoginContainer";


export {
    Header,
    Footer,
    Loader,
    Blogs,
    Feature,
    Arrival,
    Deal,
    Review,
    IconsContainer,
    HomeSection,
    Newsletter,
    LoginContainer,
};