import React from 'react';
import { Link } from 'react-router-dom';

export default function Footer() {
  return (
    <section className="footer">

        <div className="box-container">

            <div className="box">
                <h3>our locations</h3>
                <Link to="/"> <i className="fas fa-map-marker-alt"></i> Indonesia </Link>
                <Link to="/"> <i className="fas fa-map-marker-alt"></i> USA </Link>
                <Link to="/"> <i className="fas fa-map-marker-alt"></i> India </Link>
                <Link to="/"> <i className="fas fa-map-marker-alt"></i> france </Link>
                <Link to="/"> <i className="fas fa-map-marker-alt"></i> japan </Link>
                <Link to="/"> <i className="fas fa-map-marker-alt"></i> africa </Link>
            </div>

            <div className="box">
                <h3>quick links</h3>
                <Link to="/"> <i className="fas fa-arrow-right"></i> home </Link>
                <Link to="/"> <i className="fas fa-arrow-right"></i> featured </Link>
                <Link to="/"> <i className="fas fa-arrow-right"></i> arrivals </Link>
                <Link to="/"> <i className="fas fa-arrow-right"></i> reviews </Link>
                <Link to="/"> <i className="fas fa-arrow-right"></i> blogs </Link>
            </div>

            <div className="box">
                <h3>extra links</h3>
                <Link to="/"> <i className="fas fa-arrow-right"></i> account info </Link>
                <Link to="/"> <i className="fas fa-arrow-right"></i> ordered items </Link>
                <Link to="/"> <i className="fas fa-arrow-right"></i> privacy policy </Link>
                <Link to="/"> <i className="fas fa-arrow-right"></i> payment method </Link>
                <Link to="/"> <i className="fas fa-arrow-right"></i> our serivces </Link>
            </div>

            <div className="box" >
                <h3>contact info</h3>
                <Link to="/"> <i className="fas fa-phone"></i> +62-821-2549-7764 </Link>
                <Link to="/"> <i className="fas fa-phone"></i> +62-821-2549-7764 </Link>
                <Link style={{ textTransform: 'lowercase' }} to="/"> <i className="fas fa-envelope"></i> admin@starlova.com </Link>
                <img src="image/worldmap.png" className="map" alt=""/>
            </div>
            
        </div>

        <div className="share">
            <Link to="/" className="fab fa-facebook-f"></Link>
            <Link to="/" className="fab fa-twitter"></Link>
            <Link to="/" className="fab fa-instagram"></Link>
            <Link to="/" className="fab fa-linkedin"></Link>
            <Link to="/" className="fab fa-pinterest"></Link>
        </div>

    <div className="credit"> created by <span>CV. StarLova Publishing</span> | all rights reserved! </div>
    </section>
    
  );
}
