import React from 'react';
import LoaderGif from 'assets/image/loader-img.gif';

export default function Loader() {
  return (
    <div className="loader-container">
        <img src={LoaderGif} alt=""/>
    </div>
  );
}
