import React from 'react';
import Book1 from 'assets/image/book-1.png';
import Book2 from 'assets/image/book-2.png';
import Book3 from 'assets/image/book-3.png';
import Book4 from 'assets/image/book-4.png';
import Book5 from 'assets/image/book-5.png';
import Book6 from 'assets/image/book-6.png';
import ConvertToRp from '../../utils/ConverToRp';

export default function Arrival() {
    const TopBook = [
        { price: 44000, discount: 10, star: 5, type: "Top Books", bookImage: Book1 },
        { price: 54000, discount: 10, star: 5, type: "Top Books", bookImage: Book2 },
        { price: 38000, discount: 10, star: 5, type: "Top Books", bookImage: Book3 },
    ]

    const PremiumBook = [
        { price: 64000, discount: 10, star: 5, type: "Premium Books", bookImage: Book4 },
        { price: 84000, discount: 10, star: 5, type: "Premium Books", bookImage: Book5 },
        { price: 58000, discount: 10, star: 5, type: "Premium Books", bookImage: Book6 },
    ]
    return (
        <section className="arrivals" id="arrivals">

            <h1 className="heading"> <span>new arrivals</span> </h1>

            <div className="swiper arrivals-slider">

                <div className="swiper-wrapper">

                    {
                        TopBook.map((item, key) => (
                            <a href="#" className="swiper-slide box" key={key}>
                                <div className="image">
                                    <img src={item.bookImage} alt="" />
                                </div>
                                <div className="content">
                                    <h3>{item.type}</h3>
                                    <div className="price">
                                        {ConvertToRp(item.price - (item.price * item.discount / 100))} 
                                        <span> {ConvertToRp(item.price)}</span>
                                    </div>
                                    <div className="stars">
                                        <i className="fas fa-star"></i>
                                        <i className="fas fa-star"></i>
                                        <i className="fas fa-star"></i>
                                        <i className="fas fa-star"></i>
                                        <i className="fas fa-star-half-alt"></i>
                                    </div>
                                </div>
                            </a>
                        ))
                    }

                </div>

            </div>

            <div className="swiper arrivals-slider">

                <div className="swiper-wrapper">

                    {
                        PremiumBook.map((item, key) => (
                            <a href="#" className="swiper-slide box" key={key}>
                                <div className="image">
                                    <img src={item.bookImage} alt="" />
                                </div>
                                <div className="content">
                                    <h3>{item.type}</h3>
                                    <div className="price">
                                        {ConvertToRp(item.price - (item.price * item.discount / 100))} 
                                        <span> {ConvertToRp(item.price)}</span>
                                    </div>
                                    <div className="stars">
                                        <i className="fas fa-star"></i>
                                        <i className="fas fa-star"></i>
                                        <i className="fas fa-star"></i>
                                        <i className="fas fa-star"></i>
                                        <i className="fas fa-star-half-alt"></i>
                                    </div>
                                </div>
                            </a>
                        ))
                    }
                </div>

            </div>

        </section>

    );
}
