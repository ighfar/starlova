import React from 'react';
import { Link } from 'react-router-dom';
import ImgDeal from 'assets/image/deal-img.jpg';

export default function Deal() {
  const showLoginComponent = () => {
    let loginForm = document.querySelector('.login-form-container');
    loginForm.classList.toggle('active');
}
  return (
    <section className="deal">
    <div className="content">
        <h3>StarLova Partner</h3>
        <h1>Publishing Program Partner</h1>
        <p>Bergabung bersama kami untuk membangun platform komunitas penerbitan buku digital yang mudah dan praktis</p>
        <Link to="#" className="btn" onClick={showLoginComponent}>Daftar Menjadi Penerbit</Link>
    </div>
    <div className="image">
        <img src={ImgDeal} alt=""/>
    </div>
    </section>
  );
}
