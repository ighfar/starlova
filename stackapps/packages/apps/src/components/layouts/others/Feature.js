import React from 'react';
import Book1 from 'assets/image/book-1.png';
import Book2 from 'assets/image/book-2.png';
import Book3 from 'assets/image/book-3.png';
import Book6 from 'assets/image/book-6.png';
import Book7 from 'assets/image/book-7.png';
import Book8 from 'assets/image/book-8.png';
import ConvertToRp from '../../utils/ConverToRp';

export default function Feature() {
    const FeaturedData = [
        { price: 44000, discount: 10, star: 5, type: "Best Seller", bookImage: Book1 },
        { price: 54000, discount: 10, star: 5, type: "Best Seller", bookImage: Book2 },
        { price: 38000, discount: 10, star: 5, type: "Best Seller", bookImage: Book3 },
        { price: 44000, discount: 10, star: 5, type: "Best Seller", bookImage: Book6 },
        { price: 54000, discount: 10, star: 5, type: "Best Seller", bookImage: Book7 },
        { price: 38000, discount: 10, star: 5, type: "Best Seller", bookImage: Book8 },
    ]
    return (
        <section className="featured" id="featured">

            <h1 className="heading"> <span>featured books</span> </h1>

            <div className="swiper featured-slider">
                <div className="swiper-wrapper">

                    {
                        FeaturedData.map((item, key) => (
                            <div className="swiper-slide box" key={key}>
                                <div className="icons">
                                    <a href="#" className="fas fa-search"></a>
                                    <a href="#" className="fas fa-heart"></a>
                                    <a href="#" className="fas fa-eye"></a>
                                </div>
                                <div className="image">
                                    <img src={item.bookImage} alt="" />
                                </div>
                                <div className="content">
                                    <h3>{item.type}</h3>
                                    <div className="price">
                                        {ConvertToRp(item.price - (item.price * item.discount / 100))}
                                        <span> {ConvertToRp(item.price)}</span>
                                    </div>
                                    <a href="#" className="btn">Beli Sekarang</a>
                                </div>
                            </div>
                        ))
                    }
                </div>
                <div className="swiper-button-next"></div>
                <div className="swiper-button-prev"></div>

            </div>

        </section>
    );
}
