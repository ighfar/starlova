import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import ApiConfig from 'config';
import jwt_decode from 'jwt-decode';
import { baseURL } from 'config';

export default function LoginContainer() {
  const [error, setError] = useState("");
  const [isAccept, setIsAccept] = useState(true);
  const [isRemember, setIsRemember] = useState(true);
  const [isSignup, setIsSignup] = useState(false);
  const [email, setEmail] = useState("");
  const [password, setPassword ] = useState("");
  const [confirm, setConfirm ] = useState("");
  const hideLoginContainer = () => {
    document.querySelector('.login-form-container').classList.remove('active');
  }
  const signIn = async (data) => {
    await ApiConfig.post(`/writer/auth/login`, data)
        .then((response) => {
          const encrypt = window.btoa(response.data.token);
          // window.location.href = `${baseURL.admin}?session=${window.btoa(encrypt)}`;
          window.location.reload();
          
        })
        .catch((error) => {
          setError(error.response.data)
        })

    }

    const signUp = async (data) => {
      await ApiConfig.post(`/writer/auth/signup`, data)
          .then((response) => {
            setError(response.data);
          })
          .catch((error) => {
            setError(error.response.data)
          })
      }

  const signUpSubmit = (e) => {
    e.preventDefault();
    const data = isSignup ? { email, password, confirm, isAccept } : { email, password, isRemember };
    isSignup ? signUp(data) : signIn(data);
  }

  return (
    <div className="login-form-container">
    <div id="close-login-btn" onClick={hideLoginContainer} className="fas fa-times"></div>
            <form onSubmit={signUpSubmit}>
            <h3>{isSignup ? "Sign Up" : "Login"}</h3>
            {
                error && <h4 className="warning animation-fades">{ `${error.message}`}</h4>
              }
            <span>Email</span>
            <input 
              type="email" 
              className="box" value={email} 
              onChange={(e) => setEmail(e.target.value)} 
              placeholder="Masukan e-mail"
            />
            <span>password</span>
            <input 
              type="password" className="box" 
              value={password} onChange={(e) => setPassword(e.target.value)} 
              placeholder="Masukan password"
            />
          
            
            {
              isSignup ? (
                <>
                  <span>Konfirmasi password</span>
            <input 
              type="password" 
              className="box" 
              value={confirm} 
              onChange={(e) => setConfirm(e.target.value)} 
              placeholder="Masukan konfirmasi password"
            />
                <div className="checkbox">
                  <input 
                    type="checkbox" id="accept-terms" 
                    value={isAccept} 
                    onChange={(e) => setIsAccept(e.target.value)}
                  />
                <label htmlFor="accept-terms"> accept terms</label>
                </div>
              </>) : (
                <div className="checkbox">
                  <input 
                    type="checkbox" id="remember-me" 
                    value={isRemember} 
                    onChange={(e) => setIsRemember(e.target.value)}
                  />
                  <label htmlFor="remember-me"> remember me</label>
              </div>
              )
            }
          

            <input type="submit" value={isSignup ? "Sign Up" : "Login"} className="btn"/>

            <p> Lupa password ? <Link to="/#">Klik disini</Link></p>
            <p>{isSignup ? "Sudah punya akun ? " : "Belum punya akun ? "}<Link to="/" onClick={(e) => { e.preventDefault(); setIsSignup(!isSignup); setError(false)}}>{isSignup ? "Masuk Akun" : "Buat Akun"}</Link></p>
            
          </form>
    </div>
  );
}
