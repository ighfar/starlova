import React from 'react';
import { Link } from 'react-router-dom';
import Cover1 from 'assets/image/blog-1.jpg';
import Cover2 from 'assets/image/blog-2.jpg';
import Cover3 from 'assets/image/blog-3.jpg';
import Cover4 from 'assets/image/blog-4.jpg';
import Cover5 from 'assets/image/blog-5.jpg';

export default function Blogs() {
    const dataBlog = [
        {
            title: 'bagaimana cara menjadi penulis yang baik',
            summary: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Optio, odio.',
            imageCover: Cover1
        },
        {
            title: 'sistem profit sharing starlova program partner',
            summary: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Optio, odio.',
            imageCover: Cover2
        },

        {
            title: 'bagaimana cara mendaftar menjadi penerbit',
            summary: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Optio, odio.',
            imageCover: Cover3
        },
        {
            title: '7 kiat rahasia sukses menjadi seorang penulis',
            summary: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Optio, odio.',
            imageCover: Cover4
        },
        {
            title: '8 panduan menulis dan menerbitkan buku di starlova',
            summary: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Optio, odio.',
            imageCover: Cover5
        },
    ]
    return (
        <section className="blogs" id="blogs">
            <h1 className="heading"> <span>our blogs</span> </h1>

            <div className="swiper blogs-slider">
                <div className="swiper-wrapper">
                    {
                        dataBlog.map((item, key) => (
                            <div className="swiper-slide box" key={key}>
                                <div className="image">
                                    <img src={item.imageCover} alt="" />
                                </div>
                                <div className="content">
                                    <h3>{item.title}</h3>
                                    <p>{item.summary}</p>
                                    <Link to="/" className="btn">read more</Link>
                                </div>
                            </div>
                        ))
                    }

                </div>
            </div>
        </section>
    );
}
