import React from 'react';

export default function IconsContainer() {
  return (
    <section className="icons-container">

    <div className="icons">
        <i className="fas fa-book-reader"></i>
        <div className="content">
            <h3>device flexibility</h3>
            <p>reading anywhere</p>
        </div>
    </div>

    <div className="icons">
        <i className="fas fa-lock"></i>
        <div className="content">
            <h3>secure payment</h3>
            <p>100 secure payment</p>
        </div>
    </div>

    <div className="icons">
        <i className="fas fa-redo-alt"></i>
        <div className="content">
            <h3>easy returns</h3>
            <p>10 days returns</p>
        </div>
    </div>

    <div className="icons">
        <i className="fas fa-headset"></i>
        <div className="content">
            <h3>24/7 support</h3>
            <p>call us anytime</p>
        </div>
    </div>

</section>
  );
}
