import React from 'react';
import Avatar1 from 'assets/image/pic-1.png';
import Avatar2 from 'assets/image/pic-2.png';
import Avatar3 from 'assets/image/pic-3.png';

export default function Review() {
    const ClientReview = [
        {
            avatar: Avatar1,
            name: 'Alam Wibowo',
            review: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Aspernatur nihil ipsa placeat. Aperiam at sint, eos ex similique facere hic.',
            star: 5,
        },
        {
            avatar: Avatar2,
            name: 'Amelia Putri',
            review: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Aspernatur nihil ipsa placeat. Aperiam at sint, eos ex similique facere hic.',
            star: 5,
        },
        {
            avatar: Avatar3,
            name: 'Ahmad Subekti',
            review: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Aspernatur nihil ipsa placeat. Aperiam at sint, eos ex similique facere hic.',
            star: 5,
        },
    ]
    return (
        <section className="reviews" id="reviews">
            <h1 className="heading"> <span>client's reviews</span> </h1>
            <div className="swiper reviews-slider">
                <div className="swiper-wrapper">

                    {
                        ClientReview.map((item, key) => (
                            <div className="swiper-slide box" key={key}>
                                <img src={item.avatar} alt={item.name} />
                                <h3>{item.name}</h3>
                                <p>{item.review}</p>
                                <div className="stars">
                                    <i className="fas fa-star"></i>
                                    <i className="fas fa-star"></i>
                                    <i className="fas fa-star"></i>
                                    <i className="fas fa-star"></i>
                                    <i className="fas fa-star-half-alt"></i>
                                </div>
                            </div>
                        ))
                    }


                </div>
            </div>
        </section>
    );
}
