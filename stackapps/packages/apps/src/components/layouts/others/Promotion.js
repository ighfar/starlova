import React from 'react';
import { Link } from 'react-router-dom';
import ImgDeal from 'assets/image/deal-img.jpg';

export default function Promotion() {
  const getReferalPartner = (e) => {
    e.preventDefault();
    alert("Ya, saya mau!")
}
  return (
    <section className="deal">
    <div className="content">
        <h3>Flash Promotion</h3>
        <h1>Dapatkan buku gratis kamu sekarang!</h1>
        <p>Kamu bisa mendapatkan akses gratis buku premium terbaik dengan mengundang teman sebanyak 3 orang!</p>
        <Link to="#" className="btn" onClick={getReferalPartner}>Ya, saya mau</Link>
    </div>
    <div className="image">
        <img src={ImgDeal} alt=""/>
    </div>
    </section>
  );
}
