import React from 'react';
import { Link } from 'react-router-dom';
import Book1 from 'assets/image/book-1.png';
import Book2 from 'assets/image/book-2.png';
import Book3 from 'assets/image/book-3.png';
import Book4 from 'assets/image/book-4.png';
import Book5 from 'assets/image/book-5.png';
import Book6 from 'assets/image/book-6.png';

export default function HomeSection() {
    const showLoginComponent = () => {
        let loginForm = document.querySelector('.login-form-container');
        loginForm.classList.toggle('active');
    }
    const BookHomeSlider = [
        Book1, Book2, Book3, Book4, Book5, Book6
    ]
    return (
        <section className="home" id="home">
            <div className="row">

                <div className="content">
                    <h3>Terbitkan Karya Terbaikmu Sekarang!</h3>
                    <p>Tunjukan karya terbaik kamu kepada dunia dan tingkatan penghasilan bersama 1000+ komunitas publisher kami diseluruh wilayah Indonesia.</p>
                    <Link to="/" className="btn" onClick={showLoginComponent}>Daftar Menjadi Penulis</Link>
                </div>

                <div className="swiper books-slider">
                    <div className="swiper-wrapper">
                       {
                           BookHomeSlider.map((item, key) =>{
                               return ( <Link key={key} to="/" className="swiper-slide"><img src={item} alt="" /></Link>)
                           })
                       }
                    </div>
                    <img src="image/stand.png" className="stand" alt="" />
                </div>

            </div>
        </section>
    );
}
