import React from 'react';

export default function Newsletter() {
  return (
    <section className="newsletter">

    <form action="">
        <h3>Beritahu saya info produk baru setiap minggu!</h3>
        <input type="email" name="" placeholder="Masukan e-mail kamu..." id="" className="box"/>
        <input type="submit" value="Berlangganan" className="btn"/>
    </form>

    </section>
  );
}
