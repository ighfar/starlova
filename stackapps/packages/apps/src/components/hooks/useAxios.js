import { useState, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import ApiConfig from 'config';

export const useAxios = (axiosParams) => {
    const [response, setResponse] = useState(undefined);
    const [error, setError] = useState('');
    const [loading, setLoading] = useState(true);
    const dispatch = useDispatch();

    const fetchData = async (params) => {
        try {
            const result = await ApiConfig.request(params);
            setResponse(result.data);

        } catch (error) {
            setError(error);
        } finally {
            setLoading(false);
        }
    };

    useEffect(() => {
        fetchData(axiosParams);
    }, []); // execute once only

    return { response, error, loading };
};

export default useAxios;