import Swiper from 'swiper';

export default function BlogSlider() {
    new Swiper(".blogs-slider", {
        spaceBetween: 10,
        grabCursor:true,
        loop:true,
        centeredSlides: true,
        autoplay: {
          delay: 9500,
          disableOnInteraction: false,
        },
        breakpoints: {
          0: {
            slidesPerView: 1,
          },
          768: {
            slidesPerView: 2,
          },
          1024: {
            slidesPerView: 3,
          },
        },
    });
}
