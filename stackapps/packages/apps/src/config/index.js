import axios from "axios";

export const baseURL = {
    api: process.env.NODE_ENV !== 'production' ? process.env.REACT_APP_WEB_API : 'https://api-snipkode.cloud.okteto.net',
    apps: process.env.NODE_ENV !== 'production' ? process.env.REACT_APP_WEB_APP : 'https://app-snipkode.cloud.okteto.net',
    admin: process.env.NODE_ENV !== 'production' ? process.env.REACT_APP_WEB_ADMIN : 'https://admin-snipkode.cloud.okteto.net',
}

const ApiConfig = axios.create({
    withCredentials: true,
    baseURL: process.env.NODE_ENV !== 'production' ? `${baseURL.api}/api/v2/` : '/api/v2'
 })

export default ApiConfig ;