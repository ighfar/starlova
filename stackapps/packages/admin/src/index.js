import React from 'react';
import ReactDOM from 'react-dom';
import App from 'App';
import 'assets/css/variable.css';
import 'assets/css/animations.css';
import "assets/css/icons.css";
import 'assets/css/layouts.css';
import 'assets/css/login.css';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import { store } from 'stores';
import 'bootstrap/dist/css/bootstrap.min.css';

ReactDOM.render(
  <React.StrictMode>
    <BrowserRouter>
      <Provider store={store}>
        <App />
      </Provider>
    </BrowserRouter>
  </React.StrictMode>,
  document.getElementById('root')
);
