import { useBreakpoints } from 'components/hooks/useBreakpoints';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { ubahPreview } from 'redux/LayoutSlices';

export default function BookOverlay({children}) {
  const dispatch = useDispatch();
  const { book } = useSelector((state) => state.layout);
  const { isMobileSmall, isMobileMid, isMobileFloor, isTabletFloor, isTabletMid, isTabletCeil, isMobileXSmall } = useBreakpoints();
  const isSmallDevice = isMobileSmall || isMobileMid || isMobileFloor || isTabletFloor || isTabletMid || isTabletCeil || isMobileXSmall;
  const overlays = document.querySelector('.overlay-container');
  const hideContainer = () => {
        dispatch(
            ubahPreview({
                zoom: !book.zoom,
            })
        )
        overlays && overlays.classList.remove('active');
  }

  React.useState(() => {

  }, [book.zoom])
  return (
      <React.Fragment>
          <div className='overlay-container preview-books' id="form-container">
             {children}
             <div className='popup' id="close-popup" onClick={hideContainer}>
                 <span className={`material-icons ${isSmallDevice ? "md-dark": "md-light"} md-30`}>cancel</span>
            </div>
          </div>
      </React.Fragment>
  );
}
