import React from 'react';

export default function FormProductOverlay({children}) {
  const hideContainer = () => {
      const overlays = document.querySelector('.overlay-container');
            overlays.classList.remove('active');
  }
  return (
      <React.Fragment>
          <div className='overlay-container' id="form-container">
             {children}
             <div className='popup' id="close-popup" onClick={hideContainer}>
                 <span className='material-icons md-light md-28'>cancel</span>
            </div>
          </div>
      </React.Fragment>
  );
}
