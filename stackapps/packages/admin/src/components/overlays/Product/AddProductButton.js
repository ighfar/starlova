import React from 'react';

export default function AddProductButton() {
  const showOverlays = () => {
      const overlay = document.querySelector('.overlay-container');
            overlay.classList.toggle('active');
  }
  return (
      <React.Fragment>
        <div className='add-button-product' onClick={showOverlays}>
            <span className='material-icons md-36'>add_circle</span>
        </div>
      </React.Fragment>
  );
}
