import React from 'react';
import MetaTags from 'react-meta-tags';
import Thumb from 'assets/images/admin.png';
import { useSelector } from 'react-redux';
import Breadcrumb from 'components/shared/Breadcrumb';

export default function Content({ children }) {
    const {heading, meta } = useSelector((state) => state.layout);
    return (<>
        <Breadcrumb label={heading?.label} icon={heading?.icon} />
        <MetaTags>
            <title>Writer - {meta?.title}</title>
            <meta name="description" content={meta?.description} />
            <meta property="og:title" content={`Admin ${meta?.title}`} />
            <meta property="og:image" content={Thumb} />
        </MetaTags>
        <div className='master-admin-content'>
            {children}
        </div>
    </>
    );
}
