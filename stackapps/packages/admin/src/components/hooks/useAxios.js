import { useState, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { setFetchData } from 'redux/ApiSlices';
import ApiConfig from 'components/config';

export const useAxios = (axiosParams) => {
    const [response, setResponse] = useState(undefined);
    const [error, setError] = useState('');
    const [loading, setLoading] = useState(true);
    const dispatch = useDispatch();

    const fetchData = async (params) => {
        try {
            const result = await ApiConfig.request(params);
            setResponse(result.data);
            dispatch(
                setFetchData({ result: result.data, error: false, loading: false })
            )
        } catch (error) {
            setError(error.response.data);
            dispatch(
                setFetchData({ data: null, error: true, loading: false })
            )
        } finally {
            setLoading(false);
        }
    };

    useEffect(() => {
        fetchData(axiosParams);
    }, []); // execute once only

    return { response, error, loading };
};

export default useAxios;