import React, { useState } from 'react';
import { NavLink } from 'react-router-dom';
import { AdminData, UserData } from 'components/json/SidebarData';
import { useSelector, useDispatch} from 'react-redux';
import { setSidebar } from 'redux/SidebarSlices';

export default function Sidebar({ toggle, setToggle }) {
	const { params, roles } = useSelector((state)=> state.auth);
	const [index, setIndex] = useState(0);
	const [data, setData] = useState(UserData);
	const handleClick = (key, e) => {
		setToggle(false)
		setIndex(key)
	}

	React.useEffect(() => {
		const isAdmin = roles === 'admin';
		if (isAdmin) {
			setData(AdminData)
		}
	}, [])
	
	return (
		<div className="master-admin-sidebar">
			<div className='admin-sidebar'>
				{
					data.map((item, key) => {
						return (
							<NavLink
								to={`${item.path + params}`}
								className="master-admin-link"
								key={key}
								style={({ isActive }) => (
									{
										background: index == key && isActive ? 'var(--blue-light)' : null,
										color: index == key && isActive && 'var(--text-white)',
									}
								)}
								onClick={(e) => handleClick(key, e)}>
								<span className="material-icons md-26">{item.icon}</span>
								<div className="sidebar-text-icons">
									{item.label}
								</div>
							</NavLink>
						)
					})
				}
			</div>
		</div>
	);
}
