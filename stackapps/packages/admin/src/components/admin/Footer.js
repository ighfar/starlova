import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export default class Footer extends Component {
	render() {
		return (
			<div className="admin-footer">
				<div className="footer-items">
					<Link to='/admin/penerbit?id=' className="footer-item">
						<span className="material-icons md-26 md-light">account_tree</span>
						<span className="footer-panel">Starlova Publishing</span>
					</Link>
					<Link to='/admin/penerbit?id=' className="footer-item">
						<span className="material-icons md-26 md-light">account_tree</span>
						<span className="footer-panel">Gramedia Indonesia</span>
					</Link>
					<Link to='/admin/penerbit?id=' className="footer-item">
						<span className="material-icons md-26 md-light">account_tree</span>
						<span className="footer-panel">Aneka Ilmu</span>
					</Link>
					<Link to='/admin/penerbit?id=' className="footer-item">
						<span className="material-icons md-26 md-light">account_tree</span>
						<span className="footer-panel">Arkana Putra</span>
					</Link>
				</div>
			</div>
		);
	}
}
