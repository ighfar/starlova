import React, { useEffect, useState } from 'react';
import admin from "assets/images/admin.png";
import { NavLink, useNavigate } from 'react-router-dom';
import { useBreakpoints } from '../hooks/useBreakpoints';
import { setSidebar } from 'redux/SidebarSlices';
import { useDispatch, useSelector } from 'react-redux';
import ApiConfig from 'components/config';
import { Button, Col, Container, Dropdown, DropdownButton, Offcanvas, Row } from 'react-bootstrap';


export default function Header({ toggle, setToggle }) {
	const { params, roles } = useSelector((state) => state.auth);
	const profile = JSON.parse(localStorage.getItem("profile"));
	const switchPath = roles === 'admin' ? '/tenant' : '/writer';
	const { isMobileSmall, isMobileMid, isMobileFloor, isTabletFloor, isTabletMid, isTabletCeil, isMobileXSmall } = useBreakpoints();
	const isSmallDevice = isMobileSmall || isMobileMid || isMobileFloor || isTabletFloor || isTabletMid || isTabletCeil || isMobileXSmall;

	const [show, setShow] = useState(false);

	const handleClose = () => setShow(false);
	const handleShow = () => setShow(true);

	// console.log(sidebar)
	const handleClick = () => {
		setToggle(!toggle)
	}

	const navigate = useNavigate();
	const SignOut = () => {
		ApiConfig.post(`/writer/auth/signout`).then((success) => {
			localStorage.removeItem("profile");
			alert("Are you sure want to sign out ?")
			if (success) return navigate('/auth');
		})
			.catch((error) => {
				console.log(error.response.data)
			})
	}

	React.useEffect(() => {
		!isSmallDevice && setToggle(!toggle);
	}, [])

	React.useEffect(() => {
		// console.log(profile)
	}, [profile])

	React.useEffect(() => {
		// console.log(switchPath);
		document.documentElement.style.setProperty('--sidebar-width', toggle ? "260px" : "60px");
		document.documentElement.style.setProperty('--display-state', toggle ? "block" : "none");
		document.documentElement.style.setProperty('--content-state', toggle ? "flex-start" : "center");

	}, [toggle])

	return (
		<div className='master-admin-header'>
			<div className='admin-logo-container'>
				<img src={admin} alt="admin-logo" className='admin-logo' />
				<div className="admin-brand">Admin Panel</div>
				<div className='master-admin-icons toggle' onClick={handleClick}>
					<i className="material-icons md-34 md-light" >double_arrow</i>
				</div>
			</div>
			<div className='master-admin-search-bar'>
				<input type='text' placeholder='Cari..' />
			</div>
			<div className='master-admin-header-menu'>

				<NavLink to={`${switchPath}/notify${params}`} className='header-icons'>
					<i className="material-icons header-menu">circle_notifications</i>
					<span>Pemberitahuan</span>
				</NavLink>

				{
					profile?.email && (
						<NavLink to={`${switchPath}/akun${params}`} className='header-icons'>
							<i className="material-icons header-menu md-26">admin_panel_settings</i>
							<span>{profile?.email}</span>
						</NavLink>
					)
				}


				<NavLink to="/" onClick={SignOut} className='header-icons'>
					<i className="material-icons header-menu md-26">exit_to_app</i>
				</NavLink>
				<div className="header-icons off-canvas" onClick={handleShow}>
					<i className="material-icons md-26">menu</i>
				</div>
				<Offcanvas show={show} onHide={handleClose} placement='end'>
					<Offcanvas.Header closeButton className='bg-white'>
						<Offcanvas.Title>Akun Saya</Offcanvas.Title>
					</Offcanvas.Header>
					<Offcanvas.Body className='bg-secondary'>
							<Row>
								<Col xs='8'>
									{
										profile?.email && (
											<Button
												size='sm'
												variant='success'
												className='d-flex align-items-center mb-2'
												style={{ maxWidth: '250px' }}
											>
												<i className="material-icons header-menu me-1">admin_panel_settings</i>
												<span>{profile.email}</span>
											</Button>
										)
									}
								</Col>
								<Col xs='4'>
									<Button onClick={SignOut} size='sm'
										style={{ width: '100%', overflow: 'hidden' }}
										variant='danger' className='d-flex align-items-center'>
										<i className="material-icons header-menu me-1">exit_to_app</i>
										<span>Exit</span>
									</Button>
								</Col>
							</Row>
					</Offcanvas.Body>
				</Offcanvas>
			</div>
		</div>
	)
}
