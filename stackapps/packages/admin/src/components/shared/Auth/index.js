import ApiConfig from 'components/config';
import React, { useEffect, useState } from 'react'
import { useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { setProfile } from 'redux/ProfileSlices';
import Encrypt from 'utils/Encrypt';
import Loader from '../Loader';
export default function Auth() {

    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(false);
    const [token, setToken] = useState("");
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const navigate = useNavigate();
    const dispatch = useDispatch();
    const profileStoreLocal = localStorage.getItem('profile');

    const handleOnSubmit = async (e) => {
        e.preventDefault();
        ApiConfig.post("/writer/auth/login", { email, password }).then((response) => {
            setTimeout(()=> setLoading(false), 2500)
            const {email, token } = response.data;
            localStorage.setItem('profile', JSON.stringify({ email, secret: Encrypt(Encrypt(token))}));
            window.location.reload();
            })
        .catch((error) => {
            setTimeout(() => {
                setLoading(false);
                setError(error.response.data);
            }, 2500)
        })
    }

    const HandleLoginButton = (e) => {
        setLoading(true);
        setError(false);
        
    }

    useEffect(() => {
        // console.log(profileStoreLocal)
        profileStoreLocal && dispatch(setProfile(JSON.parse(profileStoreLocal)));
    }, [])

    return (
        <React.Fragment>
            <Loader loading={loading}/>
            <div className="container_login animation-fades">
                <div className="screen">
                    <div className="screen__content">
                        { error && <span className='warning' >{error.message}</span> } 
                        <form className="login" onSubmit={handleOnSubmit}>
                            <div className="login__field">
                                <i className="login__icon material-icons">account_circle</i>
                                <input 
                                    type="text"
                                    value={email}
                                    onChange={(e) => setEmail(e.target.value) }
                                    className="login__input" 
                                    placeholder="User name / Email" />
                            </div>
                            <div className="login__field">
                                <i className="login__icon material-icons">lock</i>
                                <input 
                                    type="password" 
                                    value={password}
                                    onChange={(e) => setPassword(e.target.value)}
                                    className="login__input" 
                                    placeholder="Password" />
                            </div>
                            <button className="button login__submit" onClick={HandleLoginButton}>
                                <span className="button__text">{ loading ? "Loading..." : "Login Admin"}</span>
                                <i className="button__icon material-icons">double_arrow</i>
                            </button>
                        </form>
                        {/* <div className="social-login">
                            <h3>Log in via</h3>
                            <div className="social-icons">
                                <a href="#" className="social-login__icon material-icons">facebook</a>
                                <a href="#" className="social-login__icon material-icons"></a>
                                <a href="#" className="social-login__icon material-icons"></a>
                            </div>
                        </div> */}
                    </div>
                    <div className="screen__background">
                        <span className="screen__background__shape screen__background__shape4"></span>
                        <span className="screen__background__shape screen__background__shape3"></span>
                        <span className="screen__background__shape screen__background__shape2"></span>
                        <span className="screen__background__shape screen__background__shape1"></span>
                    </div>
                </div>
            </div>
        </React.Fragment>
    )
}