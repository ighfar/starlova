import React from 'react'
import { Button } from 'react-bootstrap'

export default function ButtonIcon({ title, icon, style, className, variant = 'primary', disabled = false, ...rest}) {
    return (
        <>
            <Button variant={variant} size='sm' disabled={disabled} {...rest}
                className={className}
                style={{
                    ...style,
                    display: 'flex',
                    alignItems: 'center',
                    justifySelf: 'flex-end',
                    paddingRight: 12,
                }}
            >
                <span className='material-icons md-20 me-1'>{icon}</span>
                {title}
            </Button>
        </>
    )
}
