import { useBreakpoints } from 'components/hooks/useBreakpoints';
import React from 'react'
import { CardGroup, Col, Row, Card, Nav, Button } from 'react-bootstrap';
export const { Provider, Consumer } = React.createContext({ eventKey: null });
export default function Wrapper({ children, data }) {
    const { isMobileSmall, isMobileMid, isMobileFloor, isTabletFloor, isTabletMid, isTabletCeil, isMobileXSmall } = useBreakpoints();
    const [navKey, setKey] = React.useState(data[0].path);
    const isSmallDevice = isMobileSmall || isMobileMid || isMobileFloor || isTabletFloor || isTabletMid || isTabletCeil || isMobileXSmall;
    const handleSelect = (eventKey) => setKey(eventKey);
   
    return (
        <div>
            <Card style={{
                fontFamily: 'tahoma'
            }}>
                <Card.Header>
                    <Nav variant="tabs" defaultActiveKey={data[0].path} onSelect={handleSelect}>
                        {
                            data.map((item, key) => (
                                <Nav.Item key={key}>
                                    <Nav.Link href={item.path}>{item.title}</Nav.Link>
                                </Nav.Item>
                            ))
                        }
                    </Nav>
                </Card.Header>
                <Card.Body style={{ overflowX: isSmallDevice && 'scroll' }}>
                    <Provider value={navKey}>
                        {children}
                    </Provider>
                </Card.Body>
            </Card>
        </div>
    )
}
