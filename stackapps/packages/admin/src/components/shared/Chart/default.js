import React from 'react';
import {
  Chart as ChartJS,
  LinearScale,
  CategoryScale,
  BarElement,
  PointElement,
  LineElement,
  Legend,
  Tooltip,
} from 'chart.js';
import { Chart } from 'react-chartjs-2';

ChartJS.register(
  LinearScale,
  CategoryScale,
  BarElement,
  PointElement,
  LineElement,
  Legend,
  Tooltip
);

const labels = ['January', 'February', 'March', 'April', 'May', 'June', 'July'];

export const data = {
  labels,
  datasets: [
    {
      type: 'line',
      label: 'Pendapatan Bulanan',
      borderColor: 'rgb(255, 99, 132)',
      borderWidth: 2,
      fill: true,
      data: [],
    },
    {
      type: 'bar',
      label: 'Pendapatan Harian',
      backgroundColor: 'rgb(75, 192, 192)',
      data: [],
      borderColor: 'white',
      borderWidth: 2,
    },
    {
      type: 'bar',
      label: 'Pendapatan Tahunan',
      backgroundColor: 'rgb(53, 162, 235)',
      data: [],
    },
  ],
};

export default function Charts() {
  return <Chart type='bar' data={data} />;
}
