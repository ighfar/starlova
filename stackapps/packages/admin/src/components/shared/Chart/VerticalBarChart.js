import React from 'react';
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend,
} from 'chart.js';
import { Bar } from 'react-chartjs-2';
// import faker from 'faker';

ChartJS.register(
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend
);

export const options = {
  responsive: true,
  plugins: {
    legend: {
      position: 'top',
    },
    title: {
      display: true,
      text: 'Grafik Total Penerbitan',
    },
  },
};

const labels = ['January', 'February', 'March', 'April', 'May', 'June', 'July'];

export const data = {
  labels,
  datasets: [
    {
      label: 'PLATFORM',
      data: [12, 14, 12, 234, 32, 234,456, 43, 23],
      backgroundColor: 'rgba(255, 99, 132, 0.5)',
    },
    {
      label: 'PDF',
      data: [12, 14, 12, 234, 32, 234,456, 43, 23],
      backgroundColor: 'rgba(53, 162, 235, 0.5)',
    },
  ],
};

export default function VerticalBarChart() {
  return <Bar options={options} data={data} />;
}
