import React from 'react';
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend,
} from 'chart.js';
import { Line } from 'react-chartjs-2';
// import faker from 'faker';

ChartJS.register(
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend
);

export const options = {
  responsive: true,
  plugins: {
    legend: {
      position: 'top',
    },
    title: {
      display: true,
      text: 'Grafik Total Pendapatan',
    },
  },
};

const labels = ['January', 'February', 'March', 'April', 'May', 'June', 'July'];

export const data = {
  labels,
  datasets: [
    {
      label: 'AFF',
      data: [200000, 340000, 50000, 100000, 1140000, 130000, 65000],
      backgroundColor: 'rgb(255, 99, 132)',
      stack: 'Stack 0',
    },
    {
      label: 'TRX',
      data: [1145, 344, 456, 567, 677, 867, 123],
      backgroundColor: 'rgb(75, 192, 192)',
      stack: 'Stack 2',
    },
    {
      label: 'EARN',
      data: [8000000, 1900000, 860000, 9000000, 7288000, 4500000, 2400000, 3450000],
      backgroundColor: 'rgb(53, 162, 235)',
      stack: 'Stack 1',
    },
  ],
};

export default function LineChart() {
  return <Line options={options} data={data} />;
}
