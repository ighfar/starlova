import React from 'react'

export default function Loader(props) {
    const styles = {
        loaderLogo: {
            width: '35px',
            height: '35px',
            top: '45%',
            padding: 0,
            borderWidth: '3px'
        },

        loaderText: {
            position: 'absolute',
            top: '48.5%',
            zIndex: 1000,

        },
        flex: {
            gridColumn: ' span 16',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            flexDirection: 'column',
        }

    }

    return (
        <React.Fragment>
            {
                props.loading && (
                    <div style={styles.flex}>
                        <span className='master-admin-loader' style={styles.loaderLogo}></span>
                        {/* <span style={styles.loaderText}>Loading... </span> */}
                    </div>
                )
            }
        </React.Fragment>
    )
}
