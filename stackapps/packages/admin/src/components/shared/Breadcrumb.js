import React from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { ubahPreview } from 'redux/LayoutSlices';

const styles = {
    button: {
        flex: 1,
        padding: '6px',
        textAlign: 'end',
    },
    active: {
        color: 'grey',
    }
}

function Breadcrumb({ label, icon}) {
    const dispatch = useDispatch();
    const { book } = useSelector((state) => state.layout)
    const overlay = document.querySelector('.preview-books');
    const showOverlays = () => {
        dispatch(
            ubahPreview({
               zoom: !book.zoom
            })
        );

        overlay && overlay.classList.toggle('active');
        
    }
    React.useEffect(() => {
        
    }, [book])
    
    return (
        <div className='master-admin-heading-page'>
            <div className='page-title'>
                <div className="material-icons md-26" style={styles.button}>{icon}</div>
                <div className='title'>{label}</div>

            </div>
            <div className='view-content-type'>
                <div 
                    className="material-icons md-26 active" 
                    style={styles.button}>grid_view</div>
                <div 
                    onClick={showOverlays}
                    className="material-icons md-26" 
                    style={{ 
                    ...styles.button, 
                    ...styles.active 
                    }}
                >
                    zoom_out_map
                </div>
            </div>
        </div>
    )
}

export default Breadcrumb
