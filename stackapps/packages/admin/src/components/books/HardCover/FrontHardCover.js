import React from 'react';
import { useBreakpoints } from 'components/hooks/useBreakpoints';
import StandImg from 'assets/clients/stand.png';
import LoveBookImg from 'assets/clients/book-9.png';

const FrontHardCover =  React.forwardRef((props, ref) => {
    const { isMobileSmall, isMobileMid, isMobileFloor, isTabletFloor } = useBreakpoints();
    const isSmallDevice = isMobileSmall || isMobileMid || isMobileFloor || isTabletFloor;
    return (
        <div 
            className="page hard" 
            data-density={ isSmallDevice ? "soft" : "hard"} 
            ref={ref}
        >
            <div 
                className='hard-logo' 
                style={{ 
                     width: isSmallDevice  ? '100%' : 'auto',
                }}
            >
                <img src={LoveBookImg} width='50%' alt='Membuat Kamu Bahagia'/>
                <img src={StandImg} width='100%' alt='Book Show Stand'/>
                {
                    isSmallDevice  ?  (<h4>Start Reading</h4>) : null
                }
           </div>
           
        </div>
    )
})


export default FrontHardCover;
