import React from 'react';
import { useBreakpoints } from 'components/hooks/useBreakpoints';

const BackHardCover =  React.forwardRef((props, ref) => {
    const { isMobileSmall, isMobileMid, isMobileFloor, isTabletFloor } = useBreakpoints();
    const isSmallDevice = isMobileSmall || isMobileMid || isMobileFloor || isTabletFloor ;
    return (
        <div 
            className="page hard" 
            data-density={isSmallDevice ? "soft" : "hard"} 
            ref={ref}>
           
        </div>
    )
})
export default BackHardCover;
