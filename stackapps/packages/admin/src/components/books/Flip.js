import React from 'react';
import { PageFlip } from 'page-flip';
import { useBreakpoints } from 'components/hooks/useBreakpoints';
import StandImg from 'assets/clients/stand.png';
import LoveBookImg from 'assets/clients/book-9.png';
import { BookPreview } from 'components/json/BookPreview';

export default function Flip() {
    const bookRef = React.useRef();
    const { isMobileSmall, isMobileMid, isMobileFloor, isTabletFloor } = useBreakpoints();
    const [index, setIndex] = React.useState(0);
    const [pages, setPages] = React.useState(0);

    React.useEffect(()=> {
        const pageFlip = new PageFlip(bookRef.current, {
            width: window.innerWidth, // required parameter - base page width
            height: isMobileSmall || isMobileMid || isMobileFloor || isTabletFloor  ? window.innerHeight : window.outerHeight, // required parameter - base page height
            size: isMobileMid || isMobileFloor || isTabletFloor  ? 'fixed' : 'stretch',
            drawShadow: isMobileSmall || isMobileMid || isMobileFloor || isTabletFloor  ? true : true,
            maxShadowOpacity: 0.35,
            showCover: isMobileSmall || isMobileMid || isMobileFloor || isTabletFloor  ? true : false,
            usePortrait: isMobileSmall || isMobileMid || isMobileFloor || isTabletFloor  ? true : false,
            mobileScrollSupport: true,
            clickEventForward: true, // support only anchor tag & button => <a href='#'>1</a>
            autoSize: isMobileSmall || isMobileMid || isMobileFloor || isTabletFloor  ? false : true,
            startZIndex: 10000,
            startPage:index,
            swipeDistance: 150,
        });
        
        pageFlip.loadFromHTML(document.querySelectorAll('.page'));
        const flipPages = pageFlip.getPageCount();
        pageFlip.on('flip', (e) => {
            setIndex(e.data);
        });

        setPages(BookPreview?.length)
   }, [])
  return (
    <div className="flip-book" ref={bookRef}>
        <div className="page hard" data-density="hard">
            <div className='hard-logo' style={{ 
                width: isMobileSmall || isMobileMid || isMobileFloor || isTabletFloor  ? '100%' : 'auto',
             }}>
                <img src={LoveBookImg} width='80%' alt='Membuat Kamu Bahagia'/>
                <img src={StandImg} width='100%' alt='Book Show Stand'/>
                {
                    isMobileSmall || isMobileMid || isMobileFloor || isTabletFloor  ?  (<h4>Start Reading</h4>) : null
                }
           </div>
        </div>
        <div className="page page-cover page-cover-top">
            <div className='page-content'>
                <div className='page-header'>Sinopsis</div>
                <div className='page-text'>
                Congue mauris rhoncus aenean vel elit scelerisque mauris pellentesque pulvinar. Imperdiet proin fermentum leo vel. Maecenas volutpat blandit aliquam etiam erat velit scelerisque in dictum. Sed turpis tincidunt id aliquet risus feugiat in. Luctus venenatis lectus magna fringilla. Praesent tristique magna sit amet. Vitae sapien pellentesque habitant morbi tristique senectus et. Et netus et malesuada fames ac turpis egestas sed. Pellentesque massa placerat duis ultricies lacus sed turpis tincidunt. Gravida neque convallis a cras semper auctor neque vitae tempus. Molestie a iaculis at erat pellentesque adipiscing commodo elit. Adipiscing elit duis tristique sollicitudin nibh sit amet commodo. Amet consectetur adipiscing elit duis tristique sollicitudin nibh sit. Ullamcorper sit amet risus nullam eget. Lobortis mattis aliquam faucibus purus in massa tempor nec feugiat. Eu mi bibendum neque egestas.
                </div>
                <div className='page-footer'>
                    <div className='author'>Alam Santiko Wibowo</div>
                </div>
            </div>
        </div>
            {
                BookPreview?.map((item,key)=> (
                    <div className={key % 2 === 0 ? "page --left" : "page --right"} key={key}>
                        <div className='page-content'>
                            <div className='page-header'>{item.title}</div>
                            <div className='page-text'>{item.words}</div>
                            <div className='page-text'>{item.words}</div>
                            <div className='page-footer'>Hal { key + 1} / {pages} </div>
                        </div>
                    </div>
                ))
            }
        <div className="page page-cover page-cover-bottom">
            <div className='page-content'>
                <div className='page-header'>Penutup</div>
                <div className='page-text'>
                    Congue mauris rhoncus aenean vel elit scelerisque mauris pellentesque pulvinar. Imperdiet proin fermentum leo vel. Maecenas volutpat blandit aliquam etiam erat velit scelerisque in dictum. Sed turpis tincidunt id aliquet risus feugiat in. Luctus venenatis lectus magna fringilla. Praesent tristique magna sit amet. Vitae sapien pellentesque habitant morbi tristique senectus et. Et netus et malesuada fames ac turpis egestas sed. Pellentesque massa placerat duis ultricies lacus sed turpis tincidunt. Gravida neque convallis a cras semper auctor neque vitae tempus. Molestie a iaculis at erat pellentesque adipiscing commodo elit. Adipiscing elit duis tristique sollicitudin nibh sit amet commodo. Amet consectetur adipiscing elit duis tristique sollicitudin nibh sit. Ullamcorper sit amet risus nullam eget. Lobortis mattis aliquam faucibus purus in massa tempor nec feugiat. Eu mi bibendum neque egestas.
                </div>
                <div className='page-footer'>...</div>
            </div>
        </div>
        <div className="page hard" data-density="hard">
            <div className='page-content'>
                <div className='page-header'>Terima Kasih</div>
                <div className='page-text'>
                Congue mauris rhoncus aenean vel elit scelerisque mauris pellentesque pulvinar. Imperdiet proin fermentum leo vel. Maecenas volutpat blandit aliquam etiam erat velit scelerisque in dictum. Sed turpis tincidunt id aliquet risus feugiat in. Luctus venenatis lectus magna fringilla. Praesent tristique magna sit amet. Vitae sapien pellentesque habitant morbi tristique senectus et. Et netus et malesuada fames ac turpis egestas sed. Pellentesque massa placerat duis ultricies lacus sed turpis tincidunt. Gravida neque convallis a cras semper auctor neque vitae tempus. Molestie a iaculis at erat pellentesque adipiscing commodo elit. Adipiscing elit duis tristique sollicitudin nibh sit amet commodo. Amet consectetur adipiscing elit duis tristique sollicitudin nibh sit. Ullamcorper sit amet risus nullam eget. Lobortis mattis aliquam faucibus purus in massa tempor nec feugiat. Eu mi bibendum neque egestas.
                </div>
                <div className='page-footer'>...</div>
            </div>
        </div>
    </div>
  )
}
