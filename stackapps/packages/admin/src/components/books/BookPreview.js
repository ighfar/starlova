import React, { useRef, useState } from 'react';
import HTMLFlipBook from 'react-pageflip';
import { useBreakpoints } from '../hooks/useBreakpoints';
import 'components/books/Styles/book.css';
import BackHardCover from './HardCover/BackHardCover';
import FrontHardCover from './HardCover/FrontHardCover';
import FrontCoverPage from './PageCover/FrontCoverPage';
import BackCoverPage from './PageCover/BackCoverPage';
import { BookPreview } from 'components/json/BookPreview';
import DisableInspect from 'misc/DisableInspect';
import LoveBookImg from 'assets/clients/book-9.png';
import LoveBookImg2 from 'assets/clients/book-8.png';
import { useSelector } from 'react-redux';

export default function PreviewBook(props) {
    const BookPageRef = useRef();
    const {book} = useSelector((state) => state.layout);

    const { isMobileSmall, isMobileMid, isMobileFloor, isTabletFloor, isTabletMid, isTabletCeil, isMobileXSmall } = useBreakpoints();
    const isSmallDevice = isMobileSmall || isMobileMid || isMobileFloor || isTabletFloor || isTabletMid || isTabletCeil || isMobileXSmall;

    const onFlip = React.useCallback((e) => {
        // console.log('Current page: ', e);
        // console.log(BookPageRef.current);
    }, []);

    const onInit = React.useCallback((e) => {
        // console.log('on init: ', e);
        GetOrientation()
    }, []);

    const onChangeOrientation = React.useCallback((e) => {
        // console.log('on change orientation: ', e);
    }, []);

    const onChangeState = React.useCallback((e) => {
        // console.log('on onChangeState: ', e);
    }, []);

    const onUpdate = React.useCallback((e) => {
        // console.log('on update: ', e);
    }, []);

    const GetOrientation = () => {
       const value = BookPageRef.current.pageFlip().getOrientation();
    //    console.log(value)
    }

    const GetCurrentPageIndex = () => {
        const value = BookPageRef.current.pageFlip().getCurrentPageIndex();
        console.log(value)
     }

    const GetPageCount = () => {
        const value = BookPageRef.current.pageFlip().getPageCount();
        console.log(value)
    }

    const GetBoundsRect = () => {
        const value = BookPageRef.current.pageFlip().getBoundsRect();
        console.log(value)
    }

    const LoadFromImages = () => {
        BookPageRef.current.pageFlip().loadFromImages({
            images: [LoveBookImg, LoveBookImg2]
        });
    }


    const updateFromImages = () => {
        BookPageRef.current.pageFlip().updateFromImages({
            images: [LoveBookImg2, LoveBookImg]
        });
    }

    const LoadFromHtml = () => {
        BookPageRef.current.pageFlip().loadFromHtml({
            items: ['<h1>page1</h1>', '<h1>page1</h1>']
        });
    }

    const UpdateFromHtml = () => {
        BookPageRef.current.pageFlip().updateFromHtml({
            items: ['<h1>page1</h1>', '<h1>page1</h1>']
        });
    }

    const NextPaper = () => {
        BookPageRef.current.pageFlip().flipNext({
            corner: ['top', 'bottom']
        });
    }

    const PrevPaper = () => {
        BookPageRef.current.pageFlip().flipPrev({
            corner: ['top', 'bottom']
        });
    }

    const NextPaperWithoutAnimation = () => {
        BookPageRef.current.pageFlip().turnToNextPage({
            corner: ['top', 'bottom']
        });
    }

    const PrevPaperWithoutAnimation = () => {
        BookPageRef.current.pageFlip().turnToPrevPage({
            corner: ['top', 'bottom']
        });
    }

    const JumpPage = () => {
        BookPageRef.current.pageFlip().flip({
            pageNum: 8, corner: ['top', 'bottom']
        });
    }

    const JumpPageWithoutAnimation = () => {
        BookPageRef.current.pageFlip().turnToPage({
            pageNum: 8
        });
    }

    const CloseBooks = () => {
        BookPageRef.current.pageFlip().destroy();
    }

    React.useEffect(()=> {
        // document.addEventListener('contextmenu', (e) => {
        //     e.preventDefault();
        //     // alert('Hayo mau ngapain ?');
        // });
        // document.addEventListener('keydown', (e) => {
        //     e.preventDefault();
            
        //   });
        // document.addEventListener('onmousedown', (e) => {
        //     e.preventDefault();
        // });
        DisableInspect();
    }, [book])

    return (
        <HTMLFlipBook 
            ref={BookPageRef}
            startZIndex={600000}
            width={ isSmallDevice ? window.innerWidth - (book.zoom ? 0 : 90): (book.zoom ? window.innerWidth / 3.5: window.innerWidth / 3.2)}
            height={isSmallDevice ? window.innerHeight : window.innerHeight - (book.zoom ? 100 : 200)} size="fixed"
            minWidth={315}
            // maxWidth={1000}
            // minHeight={400}
            // maxHeight={1533}
            maxShadowOpacity={0.4}
            showCover={isSmallDevice ? false : false}
            autoSize={false}
            disableFlipByClick={isSmallDevice ? true : false}
            flippingTime={1500}
            mobileScrollSupport={true}
            swipeDistance={isSmallDevice ? 50 :  50}
            onFlip={onFlip}
            onInit={onInit}
            onUpdate={onUpdate}
            onChangeState={onChangeState}
            onChangeOrientation={onChangeOrientation}
            renderOnlyPageLengthChange={false}
        >
          <FrontHardCover/>
            <FrontCoverPage/>
            {
                BookPreview?.map((item,key)=> (
                    <div className={key % 2 === 0 ? "page --left" : "page --right"} key={key}>
                        <div className='page-content'>
                            <div className='page-header'>{item.title}</div>
                            <div className='page-text'>{item.words}</div>
                            <div className='page-text'>{item.words}</div>
                            <div className='page-footer'>Hal { key + 1} / {BookPreview.length} </div>
                        </div>
                    </div>
                ))
            }
            <BackCoverPage/>
          <BackHardCover/>
        </HTMLFlipBook>
    );
}