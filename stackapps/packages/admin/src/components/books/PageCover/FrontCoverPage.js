import React from 'react';

const FrontCoverPage =  React.forwardRef((props, ref) => {
    return (
        <div className="page page-cover page-cover-top" ref={ref}>
            <div className='page-content'>
                <div className='page-header'>Sinopsis</div>
                <div className='page-text'>
                Congue mauris rhoncus aenean vel elit scelerisque mauris pellentesque pulvinar. Imperdiet proin fermentum leo vel. Maecenas volutpat blandit aliquam etiam erat velit scelerisque in dictum. Sed turpis tincidunt id aliquet risus feugiat in. Luctus venenatis lectus magna fringilla. Praesent tristique magna sit amet. Vitae sapien pellentesque habitant morbi tristique senectus et. Et netus et malesuada fames ac turpis egestas sed. Pellentesque massa placerat duis ultricies lacus sed turpis tincidunt. Gravida neque convallis a cras semper auctor neque vitae tempus. Molestie a iaculis at erat pellentesque adipiscing commodo elit. Adipiscing elit duis tristique sollicitudin nibh sit amet commodo. Amet consectetur adipiscing elit duis tristique sollicitudin nibh sit. Ullamcorper sit amet risus nullam eget. Lobortis mattis aliquam faucibus purus in massa tempor nec feugiat. Eu mi bibendum neque egestas.
                </div>
                <div className='page-footer'>
                    <div className='author'>Alam Santiko Wibowo</div>
                </div>
            </div>
        </div>
    )
})

export default FrontCoverPage;
