const AdminData = [
    {
        icon: 'space_dashboard',
        label: 'Dashboard',
        path: '/tenant/dashboard'
    },

    {
        icon: 'inventory_2',
        label: 'Manage Produk',
        path: '/tenant/produk'
    },

    {
        icon: 'sell',
        label: 'Manage Pesanan',
        path: '/tenant/pesanan'
    },
    {
        icon: 'credit_card',
        label: 'Pendapatan',
        path: '/tenant/pendapatan'
    },

    {
        icon: 'add_moderator',
        label: 'Manage Admin',
        path: '/tenant/roles'
    },
    {
        icon: 'manage_accounts',
        label: 'Manage Users',
        path: '/tenant/users'
    },
    {
        icon: 'published_with_changes',
        label: 'Approval',
        path: '/tenant/approval'
    },

    {
        icon: 'feed',
        label: 'Manage Publishing',
        path: '/tenant/review'
    },

    {
        icon: 'gavel',
        label: 'Peraturan Platform',
        path: '/tenant/terms'
    },

]

const UserData = [

    {
        icon: 'book',
        label: 'Semua Buku',
        path: '/writer/books'
    },

    {
        icon: 'sell',
        label: 'Manage Pesanan',
        path: '/writer/pesanan'
    },

    {
        icon: 'inventory_2',
        label: 'Manage Karya',
        path: '/writer/produk'
    },


    {
        icon: 'credit_card',
        label: 'Pendapatan',
        path: '/writer/pendapatan'
    },
    {
        icon: 'gavel',
        label: 'Peraturan Platform',
        path: '/writer/terms'
    },

]

export { AdminData, UserData };