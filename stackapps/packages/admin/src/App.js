import React, { useEffect } from 'react';
import MasterAdminLayout from 'layouts/MasterAdminLayout';
import MasterClientLayout from 'layouts/MasterClientLayout';
import { Route, Routes} from 'react-router-dom';
import Loader from 'components/shared/Loader';
import SuperAdminRoutes, { WriterAdminRoutes } from 'routes/SuperAdminRoutes';
import Auth from 'pages/Auth';
import BookPreview from 'components/books/BookPreview';
import { useSelector } from 'react-redux';
import useSession from 'api/useSession';

const AdminRoutes = React.lazy(() => import('routes/AdminRoutes'));
const WriterRoutes = React.lazy(() => import('routes/WriterRoutes'));

const SuspenseLoader = () => {
     return (
          <Loader loading={true} />
     )
}

const Admin = () => {
     return (
          <React.Suspense fallback={<SuspenseLoader />}>
               <MasterAdminLayout>
                    <AdminRoutes />
               </MasterAdminLayout>
          </React.Suspense>
     )
}

const Klien = () => {
     return (
          <React.Suspense fallback={<SuspenseLoader />}>
               <MasterClientLayout>
                    <WriterRoutes />
               </MasterClientLayout>
          </React.Suspense>
     )
}

export default function App() {
     useSession();
     const { isLoggedIn, roles } = useSelector( (state) => state.auth );
     return (
          <Routes>
                <Route 
                    path='/*' 
                    element={<Loader loading={true}/>} 
                />
                <Route 
                    path='auth/*' 
                    element={<Auth/>} 
                />
               <Route element={<WriterAdminRoutes isLoggedIn={isLoggedIn} roles={roles}/>}>
                         <Route 
                              path={`writer/*`} 
                              element={<Klien roles={roles}/>} 
                         />
               </Route>
               <Route element={<SuperAdminRoutes isLoggedIn={isLoggedIn} roles={roles}/>}>
                    <Route
                         path={`tenant/*`}
                         element={<Admin />}
                    />
               </Route>
               <Route path='books' element={<BookPreview />} />
               <Route 
                    path="*" 
                    element={<div>Tidak ditemukan halaman disini!</div>} 
               />
          </Routes>
     );
}