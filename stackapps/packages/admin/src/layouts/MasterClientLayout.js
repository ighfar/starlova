import React from 'react';
import Footer from 'components/admin/Footer';
import Header from 'components/admin/Header';
import Sidebar from 'components/admin/Sidebar';
import Content from 'components/clients/Content';

export default function MasterAdminLayout({ children }) {
	const [toggle, setToggle] = React.useState(false);
	return (
		<div className="master-admin">
			<Header toggle={toggle} setToggle={setToggle}/>
			<Sidebar toggle={toggle} setToggle={setToggle}/>
			<Content>
				{children}
			</Content>
			<Footer />
		</div >
	);
}
