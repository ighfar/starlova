import LayoutSlices from 'redux/LayoutSlices';
import ApiSlices from 'redux/ApiSlices';
import AuthSlices from 'redux/AuthSlices';
import { configureStore } from '@reduxjs/toolkit';
import SidebarSlices from 'redux/SidebarSlices';

export const store = configureStore({
  reducer: {
    layout: LayoutSlices,
    apis: ApiSlices,
    auth: AuthSlices,
    sidebar: SidebarSlices
  },
})

