import React from 'react'
import { Route, Routes } from 'react-router-dom'
import AuthComp from "components/shared/Auth";
const styles = {
    NotFound: {
        gridColumn: 'span 16'
    }
}
export default function Auth() {
    return (
        <Routes>
            {/* <Route index element={<LoginOne />} /> */}
            <Route path="/" element={<AuthComp />} />
            <Route path="/*" element={<div style={styles.NotFound}>Tidak ditemukan halaman disini!</div>} />
        </Routes>
    )
}
