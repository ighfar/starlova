import React from 'react'
import ProductData from 'components/json/ProductData';
import { ubahIcon, ubahMeta } from 'redux/LayoutSlices';
import { useDispatch } from 'react-redux';
import { AddProductButton, FormProductOverlay } from 'components/overlays';

export default function Product({ action }) {
    const dispatch = useDispatch();
    
    React.useEffect(() => {
        dispatch(
            ubahMeta({
                title: 'Manage Produk',
                description: 'Manage Produk Starlova Publishing Apps',
            })
        )

        dispatch(
            ubahIcon({
                icon: 'inventory_2',
                label: 'Manage Produk'
            })
        )

    }, [])
    return (
        <>
            Produk container
            <FormProductOverlay>This is our product</FormProductOverlay>
            <AddProductButton></AddProductButton>
        </>
    )
}
