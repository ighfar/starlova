import React from 'react'
import Loader from 'components/shared/Loader'
import { useDispatch } from 'react-redux';
import { ubahIcon, ubahMeta } from 'redux/LayoutSlices';
import { Button } from 'react-bootstrap';

export default function Pendapatan() {
    const dispatch = useDispatch();

    const styles = {
        container: {
            gridColumn: 'span 16'
        }
    }
    React.useEffect(() => {
        dispatch(
            ubahMeta({
                title: 'Estimasi Pendapatan',
                description: 'Estimasi Pendapatan Admin Starlova Publishing Apps',
            })
        );

        dispatch(
            ubahIcon({
                icon: 'credit_card',
                label: 'Estimasi Pendapatan'
            })
        );
    }, [])
    return (
        <div style={styles.container}>
            {/* Pendapatan */}
            {/* <Loader loading={true} /> */}
            <Button variant='outline-primary' size='sm'>Check</Button>
        </div>
    )
}
