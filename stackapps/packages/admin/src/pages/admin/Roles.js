import React from 'react'
import Loader from 'components/shared/Loader';
import { ubahIcon, ubahMeta } from 'redux/LayoutSlices';
import { useDispatch } from 'react-redux';

export default function Roles({ action }) {
    const styles = {
        container: {
            gridColumn: 'span 16'
        }
    }
    const dispatch = useDispatch();

    React.useEffect(() => {
        dispatch(
            ubahMeta({
                title: 'Manage Admin',
                description: 'Manage Admin Roles Starlova Publishing Apps',
            })
        )

        dispatch(
            ubahIcon({
                icon: 'add_moderator',
                label: 'Manage Admin'
            })
        )
    }, [])
    return (
        <div style={styles.container}>
            <Loader loading={true} />
        </div>
    )
}
