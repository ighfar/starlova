import React from 'react'
import Loader from 'components/shared/Loader'
import TableResponsive from 'components/shared/Table/TableResponsive'
import { useDispatch } from 'react-redux';
import { ubahIcon, ubahMeta } from 'redux/LayoutSlices';

export default function Pesanan() {
    const dispatch = useDispatch();
    React.useEffect(() => {
        dispatch(
            ubahMeta({
                title: 'Manage Pesanan',
                description: 'Manage Pesanan Starlova Publishing Apps',
            })
        )
        dispatch(
            ubahIcon({
                icon: 'sell',
                label: 'Manage Pesanan'
            })
        );
    }, [])

    return (
        <>
            
        </>
    )
}
