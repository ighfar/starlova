import React from 'react'
import { useDispatch } from 'react-redux';
import { ubahMeta, ubahIcon } from 'redux/LayoutSlices';
import GroupBarChart from 'components/shared/Chart/GroupBarChart'
import LineChart from 'components/shared/Chart/LineChart';
import VerticalBarChart from 'components/shared/Chart/VerticalBarChart';
import MultiTypeChart from 'components/shared/Chart/MultiTypeChart';

export default function Dashboard() {
    const dispatch = useDispatch();
    React.useEffect(() => {
        dispatch(
            ubahIcon({
                icon: 'space_dashboard',
                label: 'Manage Dashboard'
            })
        )

        dispatch(
            ubahMeta({
                title: 'Dashboard',
                description: 'Overview Dashboard Starlova Publishing Apps',
            })
        )
    }, [])
    return (
        <React.Fragment>
            <div className='dashboard-admin animation-fades'>
                <div className='panel'><GroupBarChart /></div>
                <div className='panel'><LineChart /></div>
            </div>
            <div className='dashboard-admin animation-fades'>
                <div className='panel'>
                    <MultiTypeChart/>
                </div>
                <div className='panel'>
                    <VerticalBarChart/>
                </div>
            </div>
        </React.Fragment>
    )
}
