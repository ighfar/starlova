import React from 'react';
import { Form, Row, Col, Button, InputGroup, FormControl, CardGroup, Card, ButtonGroup } from 'react-bootstrap';
import { useBreakpoints } from 'components/hooks/useBreakpoints';
import Editor from 'components/admin/Editor';


export default function FormEditBook({ setMenulis }) {
    const textInit = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus non nisl fermentum, aliquam massa sed, rhoncus nisl. Quisque molestie posuere lectus, quis tempor nisl bibendum in. Praesent gravida est arcu, sed semper justo congue nec. Mauris dictum orci a lacus ultricies, eget accumsan libero luctus. Nunc a velit ut mi rhoncus iaculis. Ut gravida egestas nunc, in sodales lorem scelerisque id. Proin consequat volutpat auctor. Duis eget odio interdum, porttitor magna ut, tincidunt ipsum."
    const { isMobileSmall, isMobileMid, isMobileFloor, isTabletFloor, isTabletMid, isTabletCeil, isMobileXSmall } = useBreakpoints();
    const isSmallDevice = isMobileSmall || isMobileMid || isMobileFloor || isTabletFloor || isTabletMid || isTabletCeil || isMobileXSmall;
    return (
        <React.Fragment>
            <CardGroup className='animation-fades'>
                <Card>
                    <Card.Body>
                        <Form>
                            <Row className="mb-3">
                                <Form.Group as={Col} sm={12} md={12} controlId="formGridJudul" className={isSmallDevice ? 'mb-3' : 'mb-0'}>
                                    <Form.Label>Judul Buku</Form.Label>
                                    <Form.Control type="text" placeholder="Masukan Judul" />
                                </Form.Group>

                                {/* <Form.Group as={Col} sm={12} md={6} controlId="formGridPrice">
                                    <Form.Label>Nomor ISBN</Form.Label>
                                    <InputGroup as={Col} className="mb-0">
                                        <InputGroup.Text>ISBN</InputGroup.Text>
                                        <FormControl aria-label="Masukan Nomor Pendaftaran" placeholder='12938719823712' disabled />
                                    </InputGroup>
                                </Form.Group> */}
                            </Row>

                            <Form.Group className="mb-3" controlId="formGridDesc">
                                <Form.Label>Deskripsi Singkat</Form.Label>
                                <Form.Control as="textarea" rows={5} />
                            </Form.Group>

                            <Form.Group className="mb-3" controlId="formGridCover">
                                <Form.Label>Cover Buku</Form.Label>
                                <Form.Control type='file' />
                            </Form.Group>

                            <Row className="mb-3">
                                {/* <Form.Group as={Col} sm={12} md={6} controlId="formGridPrice">
                                    <Form.Label>Harga Buku</Form.Label>
                                    <InputGroup as={Col} className="mb-3">
                                        <InputGroup.Text>IDR</InputGroup.Text>
                                        <FormControl aria-label="Jumlah dana dalam rupiah" placeholder='15000' />
                                        <InputGroup.Text>.00</InputGroup.Text>
                                    </InputGroup>
                                </Form.Group>

                                <Form.Group as={Col} sm={12} md={6} controlId="formGridPrice">
                                    <Form.Label>Koin Rate</Form.Label>
                                    <InputGroup as={Col} className="mb-3">
                                        <InputGroup.Text>STC</InputGroup.Text>
                                        <FormControl aria-label="Jumlah dana dalam rupiah" placeholder='15' disabled />
                                    </InputGroup>
                                </Form.Group> */}

                                <Form.Group as={Col} sm={12} controlId="formGridState" className='mt-2'>

                                    <Form.Label>Pilih Kategori</Form.Label>
                                    <Form.Select defaultValue="novel">
                                        <option>Novel</option>
                                        <option>Fiksi</option>
                                        <option>Romance</option>
                                    </Form.Select>
                                </Form.Group>

                            </Row>
                        </Form>
                    </Card.Body>
                </Card>
                <Card>
                    <Card.Body>
                        <Form>
                            {/* <Row className="mb-3">
                                <Form.Group as={Col} sm={12} md={6} controlId="formGridJudul" className={isSmallDevice ? 'mb-3' : 'mb-0'}>
                                    <Form.Label>Pilih Penerbit</Form.Label>
                                    <Form.Select defaultValue="Aneka Ilmu">
                                        <option>Starlova Publishing</option>
                                        <option>Gramedia Indonesia</option>
                                        <option>Aneka Ilmu</option>
                                    </Form.Select>
                                </Form.Group>

                                <Form.Group as={Col} sm={12} md={6} controlId="formGridPrice">
                                    <Form.Label>Asal Kota/Kab Penerbit</Form.Label>
                                    <Form.Select defaultValue="semarang">
                                        <option>Semarang</option>
                                        <option>Jakarta</option>
                                        <option>Surabaya</option>
                                    </Form.Select>
                                </Form.Group>
                            </Row> */}

                            <Row className="mb-3">
                                <Form.Group as={Col} sm={12} md={6} controlId="formGridPrice">
                                    <Form.Label>Voucher Book %</Form.Label>
                                    <InputGroup as={Col} className="mb-3">
                                        <InputGroup.Text>OFF</InputGroup.Text>
                                        <FormControl aria-label="Jumlah dana dalam rupiah" placeholder='15' />
                                        <InputGroup.Text>%</InputGroup.Text>
                                    </InputGroup>
                                </Form.Group>

                                <Form.Group as={Col} sm={12} md={6} controlId="formGridPrice">
                                    <Form.Label>Kode Voucher</Form.Label>
                                    <InputGroup as={Col} className="mb-3">
                                        <InputGroup.Text>CODE</InputGroup.Text>
                                        <FormControl aria-label="Masukan kode voucher buku" placeholder='DISC152022#STC' />
                                    </InputGroup>
                                </Form.Group>

                                <Form.Group as={Col} sm={12} controlId="formGridState" className='mb-2'>
                                    <Form.Label>Penayangan Iklan</Form.Label>
                                    <Form.Select defaultValue="novel">
                                        <option>Regular - Rp 0</option>
                                        <option>Premium - Rp 10.000/hari</option>
                                        <option>Executive - Rp 15.000/hari</option>
                                    </Form.Select>
                                </Form.Group>

                                <Form.Group as={Col} sm={12} controlId="formGridState" className='mt-2'>
                                    <Form.Label>Buat Halaman</Form.Label>
                                    <Editor initialData={textInit}/>
                                </Form.Group>
                            </Row>
                        </Form>
                    </Card.Body>
                </Card>
            </CardGroup>
            <Form.Group className="mb-3 mt-3" id="formGridCheckbox" style={{ display: 'flex', justifyContent: 'space-between' }}>
                <Form.Check
                    inline
                    label="Saya mematuhi Persyaratan Layanan"
                    name="syarat"
                    type={'checkbox'}
                    isValid={false}
                    style={{ display: 'flex', alignItems: 'center' }}
                />
                <ButtonGroup className='justify-content-around'>
                    <Button variant="warning" type="submit" size='sm' onClick={setMenulis}>
                        Batalkan
                    </Button>
                    <Button variant="primary" type="submit" size='sm'>
                        Buat Buku
                    </Button>
                </ButtonGroup>
            </Form.Group>
        </React.Fragment>
    )
}
