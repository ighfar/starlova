import React, { useState } from 'react'
import ProductData from 'components/json/ProductData';
import { ubahIcon, ubahMeta } from 'redux/LayoutSlices';
import { useDispatch } from 'react-redux';
import { AddProductButton, FormProductOverlay } from 'components/overlays';
import { CardGroup, Col, Row, Card, Button } from 'react-bootstrap';
import Wrapper, { Consumer } from 'components/shared/Page/Wrapper';
import ButtonIcon, { ButtonIconDisable } from 'components/shared/Button/ButtonIcon';
import BookIcon from 'assets/images/pdf.png'
import BookPdfIcon from 'assets/images/wfh.png'
import FormWriteBook from './Form/FormWriteBook';
import Draft from './Book/Draft';
import Pengajuan from './Book/Pengajuan';

const SelectTypeBook = () => {
    const [menulis, setMenulis] = useState(false);
    const toggleMenulis = () => {
        setMenulis(!menulis);
    }
    const styles = {
        footerButton: {
            display: 'flex',
            justifyContent: 'flex-end',
            alignItems: 'center'
        },
        cardImage: { width: '80%', maxHeight: '320px' }
    }
    return (
        <>
            {
                menulis ? (
                    <FormWriteBook setMenulis={toggleMenulis} />
                ) :
                    (
                        <CardGroup className='animation-fades'>
                            <Card>
                                <Card.Img variant="center" style={styles.cardImage} src={BookIcon} />
                                <Card.Body>
                                    <Card.Title>Menulis Buku Digital</Card.Title>
                                    <Card.Text style={{ fontSize: '14px' }}>
                                        Menulis sebuah buku &amp; undang penulis lainnya untuk berkolaborasi dalam pembuatan karyamu.
                                        Berkolaborasi dengan para penulis akan lebih cepat dan efisien dalam pembuatan konten yang menarik.
                                    </Card.Text>
                                </Card.Body>
                                <Card.Footer style={styles.footerButton}>
                                    <ButtonIcon title='Menulis Buku' icon='post_add' variant='success' onClick={toggleMenulis} />
                                </Card.Footer>
                            </Card>
                            <Card className='opacity-50'>
                                <Card.Img variant="center" style={styles.cardImage} src={BookPdfIcon} />
                                <Card.Body>
                                    <Card.Title>Upload Buku PDF</Card.Title>
                                    <Card.Text style={{ fontSize: '14px' }}>
                                        Publikasikan buku berupa dokumen PDF yang kamu punya yang telah memenuhi standard persyaratan platform kami.
                                    </Card.Text>
                                </Card.Body>
                                <Card.Footer style={styles.footerButton}>
                                    <ButtonIcon title='Publish Buku' icon='library_add' variant='success' disabled={true} />
                                </Card.Footer>
                            </Card>
                        </CardGroup>
                    )
            }
        </>
    )
}

export default function Product({ action }) {

    const dispatch = useDispatch();

    React.useEffect(() => {
        dispatch(
            ubahMeta({
                title: 'Manage Produk',
                description: 'Manage Produk Penulis Starlova Publishing Apps',
            })
        )

        dispatch(
            ubahIcon({
                icon: 'inventory_2',
                label: 'Manage Produk'
            })
        )

    }, [])
    const pages = [
        { title: 'Buat Buku', path: '#buku' },
        { title: 'Draft Buku', path: '#draft' },
        { title: 'Pengajuan', path: '#pengajuan' },
    ]


    return (
        <div className='animation-fades'>
            <Wrapper data={pages}>
                <Consumer>
                    {
                        value => {
                            switch (value) {
                                case '#buku':
                                    return value == null || '#buku' && (<SelectTypeBook />);
                                case '#draft':
                                    return value == null || '#draft' && (<Draft />)
                                case '#pengajuan':
                                    return value == null || '#draft' && (<Pengajuan />)
                            }
                        }
                    }
                </Consumer>
            </Wrapper>
        </div>
    )
}
