import React from 'react'
import Loader from 'components/shared/Loader'
import { useDispatch } from 'react-redux';
import { ubahIcon, ubahMeta } from 'redux/LayoutSlices';
import Book1 from 'assets/clients/book-1.png';
import { CardGroup, Col, Row, Card, Button } from 'react-bootstrap';
import ButtonIcon from 'components/shared/Button/ButtonIcon';
import Wrapper, { Consumer } from 'components/shared/Page/Wrapper';

export default function Books() {
    const dispatch = useDispatch();

    const styles = {
        container: {
            gridColumn: 'span 16'
        }
    }
    React.useEffect(() => {
        dispatch(
            ubahMeta({
                title: 'Semua Buku',
                description: 'Semua Buku Penulis Starlova Publishing',
            })
        );

        dispatch(
            ubahIcon({
                icon: 'book',
                label: 'Semua Buku'
            })
        );
    }, []);
    const pages = [
        { title: 'My Books', path: '#showcase' },
        { title: 'Archive Books', path: '#archive' },
        { title: 'Purchase Books', path: '#purchase' },
    ]
    return (
        <div style={styles.container} className='animation-fades'>
            {/* Pendapatan */}
            {/* <Loader loading={true} /> */}
            <Wrapper data={pages}>
                <Consumer>
                    {
                        value => {
                            switch (value) {
                                case '#showcase':
                                    return value !== null && (
                                        <Row xs={1} md={4} xs={2} className="g-4">
                                            {Array.from({ length: 10 }).map((_, idx) => (
                                                <Col key={idx}>
                                                    <Card>
                                                        <Card.Img variant="top" src={Book1} />
                                                        <Card.Body>
                                                            <Card.Title style={{ fontSize: '1rem', }}>Kisah Mistis Kakek Sugiono #{idx + 1}</Card.Title>
                                                            <Card.Text className='text-muted' style={{ fontSize: '0.8em', }}>
                                                                This is a longer card with supporting text below as a natural
                                                                lead-in to additional content. This content is a little bit longer.
                                                            </Card.Text>
                                                            <div className='text-right' style={{ textAlign: 'right' }}>
                                                                <ButtonIcon title='Preview Buku' icon='import_contacts' variant='success' />
                                                            </div>
                                                        </Card.Body>
                                                    </Card>
                                                </Col>
                                            ))}
                                        </Row>
                                    )
                                case '#archive':
                                    return value !== null && (
                                        <>Archive Books</>
                                    )
                                case '#purchase':
                                    return value !== null && (
                                        <>Pembelian Buku</>
                                    )
                            }
                        }
                    }
                </Consumer>
            </Wrapper>
        </div>
    )
}
