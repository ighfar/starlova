import React from 'react'
import Book1 from 'assets/clients/book-1.png';
import Book2 from 'assets/clients/book-2.png';
import Book3 from 'assets/clients/book-3.png';
import Book4 from 'assets/clients/book-4.png';
import FormWriteBook from './Form/FormEEditBook';
import { CardGroup, Col, Row, Card, Button, ButtonGroup } from 'react-bootstrap';
import ButtonIcon from 'components/shared/Button/ButtonIcon';
import Check from './Check';

export default function Draft() {
    const data = [Book1, Book2, Book3, Book4, Book1, Book2, Book3, Book4, Book1, Book2, Book3, Book4];
    const [checkBooks, setCheckBooks] = React.useState(false);

    const setCheckBuku = () => {
        setCheckBooks(!checkBooks)
    }

    const SelectEditBook = () => {
    const [mengupdate, setUpdate] = useState(false);
    const toggleMengupdate = () => {
        setMengupdate(!mengupdate);
    }

    return (
        <React.Fragment>
            {
                checkBooks ? (
                    <Check/>
                ) 
                mengupdate ? (
                    <FormEditBook setUpdate={toggleMengupdate} />
                ) :
                (
                    <Row xs={1} md={4} xs={2} className="g-4 animation-fades">
                        {Array.from({ length: 4 }) && data.map((_, idx) => (
                            <Col key={idx}>
                                <Card>
                                    <Card.Img variant="top" src={_} />
                                    <Card.Body>
                                        <Card.Title style={{ fontSize: '1rem', }}>Kisah Mistis Kakek Sugiono #{idx + 1}</Card.Title>
                                        <Card.Text className='text-muted' style={{ fontSize: '0.8rem', }}>
                                            This is a longer card with supporting text below as a natural
                                            lead-in to additional content. This content is a little bit longer.
                                        </Card.Text>
                                        <ButtonGroup aria-label="Basic example">
                                            <ButtonIcon title='Edit' icon='edit_note' variant='warning' onClick={toggleMengupdate} />
                                            <ButtonIcon title='Hapus' icon='delete_forever' variant='danger'/>
                                            <ButtonIcon title='Check' icon='checklist' variant='success' onClick={setCheckBuku}/>
                                        </ButtonGroup>
                                    </Card.Body>
                                </Card>
                            </Col>
                        ))}
                    </Row>
                )
            }
        </React.Fragment>
    )
}
