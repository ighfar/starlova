import React from 'react'
import Book1 from 'assets/clients/book-1.png';
import Book2 from 'assets/clients/book-2.png';
import Book3 from 'assets/clients/book-3.png';
import Book4 from 'assets/clients/book-4.png';
import { CardGroup, OverlayTrigger, Col, Row, Card, Button, ButtonGroup, Tooltip } from 'react-bootstrap';
import ButtonIcon from 'components/shared/Button/ButtonIcon';
import { Consumer } from 'components/shared/Page/Wrapper';

export default function Pengajuan() {
  const data = [Book1, Book2, Book3, Book4];
  const [detailPengajuan, setDetailPengajuan] = React.useState(false);
  return (
    <React.Fragment>
      {
        !detailPengajuan ? (
          <Row xs={2} md={4} className="g-4 animation-fades">
            {Array.from({ length: 4 }) && data.map((_, idx) => (
              <Col key={idx}>
                <Card>
                  <Card.Img variant="top" src={_} />
                  <Card.Header>
                    <OverlayTrigger
                      placement="right"
                      delay={{ show: 250, hide: 400 }}
                      overlay={
                        <Tooltip id="button-tooltip" style={{ fontSize: '10px' }}>
                          Accepted
                        </Tooltip>
                      }
                    >
                      <Button variant="success"
                        style={{
                          position: 'absolute',
                          right: 10,
                          top: 5,
                          padding: '0.8rem',
                          opacity: 0.5,
                          borderRadius: 50,
                        }}
                      >
                        {' '}
                      </Button>
                    </OverlayTrigger>
                  </Card.Header>
                  <Card.Body>
                    <Card.Title style={{ fontSize: '1rem', }}>Kisah Mistis Kakek Sugiono #{idx + 1}</Card.Title>
                    <Card.Text className='text-muted' style={{ fontSize: '0.8rem' }}>
                      This is a longer card with supporting text below as a natural
                      lead-in to additional content. This content is a little bit longer.
                    </Card.Text>
                    <ButtonGroup aria-label="Basic example">
                      <ButtonIcon title='Lihat Detail' icon='feed' variant='success' onClick={setDetailPengajuan} />
                      <ButtonIcon title='12 Comment' icon='forum' variant='dark' />
                    </ButtonGroup>
                  </Card.Body>
                </Card>
              </Col>
            ))}
          </Row>
        ) : (
          <>Detail Pengajuan Buku</>
      )
      }
    </React.Fragment>
  )
}
