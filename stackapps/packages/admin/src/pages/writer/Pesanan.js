import React from 'react'
import Loader from 'components/shared/Loader'
import { useDispatch } from 'react-redux';
import { ubahIcon, ubahMeta } from 'redux/LayoutSlices';
import { Button, Table, Badge, OverlayTrigger, Popover, Pagination } from 'react-bootstrap';
import Wrapper from 'components/shared/Page/Wrapper'

export default function Pesanan() {
    const styles = {
        button: {
            padding: '0.2rem',
            fontSize: '12px',
            marginInline: 5,
            width: 80,
        },

        statusBadge: {
            borderRadius: '50px',
            fontWeight: '300',
            padding: 6,
            marginRight: 5,
            marginTop: 5,
        },

        produkName: {
            textTransform: 'capitalize',
            padding: '.3rem',
            fontWeight: '300',
        },

        textTruncate: {
            whiteSpace: 'nowrap',
            overflow: 'hidden',
            textOverflow: 'ellipsis',
        },

        type: {
            textAlign: 'center',
            textTransform: 'uppercase',
            fontSize: 12,
        },
        pagination: {
            justifyContent: 'flex-end',
            marginTop: 10,
            marginRight: 10,
            marginBottom: 10,
        },
    };

    const dispatch = useDispatch();
    React.useEffect(() => {
        dispatch(
            ubahMeta({
                title: 'Manage Pesanan',
                description: 'Manage Pesanan Penulis Starlova Publishing Apps',
            })
        )
        dispatch(
            ubahIcon({
                icon: 'sell',
                label: 'Manage Pesanan'
            })
        );
    }, []);

    const popover = () => {
        return (<>
            <Popover id="popover-basic">
                <Popover.Header as="h3">Popover right</Popover.Header>
                <Popover.Body>
                    And here's some <strong>amazing</strong> content. It's very engaging.
                    right?
                </Popover.Body>
            </Popover>
        </>)
    }

    const pages = [
        { title: 'Pending', path: '#pending'},
        { title: 'Success', path: '#success'},
        { title: 'Canceled', path: '#canceled'},
    ]

    const data = [
        {
            orderId: '#1231311',
            namaProduk: 'Kisah Mistis Kakek Sugiono',
            statusTrx: 'pending',
            buyer: 'anisa@starlova.com',
            type: 'PDF',
        },
        {
            orderId: '#1231312',
            namaProduk: 'Lempar Batu Dewa',
            statusTrx: 'success',
            buyer: 'anisa@starlova.com',
            type: 'e-book',

        },
        {
            orderId: '#1231313',
            namaProduk: 'Cantik itu Luka',
            statusTrx: 'process',
            buyer: 'anisa@starlova.com',
            type: 'e-book',

        },
        {
            orderId: '#1231313',
            namaProduk: 'Purnama Membawa Duka',
            statusTrx: 'success',
            buyer: 'anisa@starlova.com',
            type: 'PDF',

        },
        {
            orderId: '#1231311',
            namaProduk: 'Mistikus Cinta',
            statusTrx: 'pending',
            buyer: 'irawan23writer@starlova.com',
            type: 'e-book',

        },
        {
            orderId: '#1231312',
            namaProduk: 'Perwira Mimpiku',
            statusTrx: 'success',
            buyer: 'anggadwidarma@starlova.com',
            type: 'PDF',

        },
        {
            orderId: '#1231313',
            namaProduk: 'Keajaiban Anak Pungut',
            statusTrx: 'process',
            buyer: 'anisa@starlova.com',
            type: 'e-book',

        },
        {
            orderId: '#1231313',
            namaProduk: 'Bulan Purnama Membawa Duka',
            statusTrx: 'success',
            buyer: 'anisa@starlova.com',
            type: 'e-book',

        },
        {
            orderId: '#1231313',
            namaProduk: 'Bulan Purnama Membawa Duka',
            statusTrx: 'success',
            buyer: 'puspitaningrum@starlova.com',
            type: 'e-book',

        },
        {
            orderId: '#1231313',
            namaProduk: 'Bulan Purnama Membawa Duka',
            statusTrx: 'success',
            buyer: 'anisa@starlova.com',
            type: 'e-book',

        },
        {
            orderId: '#1231313',
            namaProduk: 'Wasiat Nenek Tua',
            statusTrx: 'success',
            buyer: 'mahasura@starlova.com',
            type: 'e-book',

        },
        {
            orderId: '#1231313',
            namaProduk: 'Bulan Purnama Membawa Duka',
            statusTrx: 'success',
            buyer: 'anisa@starlova.com',
            type: 'e-book',

        },
    ]

    return (
        <div className='animation-fades'>
            <Wrapper data={pages}>
                <Table striped bordered hover variant='light' size='sm' style={{
                    fontFamily: 'Times New Roman',
                    fontSize: 14,
                    // borderColor: '#234',
                }}>
                    <thead>
                        <tr align='center' style={{ ...styles.textTruncate, }}>
                            <th width='8%'>ID</th>
                            <th>NAMA PRODUK</th>
                            <th width='15%'>DATE</th>
                            <th width='10%'>TYPE</th>
                            <th width='8%'>STATUS</th>
                            <th width='20%'>AKSI</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            data.map((item, key) => (
                                <tr key={key} style={styles.textTruncate}>
                                    <td align='center'>
                                        {item.orderId}
                                        {/* <Badge
                                            style={{ ...styles.statusBadge, color: item.statusTrx === 'process' && '#234' }}
                                            bg={
                                                item.statusTrx === 'success' ? 'success' :
                                                    item.statusTrx === 'process' ? 'warning' : 'primary'}
                                        >{' '}</Badge> */}
                                    </td>
                                    <td style={styles.produkName}>{item.namaProduk}</td>
                                    <td align='center' >{new Date().toUTCString()}</td>
                                    <td style={styles.type}>{item.type}</td>
                                    <td
                                        align='center'
                                        style={{
                                            textTransform: 'capitalize',
                                            background: 'var(--bs-gray-100)',
                                            fontWeight: 400,
                                        }}
                                    >
                                        {item.statusTrx}
                                    </td>
                                    <td align='center' style={styles.textTruncate}>
                                        {
                                            item.statusTrx !== 'success' ? (<>
                                                <Button
                                                    disabled={item.statusTrx === 'success' || item.statusTrx === 'process' && true}
                                                    variant='danger' size='sm'
                                                    style={styles.button}
                                                >
                                                    Batalkan
                                                </Button>
                                                <Button
                                                    disabled={item.statusTrx === 'success' && true}
                                                    variant={item.statusTrx === 'pending' ? 'primary' : 'success'} size='sm'
                                                    style={styles.button}
                                                >
                                                    {
                                                        item.statusTrx === 'process' ? 'Konfirmasi' : 'Proses'
                                                    }
                                                </Button>
                                            </>) : (
                                                <span className='text-letter-legal'>
                                                    ==== ORDER COMPLETED ====
                                                </span>
                                            )
                                        }
                                    </td>
                                </tr>
                            ))
                        }
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colSpan={6}>
                                <Pagination
                                    style={styles.pagination}
                                >
                                    <Pagination.First />
                                    <Pagination.Prev />
                                    <Pagination.Item>{1}</Pagination.Item>
                                    <Pagination.Ellipsis />

                                    <Pagination.Item>{10}</Pagination.Item>
                                    <Pagination.Item>{11}</Pagination.Item>
                                    <Pagination.Item active>{12}</Pagination.Item>
                                    <Pagination.Item>{13}</Pagination.Item>
                                    <Pagination.Item disabled>{14}</Pagination.Item>

                                    <Pagination.Ellipsis />
                                    <Pagination.Item>{20}</Pagination.Item>
                                    <Pagination.Next />
                                    <Pagination.Last />
                                </Pagination>
                            </td>
                        </tr>
                    </tfoot>
                </Table>
            </Wrapper>
        </div>
    )
}
