import React, { useState } from 'react'
import { ubahMeta, ubahIcon } from 'redux/LayoutSlices';
import { useDispatch, useSelector } from 'react-redux';
import PreviewBook from 'components/books/BookPreview';
import { FormProductOverlay } from 'components/overlays';
import BookOverlay from 'components/overlays/Books/BookOverlay';

export default function Preview() {
    const { book } = useSelector((state)=> state.layout);
    const showOverlays = () => {
        const overlay = document.querySelector('.preview-books');
              overlay.classList.toggle('active');
    }
    const dispatch = useDispatch();
    React.useEffect(() => {
        
        dispatch(
            ubahMeta({
                title: 'Preview Buku',
                description: 'Preview Buku Penulis Starlova Publishing Apps',
            })
        );

        dispatch(
            ubahIcon({
                icon: 'bookmark',
                label: 'Preview Buku'
            })
        );
        book.zoom && showOverlays();
    }, [book])

    return (
        <>
           <div className='dasboard-admin animation-fades'>
                {
                    book.zoom === false && ( <PreviewBook/> )
                }
                 { 
                    book.zoom && (<BookOverlay><PreviewBook/></BookOverlay>)
                }
           </div>
        </>
    )
}
