import React from 'react'
import Loader from 'components/shared/Loader'
import { useDispatch } from 'react-redux';
import { ubahIcon, ubahMeta } from 'redux/LayoutSlices';
import Wrapper from 'components/shared/Page/Wrapper';
import MultiTypeChart from 'components/shared/Chart/MultiTypeChart';

export default function Pendapatan() {
    const dispatch = useDispatch();

    const styles = {
        container: {
            gridColumn: 'span 16'
        }
    }
    React.useEffect(() => {
        dispatch(
            ubahMeta({
                title: 'Estimasi Pendapatan',
                description: 'Estimasi Pendapatan Penulis Starlova Publishing Apps',
            })
        );

        dispatch(
            ubahIcon({
                icon: 'credit_card',
                label: 'Estimasi Pendapatan'
            })
        );
    }, [])
    const pages = [
        {title: 'Bulan', path: '#month'},
        {title: 'Minggu', path: '#week'},
    ]
    return (
        <div style={styles.container}>
            {/* Pendapatan */}
            <Wrapper data={pages}>
                <Loader loading={true} />
                <MultiTypeChart/>
            </Wrapper>
        </div>
    )
}
