import { createSlice } from '@reduxjs/toolkit'

const initialState = {
    heading: {
        icon: 'space_dashboard',
        label: 'Manage Dashboard'
    },
    meta: {
        title: 'Home',
        description: 'Starlova Publishing Apps',
    },
    book: {
        zoom: false,
    }
};

const LayoutSlices = createSlice({
    name: 'layout',
    initialState,
    reducers: {
        ubahIcon(state, action) {
            return {
                ...state,
                heading: {
                    icon: action.payload.icon,
                    label: action.payload.label
                }
            };
        },

        ubahPreview(state, action) {
            return {
                ...state,
                book: {
                    zoom: action.payload.zoom,
                }
            };
        },



        ubahMeta(state, action) {
            return {
                ...state,
                meta: {
                    title: action.payload.title,
                    description: action.payload.description
                }
            };
        }

    },
})

export const { ubahIcon, ubahMeta, ubahPreview } = LayoutSlices.actions
export default LayoutSlices.reducer