import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    loading: true,
    error: '',
    data: []
}

const ApiSlices = createSlice({
    name: "apis",
    initialState,
    reducers: {
        setFetchData(state, action){
            return {
                ...state,
                loading : action.payload.loading,
                error: action.payload.error,
                data: action.payload.data
            }
        },
    }
})

export const { setFetchData } = ApiSlices.actions;
export default ApiSlices.reducer;