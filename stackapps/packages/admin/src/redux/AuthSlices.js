import { createSlice } from "@reduxjs/toolkit"

const initialState = {
    isLoggedIn: false, 
    roles: 'user',
    params: null,
    secret: null,
    email: null,
}
const AuthSlices = createSlice({
    name: "auth",
    initialState,
    reducers: {
        setAuth(state, action){
            return {
                ...state,
                isLoggedIn: action.payload.isLoggedIn,
                roles: action.payload.roles,
                params: action.payload.params,
                secret: action.payload.secret,
                email: action.payload.email
            }
        }
    }
})

export const { setAuth } = AuthSlices.actions;
export default AuthSlices.reducer;