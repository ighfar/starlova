import { createSlice } from "@reduxjs/toolkit"

const initialState = {
    email: null,
}
const ProfileSlices = createSlice({
    name: "profile",
    initialState,
    reducers: {
        setProfile(state, action){
            return {
                ...state,
                email: action.payload.email
            }
        }
    }
})

export const { setProfile } = ProfileSlices.actions;
export default ProfileSlices.reducer;