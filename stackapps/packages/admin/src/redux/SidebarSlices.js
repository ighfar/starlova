import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    toggle: true,
    display: 'block',
    width: '260px',
    content: 'flex-start'
}

const SidebarSlices = createSlice({
    name: "sidebar",
    initialState,
    reducers: {
        setSidebar(state, action){
            return {
                ...state,
                display : action.payload.display,
                toggle: action.payload.toggle,
                width: action.payload.width,
                content: action.payload.content
            }
        },
    }
})

export const { setSidebar } = SidebarSlices.actions;
export default SidebarSlices.reducer;