import React, { useState, useEffect } from "react";
import useAxios from "components/hooks/useAxios";
import jwtDecode from "jwt-decode";
import { setAuth } from "redux/AuthSlices";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import Encrypt, { Decrypt } from "utils/Encrypt";

const useSession = async () => {
    const dispatch = useDispatch();
    const { response, loading, error } = useAxios({
        method: 'POST',
        url: '/writer/auth/revoke',
        headers: {
            accept: "*/*"
        }
    });

    const decoded = response && jwtDecode(response.token);
    const forbidden = error?.response && error.response.data.code;
    const navigate = useNavigate();

   React.useEffect(() => {
        const roles = decoded?.roles;
        // console.log(Decrypt(Encrypt(decoded?.userid)))
        forbidden === 403 ? dispatch(setAuth({
            isLoggedIn: false,
            roles: 'user'
        })) : decoded !== 'undefined' && dispatch(setAuth({
            isLoggedIn: true,
            roles,
            params: `?stpub=${Encrypt(decoded?.userid)}&pubid=${Encrypt(decoded?.pubid)}&role=${Encrypt(roles)}`,
            secret: Encrypt(response?.token)
        }));

        decoded !== 'undefined' && decoded?.roles !== 'admin' ? 
        navigate(`/writer?stpub=${Encrypt(decoded?.userid)}&pubid=${Encrypt(decoded?.pubid)}&role=${Encrypt(roles)}`, { replace: true }) : 
        navigate(`/tenant?stpub=${Encrypt(decoded?.userid)}&pubid=${Encrypt(decoded?.pubid)}&role=${Encrypt(roles)}`, { replace: true });

   }, [response, forbidden])
};

export default useSession;