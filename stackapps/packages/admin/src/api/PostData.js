import { useState, useEffect } from "react";
import useAxios from "components/hooks/useAxios";

const PostData = () => {
    const { response, loading, error } = useAxios({
        method: 'POST',
        url: '/posts',
        headers: { // no need to stringify
            accept: '*/*'
        },
        data: {  // no need to stringify
            userId: 1,
            id: 19392,
            title: 'title',
            body: 'Sample text',
        },
    });
    const [data, setData] = useState([]);

    useEffect(() => {
        if (response !== null) {
            setData(response);
        }
    }, [response]);

    return {
        loading, error, response
    }
};

export default PostData;