import React, { useState, useEffect } from "react";
import useAxios from "components/hooks/useAxios";
import jwtDecode from "jwt-decode";
import { setAuth } from "redux/AuthSlices";
import { useDispatch, useSelector} from "react-redux";
import { useNavigate, useLocation } from "react-router-dom";
import { baseURL } from "components/config";

const getToken = async () => {
    const location = useLocation();
    const [ token, setAccess ] = useState(false);
    const dispatch = useDispatch();
    const { secret } = useSelector((state) => state.auth);
    const getToken = location.search;
    const anySession = getToken.includes("session");
    const isAny = anySession && getToken.split("=");
    const navigate = useNavigate();
  
   React.useEffect(() => {
        if(anySession){
            localStorage.setItem("secret", isAny[1]);
            navigate("/", { replace: true });
            window.location.reload();
        } else {
            const secretToken = localStorage.getItem("secret");
            if(secretToken !== null || secret !== null){
                const checkValue = isAny[1] === "" ;
                const tokenValid = checkValue !== "";
                const getTokenString = tokenValid && window.atob(window.atob(secretToken));
                setAccess(getTokenString);
                const decoded = token && jwtDecode(token);
                decoded !== 'undefined' && dispatch(setAuth({
                        isLoggedIn: true,
                        roles: decoded?.roles,
                        params: `?stpub=${decoded?.userid}$pubid=${decoded?.pubid}`,
                        secret: secretToken
                    }));
    
                decoded !== 'undefined' && decoded?.roles !== 'admin' ? 
                navigate(`/writer?stpub=${decoded?.userid}&pubid=${decoded?.pubid}`, { replace: true }) : 
                navigate(`/admin?stpub=${decoded?.userid}$pubid=${decoded?.pubid}`, { replace: true });
            } else {
                window.location.href = `${baseURL.apps}`;
            }
        }
   }, [token, secret])
};

export default getToken;