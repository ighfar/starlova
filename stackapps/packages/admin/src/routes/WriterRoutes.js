import React from 'react';
import { Route, Routes } from 'react-router-dom';
import Books from 'pages/writer/Books';
import Pesanan from 'pages/writer/Pesanan';
import Product from 'pages/writer/Produk';
import Pendapatan from 'pages/writer/Pendapatan';
import Peraturan from 'pages/writer/Peraturan';
import Preview from 'pages/writer/Preview';

export default function WriterRoutes(props) {
    const styles = {
        NotFound: {
            gridColumn: 'span 16'
        }
    }
    return (
        <Routes>
            <Route index element={<Preview/>} />
            <Route path='books' element={<Books/>} />
            <Route path='pesanan' element={<Pesanan/>} />
            <Route path='produk' element={<Product/>} />
            <Route path='pendapatan' element={<Pendapatan/>} />
            <Route path='terms' element={<Peraturan/>} />
            <Route path="/*" element={<div style={styles.NotFound}>There's nothing here!</div>} />
        </Routes>
    );
}