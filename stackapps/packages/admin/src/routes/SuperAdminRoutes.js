import React from 'react'
import { Navigate, Outlet } from 'react-router-dom'

export default function SuperAdminRoutes({ isLoggedIn, roles }) {
    return isLoggedIn && roles === 'admin' ? <Outlet /> : <Navigate to="/auth" />
}

export function WriterAdminRoutes({ isLoggedIn, roles }) {
    return isLoggedIn && roles === 'user' ? <Outlet /> : <Navigate to="/auth" />
}
