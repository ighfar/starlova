import React from 'react';
import { Route, Routes } from 'react-router-dom';
import Product from 'pages/admin/Product';
import Dashboard from 'pages/admin/Dashboard';
import Pesanan from 'pages/admin/Pesanan';
import Pendapatan from 'pages/admin/Pendapatan';
import Roles from 'pages/admin/Roles';

export default function AdminRoutes() {
    const styles = {
        NotFound: {
            gridColumn: 'span 16'
        }
    }
    return (
        <Routes>
            <Route index element={<Dashboard />}/>
            <Route path='dashboard' element={<Dashboard />} />
            <Route path="produk" element={<Product />} />
            <Route path="pesanan" element={<Pesanan />} />
            <Route path="pendapatan" element={<Pendapatan />} />
            <Route path="roles" element={<Roles />} />
            <Route path="/*" element={<div style={styles.NotFound}>There's nothing here!</div>} />
        </Routes>
    );
}