## Stackapps Javascript Boilerplate: React + Node JS (Express) + MySQL + Phpmyadmin + Okteto

This example shows how to leverage [Okteto](https://github.com/okteto/okteto) to develop a React + Node JS (Express)  + MySQL + Phpmyadmin Sample App directly on Okteto Cloud. The Sample App is deployed using a [kustomize](https://github.com/okteto/polling/blob/master/okteto-pipeline.yml). It creates the following components:

- Monorepo *React* based on dir **packages**.
- A apis as **api**.
- A [MySQL](https://www.mysql.com/) database.
- A [PHPMYADMIN](https://www.phpmyadmin.net/) for database management

## Tutorial

- Deploy the **Stackapps Javascript Boilerplate** on your personal namespace by clicking on the following button:


|  Current Maintain Branch | Contribute Project | Deploy | Maintainer
| :------------ |:------------- | :------------- | :-------------
| alamdev     | [![Contribute Project](https://wso2.org/jenkins/view/wso2-dependencies/job/forked-dependencies/job/wso2-synapse/badge/icon)](https://github.com/users/admhabits/projects/3?fullscreen=true) | Okteto Deploy | Alam Wibowo
  <p align="left">
 <a href="https://cloud.okteto.com/deploy">
  <img src="https://okteto.com/develop-okteto.svg" alt="Develop on Okteto">
</a>
</p>


- To publish running the multiple **stackapps** on kubernetes production:

```
  $ npm run deploy

```
- After deploy was succeed we should have services as follows:

```
  - Phpmyadmin Services on https://phpmyadmin-<yourusername>.cloud.okteto.net
  - Api Services on https://api-<yourusername>.cloud.okteto.net
  - React App on https://admin-<yourusername>.cloud.okteto.net
  
```

- To develop running the multiple **stackapps** on local development:

```
    $ cd stackapps && npm run start:dev
        apis-stack: [nodemon] 2.0.15
        apis-stack: [nodemon] to restart at any time, enter `rs`
        apis-stack: [nodemon] watching path(s): *.*
        apis-stack: [nodemon] watching extensions: js,mjs,json
        apis-stack: [nodemon] starting `node server.js`
        
        apis-stack: Server is running on http://localhost:8181

    - You can now view apps in the browser.
        apps:   Local:            http://localhost:8000
        apps:   On Your Network:  http://192.168.88.253:8000
        apps: Note that the development build is not optimized.
        apps: To create a production build, use yarn build.

    - You can now view apps in the browser.
        admin:   Local:            http://localhost:9000
        admin:   On Your Network:  http://192.168.88.253:9000
        admin: Note that the development build is not optimized.
        admin: To create a production build, use yarn build.
```
